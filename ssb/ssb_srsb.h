#ifndef SSB_SRSB
#define SSB_SRSB

#include "ftkvme/VMEInterface.h"
#include "ftkvme/VMEManager.h"

#include "ssb/ssb_vme_addr.h"
#include "ssb/ssb_defines.h"
#include "ssb/ssb_fpga.h"

#include <math.h>
#include <vector>

/*

Class to retrieve, hold, and decode status register spybuffer

*/



namespace daq
{
    namespace ftk
    {

    // Status Register Spybuffer contents order
    // This list must be kept up-to-date with the order of entries in the firmware
    // https://gitlab.cern.ch/atlas-ftk/fw/SSB/EXTF/blob/common_ph_and_sync/EXTF.srcs/sources_1/shell/main_exp.v#L3959
    enum class ssb_srsb_contents_extf: uint32_t
    {
        LVL1D_IN_DFA,        // 0
        Count_NomTracks_In,
        Count_PixTracks_In,
        Count_SctTracks_In,
        stream_fifo_datacount1,
        stream_fifo_datacount2,
        stream_fifo_datacount3,
        stream_fifo_datacount4,
        df_module_id_ibl__datacount,
        df_module_id_sct1__datacount,
        df_module_id_sct2__datacount,
        df_ibl_hit_col_coord__datacount,
        df_ibl_hit_row_coord__datacount,
        df_sct_hit1_coord__datacount,
        df_sct_hit2_coord__datacount,
        aux_module_id_ibl__datacount,
        aux_module_id_sct__datacount,
        aux_ibl_hit_col_coord__datacount,
        aux_ibl_hit_row_coord__datacount,
        aux_sct_hit_coord__datacount,   // 19
        SPACER1,        
        TF_outfifo_datacount,
        exmem_nom_sector_data_count,
        exmem_pix_sector_data_count,
        exmem_sct_sector_data_count,
        exmem_trans_data_count, // 25
        TF_header_fifo_datacount,
        TF_trailer_fifo_datacount,
        TF_nom_count_datacount,
        TF_pix_count_datacount,
        TF_sct_count_datacount, // 30
        SPACER2,
        ssid_fifo_datacount,
        hcm_fifo_datacount,
        hlm_fifo_datacount0,
        hlm_fifo_datacount1,
        hlm_fifo_datacount2,
        hlm_fifo_datacount3,
        hlm_fifo_datacount4,
        hlm_fifo_datacount5,
        hlm_fifo_datacount6, // 40
        hlm_fifo_datacount7,
        hlm_fifo_datacount8,
        hlm_fifo_datacount9,
        hlm_fifo_datacount10,
        hlm_fifo_datacount11,
        hlm_fifo_datacount12,
        hlm_fifo_datacount13,
        hlm_fifo_datacount14,
        hlm_fifo_datacount15,
        SPACER3, // 50
        dbg_hitfifo_error, 
        dbg_eol_counter, // error counter, doesn't reset
        hxmpp_hlm_IBL_hitrate, 
        hxmpp_hlm_SCT1_hitrate,
        hxmpp_hlm_SCT2_hitrate,
        hxmpp_hlm_SCT3_hitrate,
        processing_time_min_r,
        processing_time_max_r, 
        SPACER5, // 59
        Count_SelRecIBL,            // 60 recovery fits
        Count_SelRecSCT1,           // 61
        Count_SelRecSCT2,           // 62
        Count_SelRecSCT3, 
        max_BP_dfa,
        max_BP_dfb,
        max_BP_auxa,
        max_BP_auxb,
        dfa_max_IBL_mod_per_event,
        dfa_max_SCT_mod_per_event,
        dfa_IBL_mod_rate, // 70
        dfa_SCT_mod_rate, 
        dfa_max_IBL_clusters_per_event,
        dfa_max_SCT_clusters_per_event,
        dfa_IBL_cluster_rate, 
        dfa_SCT_cluster_rate, 
        dfb_max_IBL_mod_per_event,
        dfb_max_SCT_mod_per_event,
        dfb_IBL_mod_rate, 
        dfb_SCT_mod_rate, 
        dfb_max_IBL_clusters_per_event, // 80
        dfb_max_SCT_clusters_per_event,
        dfb_IBL_cluster_rate, 
        dfb_SCT_cluster_rate, 
        aux_track_limit_hit_cnt,    // 84
        tf_track_limit_hit_cnt,     // 85
        tf_fit_limit_hit_nom_cnt,   // 86
        tf_fit_limit_hit_pix_cnt,   // 87
        tf_fit_limit_hit_sct_cnt,   // 88
        track_count_max,            // 89 max tracks / event in last sec
            temp1, // 90
            temp2,
            temp3,
            temp4,
            temp5,
            temp6,
            temp7,
            temp8,           
            temp9, 
            temp10, // 100
            temp11,
            temp12,
            temp13,
            temp14,
            temp15,
            temp16,           
    };

    // Status Register Spybuffer contents order
    // This list must be kept up-to-date with the order of entries in the firmware
    // https://gitlab.cern.ch/atlas-ftk/fw/SSB/HW/blob/hw_srsb/HitWarrior.srcs/sources_1/srsb/top_srsb.vhd#L179
    enum class ssb_srsb_contents_hw: uint32_t
    {
        event_rate,
        eventsout,
        track_rate,
        tracksout,
        extf0_fifo,
        extf1_fifo,
        extf2_fifo,
        extf3_fifo,
        flic_fifo, 
        processing_time_min, // 10
        processing_time_max,
        temp8,
        temp9,
        temp10,
        temp11,
        temp12,
        temp13,
        temp14,
        temp15,
        temp16, // 20
        temp17,
        temp18,
        temp19,
        temp20,
        temp21,
        temp22,
        temp23,
        temp24,
        temp25,
        temp26, // 30
        temp27,
        temp28,
        temp29,
        temp30,

    };

    // If we're cross-comparing enums, return false
    inline bool operator==(const ssb_srsb_contents_extf &lhs, const ssb_srsb_contents_hw &rhs)
    {
        return false;
    }

    inline bool operator==(const ssb_srsb_contents_hw &lhs, const ssb_srsb_contents_extf &rhs)
    {
        return false;
    }


    // Class definition
    class ssb_srsb
    {
    public:
        // constructor
        ssb_srsb(){;};
        ssb_srsb(VMEInterface *vmeif, bool isEXTF=true); // pulling srsb from fpga on board
        ssb_srsb(std::shared_ptr<ssb_fpga> fpga, bool isEXTF=true); // pulling srsb from fpga on board

        ssb_srsb(std::vector<uint32_t> &emon_srsb); // using class to parse srsb from emon
   
        void update(); // pull srsb from board

        // template to return int or float for status words which are partially processed       
        template<typename T, typename en> T getStatus(en reg)
        {
            int index = static_cast<int>(reg);
            //if (index > srsb_raw.size()) return; // TODO how to make a check like this work

            // Full 32bit counters, directly shipped out
            if (reg == ssb_srsb_contents_extf::LVL1D_IN_DFA ||
                reg == ssb_srsb_contents_extf::Count_NomTracks_In ||
                reg == ssb_srsb_contents_extf::Count_PixTracks_In ||
                reg == ssb_srsb_contents_extf::Count_SctTracks_In ||
                reg == ssb_srsb_contents_extf::dbg_eol_counter ||
                reg == ssb_srsb_contents_extf::processing_time_min_r ||
                reg == ssb_srsb_contents_extf::processing_time_max_r ||
                // hitwarrior
                reg == ssb_srsb_contents_hw::eventsout ||
                reg == ssb_srsb_contents_hw::track_rate ||
                reg == ssb_srsb_contents_hw::event_rate ||
                reg == ssb_srsb_contents_hw::tracksout  )
            {
                return srsb_raw[index];
            }
            else // FIFO counters to be devided by high 16bits to get fraction
            {
                uint16_t fifo_size = (SSB_SRSB_FIFO_SIZE_MASK & srsb_raw[index]) >> 16;
                uint16_t fifo_count = SSB_SRSB_FIFO_COUNT_MASK & srsb_raw[index];


                return float(fifo_count) / fifo_size;
                
            }
        }


        // return raw int in srsb
        template<typename en>
        uint32_t getStatusRaw(en reg)
        {
            int index = static_cast<int>(reg);
            return srsb_raw[index];
        }

        // return raw int in srsb
        uint32_t getStatusRaw(uint reg)
        {
            return srsb_raw[reg];
        }

        // return whole raw vector
        std::vector<uint32_t> getRaw()
        {
            return srsb_raw;
        }
        

    private:        
        bool directVME; 
        bool m_isEXTF;

        VMEInterface *m_fpga_vmeif;
        std::shared_ptr<ssb_fpga> m_fpga;

        std::vector<uint32_t> srsb_raw;


    };

  }

}








#endif
