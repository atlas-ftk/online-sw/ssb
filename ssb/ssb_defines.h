// This contains definitions of constant paramters for the SSB

#ifndef SSB_DEFINES
#define SSB_DEFINES

#include <string>
#include <map>

#define SSB_MAX_VME_RETRIES 1

#define SSB_RC_INIT 0x1
#define SSB_RC_CONFIGURED 0x2
#define SSB_RC_CONNECTED 0x4
#define SSB_RC_RUNNING 0x8

#define SSB_SPYBUFFER_BLT 64     // taken from ssb spybuffer dump 
#define SSB_SPYBUFFER_NWORDS 64  // taken from ssb spybuffer dump 
#define SSB_EXTF_NSPY 10
#define SSB_HW_NSPY 10


// Link Status Mask
#define AUX_A_MASK 0x40
#define AUX_B_MASK 0x200
#define DF_MASK    0x9


// Block transfer settings for histos that are separated by less than 0x100 address space
#define SSB_HISTO_BLT    64     
#define SSB_HISTO_BIG_BLT 256     
#define SSB_HISTO_NWORDS 16  



#define SSB_EXTF_RATE_DENOMINATOR 1000
#define SSB_EXTF_BP_DIVISOR 1e8

#define SSB_HW_RATE_DENOMINATOR 500
#define SSB_HW_BP_DIVISOR 2e8

#define SSB_N_EXTF_FPGA 4

#define SSB_EXTF_SRSB_SIZE 108
#define SSB_HW_SRSB_SIZE 35
#define SSB_SRSB_NWORDS 16  
#define SSB_SRSB_FIFO_OVERFLOW_MASK 0x8000
#define SSB_SRSB_FIFO_UNDERFLOW_MASK 0x4000
#define SSB_SRSB_FIFO_COUNT_MASK 0x3FFF
#define SSB_SRSB_FIFO_SIZE_MASK 0xFFFF0000


#define SSB_OLD_FW 0x18101112 // FW before this date


#define SSB_SOURCEID_SPYBUFFER 0x00
#define SSB_SOURCEID_SRSB      0xf0
// Offsets of the source ID for histos.  n_fpga (0--3) is added to this number, need to leave space for it (0x4), only 4 spaces because the hitwarrior has a separate in/out bit
#define SSB_SOURCEID_DF_HISTO 0x10
#define SSB_SOURCEID_AUX_HISTO 0x14
#define SSB_SOURCEID_TRACK_HISTO 0x18
#define SSB_SOURCEID_OUTPACKETLEN_HISTO 0x1c
#define SSB_SOURCEID_SKEW_HISTO 0x20

#define SSB_SOURCEID_DFAPH_TIMEOUT_HISTO 0x24
#define SSB_SOURCEID_DFBPH_TIMEOUT_HISTO 0x28
#define SSB_SOURCEID_AUXAPH_TIMEOUT_HISTO 0x3c
#define SSB_SOURCEID_AUXBPH_TIMEOUT_HISTO 0x30

#define SSB_SOURCEID_L1ID_SKIP_HISTO 0x34
#define SSB_SOURCEID_DFAL1ID_SKIP_HISTO 0x38
#define SSB_SOURCEID_DFBL1ID_SKIP_HISTO 0x3c
#define SSB_SOURCEID_AUXAL1ID_SKIP_HISTO 0x40
#define SSB_SOURCEID_AUXBL1ID_SKIP_HISTO 0x44
#define SSB_SOURCEID_TFFIFOL1ID_SKIP_HISTO 0x48

#define SSB_SOURCEID_LAYERMAP_HISTO 0x4c

// HW histos n_fpga is set to 0 for the hw source id, so we don't need to skip
#define SSB_SOURCEID_HWL1ID_SKIP_HISTO 0x10
#define SSB_SOURCEID_HW_NTRACK_HISTO   0x11
#define SSB_SOURCEID_HW_CHI2_HISTO     0x12
#define SSB_SOURCEID_HW_D0_HISTO       0x13
#define SSB_SOURCEID_HW_Z0_HISTO       0x14
#define SSB_SOURCEID_HW_COTTH_HISTO    0x15
#define SSB_SOURCEID_HW_PHI0_HISTO     0x16
#define SSB_SOURCEID_HW_CURV_HISTO     0x17
#define SSB_SOURCEID_HW_LAYERMAP_HISTO 0x18




class histo_names 
{
 public:
    std::map<int, std::string> extf_name_map {
        // EXTF histograms
        {SSB_SOURCEID_DF_HISTO, "DF_HISTO"},
        {SSB_SOURCEID_AUX_HISTO, "AUX_HISTO"},
        {SSB_SOURCEID_TRACK_HISTO, "TRACK_HISTO"},
        {SSB_SOURCEID_OUTPACKETLEN_HISTO, "OUTPACKETLEN_HISTO"},
        {SSB_SOURCEID_SKEW_HISTO, "SKEW_HISTO"},
        {SSB_SOURCEID_DFAPH_TIMEOUT_HISTO, "DFAPH_TIMEOUT_HISTO"},
        {SSB_SOURCEID_DFBPH_TIMEOUT_HISTO, "DFBPH_TIMEOUT_HISTO"},
        {SSB_SOURCEID_AUXAPH_TIMEOUT_HISTO, "AUXAPH_TIMEOUT_HISTO"},
        {SSB_SOURCEID_AUXBPH_TIMEOUT_HISTO, "AUXBPH_TIMEOUT_HISTO"},
        {SSB_SOURCEID_L1ID_SKIP_HISTO, "L1ID_SKIP_HISTO"},
        {SSB_SOURCEID_DFAL1ID_SKIP_HISTO, "DFAL1ID_SKIP_HISTO"},
        {SSB_SOURCEID_DFBL1ID_SKIP_HISTO, "DFBL1ID_SKIP_HISTO"},
        {SSB_SOURCEID_AUXAL1ID_SKIP_HISTO, "AUXAL1ID_SKIP_HISTO"},
        {SSB_SOURCEID_AUXBL1ID_SKIP_HISTO, "AUXBL1ID_SKIP_HISTO"},
        {SSB_SOURCEID_TFFIFOL1ID_SKIP_HISTO, "TFFIFOL1ID_SKIP_HISTO"},
        {SSB_SOURCEID_LAYERMAP_HISTO, "LAYERMAP_HISTO"} };

    std::map<int, std::string> hw_name_map {
        // HitWarrior histograms
        {SSB_SOURCEID_HWL1ID_SKIP_HISTO, "HWL1ID_SKIP_HISTO"},
        {SSB_SOURCEID_HW_NTRACK_HISTO, "HW_NTRACK_HISTO"},
        {SSB_SOURCEID_HW_CHI2_HISTO, "HW_CHI2_HISTO"},  
        {SSB_SOURCEID_HW_D0_HISTO, "HW_D0_HISTO"},  
        {SSB_SOURCEID_HW_Z0_HISTO, "HW_Z0_HISTO"},
        {SSB_SOURCEID_HW_COTTH_HISTO, "HW_COTTH_HISTO"},
        {SSB_SOURCEID_HW_PHI0_HISTO, "HW_PHI0_HISTO"},
        {SSB_SOURCEID_HW_CURV_HISTO, "HW_CURV_HISTO"},
        {SSB_SOURCEID_HW_LAYERMAP_HISTO, "HW_LAYERMAP_HISTO"} };


};



#endif
