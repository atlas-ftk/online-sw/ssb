#ifndef DATAFLOWREADERSsb_H
#define DATAFLOWREADERSsb_H

#include "ftkcommon/DataFlowReader.h"
#include "ftkcommon/SourceIDSpyBuffer.h"
#include "ssb/dal/FtkDataFlowSummarySSBNamed.h"
#include "ssb/SSBSpybuffer.h"
#include "ssb/ssb_srsb.h"

#include <string>
#include <vector>


// Namespaces
namespace daq {
namespace ftk {

/*! \brief  Ssb implementation of DataFlowReader
*/
class DataFlowReaderSsb : public DataFlowReader
{
public:
    /*! \brief Constructor: sets the input scheme (IS inputs or vectors)
     *  \param ohRawProvider Pointer to initialized  OHRawProvider object
     *  \param forceIS Force the code to use IS as the source of information (default is 0)
     */
    DataFlowReaderSsb(std::shared_ptr<OHRawProvider<>> ohRawProvider, 
		bool forceIS = false);


    void init(const string& deviceName,
              const std::string& partitionName = "FTK",
              const std::string& isServerName = "DF");


    /*! \brief Get fraction of BUSY from downstream
     *
     *  \param option The type of the average, depends on particular implementation
     *
     */
    //void getBusyFraction(std::vector<float>& srv, uint32_t option = 0);

    /*! \brief Get fraction of Input FIFO BUSY 
     *
     *  \param option The type of the average, depends on particular implementation
     *
     */
    void getFifoInBusyFraction(std::vector<float>& srv, uint32_t option = 0);
    /*! \brief Get fraction of Output FIFO BUSY 
     *
     *  \param option The type of the average, depends on particular implementation
     *
     */
    void getFifoOutBusyFraction(std::vector<float>& srv, uint32_t option = 0);
    /*! \brief Get fraction of Input FIFO Empty 
     *
     *  \param option The type of the average, depends on particular implementation
     *
     */
    void getFifoInEmptyFraction(std::vector<float>& srv, uint32_t option = 0); 
    /*! \brief Get fraction of Output FIFO Empty 
     *
     *  \param option The type of the average, depends on particular implementation
     *
     */
    void getFifoOutEmptyFraction(std::vector<float>& srv, uint32_t option = 0);


    /*! \brief Get current Input FIFO BUSY 
     */
    void getFifoInBusy(std::vector<int64_t>& srv);
    void getFifoInBusyCumulative(std::vector<int64_t>& srv);
    /*! \brief Get current Output FIFO BUSY 
     */
    void getFifoOutBusy(std::vector<int64_t>& srv);
    void getFifoOutBusyCumulative(std::vector<int64_t>& srv);
    /*! \brief Get current Input FIFO Empty 
     */
    //void getFifoInEmpty(std::vector<int64_t>& srv);
    /*! \brief Get current Output FIFO Empty      */
    //void getFifoOutEmpty(std::vector<int64_t>& srv);

    /*! \brief Get L1ID seen on each Input channel
     */
    void getL1id(std::vector<int64_t>& srv);
    void getL1id_error(std::vector<int64_t>& srv);

    /*! \brief Get data errors on each Input channel
     */
    //void getDataErrors(std::vector<int64_t>& srv);


    /*! \brief Get event rate on each Input channel
     */
    void getEventRate(std::vector<float>& srv);

    /*! \brief Get status of input links (0: ON, 1: OFF, 2: Errors seen)
     */
    void getLinkInStatus(std::vector<int64_t>& srv);

    /*! \brief Get status of output links (0: ON, 1: OFF, 2: Errors seen)
     */
    void getLinkOutStatus(std::vector<int64_t>& srv);

    /* Publish SSB specific info     */

    void getFpgaTemperature(std::vector<int64_t>& srv);


    /*! \brief Get number of events processed on each output channel  */
    void getNEventsProcessed(std::vector<int64_t>& srv);

    /*! \brief Get number of packets discarded */
    void getNPacketsDiscardedSync(std::vector<int64_t>& srv); 

    // Underflow flag for FIFOs
    void getFifoInUnderflowFlag(std::vector<int64_t>& srv);
    // Underflow flag for FIFOs
    void getFifoOutUnderflowFlag(std::vector<int64_t>& srv);


    // new monitoring
    void getPacketCompletionTime(std::vector<int64_t>& srv);
    void getFifoInOverflowFlag(std::vector<int64_t>& srv);
    void getFifoOutOverflowFlag(std::vector<int64_t>& srv);
    void getMaxEvtProcessingTime(std::vector<int64_t>& srv);
    void getMinEvtProcessingTime(std::vector<int64_t>& srv);
    void getMaxBackPressureTime(std::vector<int64_t>& srv);
    void getFreezeCount(std::vector<int64_t>& srv);
    void getErrorCount(std::vector<int64_t>& srv);
    void getDataTruncationCount(std::vector<int64_t>& srv);
    void getIBLHitRate(std::vector<float>& srv);
    void getSCT5HitRate(std::vector<float>& srv);
    void getSCT7HitRate(std::vector<float>& srv);
    void getSCT11HitRate(std::vector<float>& srv);


    void getNEventsIn(std::vector<int64_t>& srv);
    void getNTracksOut(std::vector<int64_t>& srv);
    void getNTracksOutRecovery(std::vector<int64_t>& srv);
    void getTrackRate(std::vector<float>& srv);
    void getHWTemperature(std::vector<float> &srv);
    void getHWVCCINT(std::vector<float> &srv);
    void getHWVCCAUX(std::vector<float> &srv);
    void getHWVCCBRAM(std::vector<float> &srv);
    void getHWVPVN(std::vector<float> &srv);
    void getHWVREFP(std::vector<float> &srv);
    void getHWVREFN(std::vector<float> &srv);
    void getLinkErrorsCount(vector<int64_t>& srv);
    void getEXTFResets(vector<int64_t>& srv);

    void getNEventsProcSyncEngine(vector<int64_t>& srv);
    

    void getEXPState(vector<int64_t>& srv);
    void getReadyState(vector<int64_t>& srv);
    void getMasterState(vector<int64_t>& srv);

    void getFWVersion(vector<int64_t>& srv);

    void getMaxModulesPerEvent(vector<int64_t>& srv);
    void getModuleRate(std::vector<float>& srv);
    void getMaxClustersPerEvent(vector<int64_t>& srv);
    void getClusterRate(std::vector<float>& srv);
    void getMaxTracksPerEvent(vector<int64_t>& srv);

    void getHitTrackingLimits(vector<int64_t>& srv);

    // Histograms built in FW
    void getTrackHistogram(vector<int64_t>& srv, int n_fpga);
    void getAuxHistogram(vector<int64_t>& srv, int n_fpga);
    void getDFHistogram(vector<int64_t>& srv, int n_fpga);
    void getSkewHistogram(vector<int64_t>& srv, int n_fpga);
    void getOutPacketLenHistogram(vector<int64_t>& srv, int n_fpga);
    void getDFA_PH_TimeoutHistogram(vector<int64_t>& srv, int n_fpga);
    void getDFB_PH_TimeoutHistogram(vector<int64_t>& srv, int n_fpga);
    void getAUXA_PH_TimeoutHistogram(vector<int64_t>& srv, int n_fpga);
    void getAUXB_PH_TimeoutHistogram(vector<int64_t>& srv, int n_fpga);
    void getL1IDSkipHistogram(vector<int64_t>& srv, int n_fpga);
    void getTFFIFO_L1IDSkipHistogram(vector<int64_t>& srv, int n_fpga);
    void getTFL1IDSkipHistogram(vector<int64_t>& srv, int n_fpga);
    void getDFAL1IDSkipHistogram(vector<int64_t>& srv, int n_fpga);
    void getDFBL1IDSkipHistogram(vector<int64_t>& srv, int n_fpga);
    void getAUXAL1IDSkipHistogram(vector<int64_t>& srv, int n_fpga);
    void getAUXBL1IDSkipHistogram(vector<int64_t>& srv, int n_fpga);
    void getLayerMapHistogram(vector<int64_t>& srv, int n_fpga);
    void getHitsPerLayerHistogramDF(vector<int64_t>& srv, int n_fpga, int layer);
    void getHitsPerLayerHistogramEXP(vector<int64_t>& srv, int n_fpga, int layer);
    void getHWL1IDSkipHistogram(vector<int64_t>& srv);
    void getHWNTrackHistogram(vector<int64_t>& srv);
    void getHWChi2Histogram(vector<int64_t>& srv);
    void getHWD0Histogram(vector<int64_t>& srv);
    void getHWZ0Histogram(vector<int64_t>& srv);
    void getHWCotthHistogram(vector<int64_t>& srv);
    void getHWPhi0Histogram(vector<int64_t>& srv);
    void getHWCurvHistogram(vector<int64_t>& srv);
    void getHWLayerMapHistogram(vector<int64_t>& srv);


    
    /*! \brief Publish extra distributions to IS as histograms
     *  \param option To be defined (default is 0)
     */
    void publishExtraHistos(uint32_t option = 0);  


    void HistoHelper(vector<int64_t>& srv, uint32_t sourceid);


    void setNEXTF(int n){n_EXTF_FPGAs = n;};
    void setSlot (int n){m_slot = n;};
    void setEnabledFPGAs( std::vector<bool> enabled){ m_enabled_FPGAs = enabled;  };
    void setEnabledChannels( std::vector<bool> enabled){ m_enabled_Channels = enabled;  };
    void setEXTFFreq(const std::shared_ptr< std::vector<uint32_t> > n){ m_EXTF_Freq = n;};
    void setOldFW(const std::shared_ptr< std::vector<bool> > n){ m_OldFW = n;};
    
    void setPublishOnlyNonZeroBins(bool pub){m_PublishOnlyNonZeroBins = pub;};
    //void setBoard(std::shared_ptr<ftk::ssb_board> board){m_board = board;};
    
    void setHistoSpybufferData(std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > histos){m_histos = histos;};
    void setSRSBs(std::vector< std::shared_ptr<ssb_srsb> > srsb){m_SRSBs = srsb;};

private:
    std::vector<std::string> getISVectorNames();

    void get_in_link_status( std::vector<int64_t> &srv, u_int sb_entry);
    void get_out_link_status(std::vector<int64_t> &srv, u_int sb_entry);
    void get_all_link_status(std::vector<int64_t> &srv, u_int sb_entry);
    u_int CLOCK_CYCLES_PER_SAMPLE = 10000;

    int n_EXTF_FPGAs;
    std::shared_ptr< std::vector<uint32_t> > m_EXTF_Freq;
    std::shared_ptr< std::vector<bool> > m_OldFW;
    int m_slot; 
    bool m_PublishOnlyNonZeroBins;
    std::vector<bool> m_enabled_FPGAs;
    std::vector<bool > m_enabled_Channels;

    std::shared_ptr<FtkDataFlowSummarySSBNamed> m_theSSBSummary;

    std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > m_histos;
    std::vector< std::shared_ptr<ssb_srsb> > m_SRSBs;

private:

};

} // namespace ftk
} // namespace daq

#endif /* DATAFLOWREADERSsb_H */
