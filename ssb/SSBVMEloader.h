#ifndef __SSB_VME_LOADER__
#define  __SSB_VME_LOADER__
#include "ftkvme/VMEInterface.h" 
#include "ftkvme/VMEManager.h" 
#include "ssb/ssb_vme_addr.h"
#include "ssb/ssb.h"


class SSBVMEloader {
public:
  SSBVMEloader(uint32_t slot,uint32_t fpga);
 SSBVMEloader(VMEInterface *v) : vme(v) {};
  void loadEXP(const std::vector<uint32_t> &payload);
  void loadTKF(const std::vector<uint32_t> &payload);
  bool verifyEXP(const std::vector<uint32_t> &payload);
  bool  verifyTKF(const std::vector<uint32_t> &payload);
 private:
  bool verify(const std::vector<uint32_t> &payload ,uint32_t addr,uint32_t waddr,uint32_t nWords);
  void load(const std::vector<uint32_t> &payload,uint32_t addr,uint32_t nWords);
   VMEInterface *vme;
};
#endif
