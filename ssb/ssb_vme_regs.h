#ifndef SSB_VME_REGS_H
#define SSB_VME_REGS_H

#define CHECK_ERROR(x) if (x != VME_SUCCESS) \
    { \
      VME_ErrorPrint(x); \
      return (x); \
    }





//++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ VME INTERFACE REGISTER ++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define STATUS_REGISTER_VME			0x00000000
#define INIT_AMB          			0x00000004
#define TMODE		        		0x00000008
#define ENABLE_TCK      	    		0x0000000C
#define AMB_VME_BANKCHECKSUM			0x00000010
#define TMS_FPGA				0x00000014
#define TDI_FPGA				0x00000018
#define TDO_FPGA				0x0000001C
//#define TMS					    0x00000020 /**< TMS signal for amchips: this address can be used only in single write mode */
//#define TDI					    0x00000024 /**< TDI signal for amchips: this address can be used only in single write mode */
//#define	TDO					    0x00000028 /**< TDO signal for amchips: this address can be used only in single write mode */
#define TMS   					0x001A0000 /**< TMS signal for amchips: this address can be used only in block transfer mode */
#define TDI 	  				0x001A1000 /**< TDI signal for amchips: this address can be used only in block transfer mode */
#define	TDO 		  			0x001A2000 /**< TDI signal for amchips: this address can be used only in block transfer mode */
#define TRST			  		0x0000002C


#define SSB_MEM_TDITMS_BT	0x198000 /**< TDI-TMS memory address for BT, in order to program the LAMB'sFPGAs */
#define SSB_MEM_TDO_BT	0x19c000




// registers for access to the JTAG chains of the 4 BOUSCAs on the LAMBs
#define SSB_VME_TDITMS			0x00000058 // TDI is bit 0, TMS is bit 1 (see vme_reg.v in VMEIF fw projecy, see also my_docs_xvc_configuration.vh in the same place)
#define SSB_VME_TDO			0x0000005C

#endif
