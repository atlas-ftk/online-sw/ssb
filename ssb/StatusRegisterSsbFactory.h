#ifndef STATUSREGISTERSSBFACTORY_H
#define STATUSREGISTERSSBFACTORY_H

#include "ftkvme/StatusRegisterVMEFactory.h"

// oks schema generated class
#include "ssb/dal/StatusRegisterSsbStatusNamed.h"

#include <string>

// Namespaces
namespace daq {
namespace ftk {

/*! \brief Reads out Registers from the SSB using StatusRegisterVMECollection objects
 */
class StatusRegisterSsbFactory 
: public StatusRegisterVMEFactory
{
public:
	/*! \brief Class constructor
	 *
	 * \param ssbName The name of the SSB being read from
	 * \param ssbSlot The VME slot the SSB is situated in
	 * \param registersToRead tells the factory which register collections to read out:
	 *   T = Random registers used for testing
	 *   
	 * \param isServerName The name of the IS server the register values will be sent to
	 * \param IPCPartition The IPCPartition to be used for IS publishing
	 */
	StatusRegisterSsbFactory(
		const std::string& ssbName,
		u_int ssbSlot,
		const std::string& registersToRead,
		const std::string& isServerName,
		const IPCPartition& ipcPartition
	);

	~StatusRegisterSsbFactory();

private:
	// Member Functions

private:
	// Member Variables
    
    //FIXME: remove this hack when migration to cmake is complete

};

} // namespace ftk
} // namespace daq

#endif /* STATUSREGISTERSSBFACTORY_H */
