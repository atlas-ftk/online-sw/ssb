//////////////////////////////////////////////////////
// Header file containing the declaration of the 
// functions used to access the ssb board
///////////////////////////////////////////////////////

#ifndef FTK_SSB_H
#define FTK_SSB_H

#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "ftkvme/VMEInterface.h"

#include <vector>
#include <sstream>

#define PERIOD 120000

namespace daq 
{
  namespace ftk
  {

//    /*! \brief Routine to load an Tx FIFO.
//     *
//     * Loads a Tx FIFO with data using the contents of a file.
//     *
//     * \param slot The slot that the card is plugged into.
//     * \param fpga The FPGA to connect to
//     * \param buffer The address of the Tx FIFO
//     * \param file Text file with data to load.
//     *
//     * \return True on success, otherwise false (ie: VME error).
//     */
//    bool ssb_load_tx_fifo( u_int slot, u_int fpga, u_int buffer, const std::string& file);
//
//    void ssb_read( u_int slot, int fpga, int regaddr);
//
//    /*! \brief Load SSMap RAM
//     *
//     * \param slot The slot that the card is plugged into.
//     * \param fpga The FPGA to connect to
//     * \param address Address of SSMap RAM
//     * \param ssmappath Path to input ssmap contents
//     * \param isModule True when loading the module ssmap
//     *
//     * \return True on success, false otherwise
//     */
//    bool ssb_ssmap_load( u_int slot, u_int fpga, u_int address, const std::string& ssmappath, bool isModule);
//
//    /*! \brief Load TF constants RAM
//     *
//     * \param slot The slot that the card is plugged into.
//     * \param fpga The FPGA to connect to
//     * \param tfconstpath Path to file listing all sector files
//     *
//     * \return True on success, false otherwise
//     */
    bool ssb_const_load_from_txt(VMEInterface*& vme, const std::string& EXPCnstPath, const std::string& TKFCnstPath, const std::string& name); 

    bool ssb_ssmap_load_from_txt(VMEInterface*& vme, const std::string& AUXSSMapPix,    const std::string& AUXSSMapSCT, const std::string& DFSSMapPix,    const std::string& DFSSMapSCT, const std::string& name);

    bool ssb_octopus_load(VMEInterface*& vme, const string& test_vector, const u_int tentacle_id, const u_int vector_id);

    /*! \brief Load constants for EXP and TKF
     * 
     * \param vme The VMEInterface for this fpga
     * \param EXPCnstPath The path to the file containing EXP constants
     * \param TKFCnstPath The path to the file containing TKF constants
     *
     * \return True on success, false otherwise
     */

////    bool ssb_ammap_load( u_int slot, u_int fpga, const std::string& ammappath, bool debug);
//
//    /*! \brief Read SSB spybuffers and dump them to text.
//     *
//     * Reads a set of SSB spybuffers from one FPGA and prints them to the 
//     * std_out. The last word written into the spybuffer is always shown at the bottom.
//     *
//     * \param slot The slot that the card is plugged into.
//     * \param fpga The FPGA to connect to
//     * \param dim_buffer The number of words to read from the buffer. Set to 0 to read everything.
//     * \param buffer A list of buffer addresses (ie: 0x1, 0x2...) to read.
//     * \param root ?
//     * \param output Path to textfile to dump spy buffer into. Set to empty for no file.
//     *
//     * \return True on success, otherwise false (ie: VME error).
//     */ 
//    bool ssb_read_buffer( int slot, int fpga, u_int dim_buffer, std::vector<u_int> buffer, const std::string& output);
//
//    // Remote update of an FPGA on the SSB card 
//    bool ssb_read_epcq( int slot, int fpga, uint epcqaddress, const std::string& file);
//   
//    // Reconfigure the SSB card using firmware at a specific address in the EPCQ.
//    bool ssb_reconfigure( int slot, int fpga, uint epcqaddress);
//
//    // Reset all of the registers and clocks in the SSB 
//    bool ssb_reset(int slot, int fpga, bool doMain, bool doXcvr, bool doSync, bool doReg, bool doErr, bool doTxFIFO, bool doSpybuffers, bool doFreeze, bool doErrLatch);
//
//    // Insert here library functions
//    bool ssb_status( int slot );
//
//    // Performe remote update of an FPGA on the SSB card
//    bool ssb_upgrade( int slot, int fpga, uint epcqaddress, std::string file, uint startaddress, uint endaddress);
//
//    // ??
//    bool ssb_vmetest( int slot, int fpga, u_int addr, u_int value);
//
//    // ??
//    bool vme7700write( int slot, int fpga, int regaddr, int value);
//
//    // Information about a FIFO
//    struct ssb_fifostatus_t
//    {
//      u_int size;
//      u_int usedw;
//      float usedw_avg;
//      bool  hold;
//      bool  empty;
//      bool  ovrflw;
//      float holdRate;
//    };
//    ssb_fifostatus_t ssb_fifostatus(VMEInterface *vme, u_int addr);
//
//    struct ssb_linkstatus_t
//    {
//      ssb_fifostatus_t fifo;
//      ssb_fifostatus_t l1idsync_fifo;
//      bool             up;
//      u_int            nPackets;
//      u_int            ratePackets;
//      u_int            l1id;
//      u_int            errors;
//    };
//    ssb_linkstatus_t ssb_load_linkstatus(VMEInterface *vme, u_int addr, u_int ch, u_int upaddr, u_int upbit);
//    
//    // Just for testing purpose. TODO: remove it
    void dummy();
//
//    // Exceptions
//    ERS_DECLARE_ISSUE( ftk,
//		       AuxChecksumFailed,
//		       "Checksum of " << ramname << " failed! expected " << std::hex << std::setfill('0') << std::setw(8) << expected << ", got = " << std::setw(8) << got ,
//		       ((std::string)ramname)
//		       ((unsigned int)expected)
//		       ((unsigned int)got)		       
//		       )
//
//    ERS_DECLARE_ISSUE( ftk,
//		       AuxAMMapChecksumTimeout,
//		       "Checksum of AMMap " << proc << " timed-out",
//		       ((unsigned int)proc)
//		       )
//
//    ERS_DECLARE_ISSUE( ftk,
//		       AuxEMIFCalibration,
//		       "EMIF " << proc << " failed to calibrate.",
//		       ((unsigned int)proc)
//		       )
//
//    ERS_DECLARE_ISSUE( ftk,
//		       AuxInvalidProcID,
//		       "Invalid processor ID: " << procID ,
//		       ((unsigned int)procID)
//		       )
      

  }//namespace ftk
}//namespace daq

#endif
