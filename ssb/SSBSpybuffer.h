#ifndef SSB_SPY_BUFFER
#define SSB_SPY_BUFFER

/*
 *  Functions used for spy buffer access using VME block transfer
 */

#include <string>
#include <vector>
#include "ftkvme/VMEInterface.h"

//Including the base class
//#include "ftkcommon/SpyBuffer.h"

#include "ftkvme/vme_spy_buffer.h"


namespace daq { 
  namespace ftk {

    class ssb_spyBuffer : public daq::ftk::vme_spyBuffer {

    public:
      ssb_spyBuffer(unsigned int status_addr, 
		    unsigned int base_addr,
		    VMEInterface * vmei);
      ~ssb_spyBuffer();

      //Set SpyBuffer size. Do not use, unless in desperate need. 
      //By default, it must be decoded from the SpyStatus word
      ////void setSpyDimension(unsigned int dim) { m_userSpyDimension = dim; }

      //base classes
      uint32_t getSpyFreeze(){return false;}; ///< Returns freeze status
      bool     getSpyOverflow(){return false;}; ///< Is the spyBuffer in overflow? 
      uint32_t getSpyPointer(){return 0;};
      //uint32_t getSpyDimension() {return m_userSpyDimension;};
      
      int readSpyStatusRegister( uint32_t& spyStatus );      

      void setBuffer(std::vector<uint32_t>& b) {m_buffer = b;};
      
    };

  } // namespace ftk
} // namespace daq

#endif
