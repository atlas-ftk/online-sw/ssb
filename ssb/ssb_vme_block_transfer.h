#ifndef SSB_VME_BLOCK_TRANSFER_H
#define SSB_VME_BLOCK_TRANSFER_H


#include <string>
#include "cmem_rcc/cmem_rcc.h"
#include "vme_rcc/vme_rcc.h"
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"


#define DEBUG_BLOCK         true                                                                                                 

#define ROD_BUFFER_MAX_SIZE    0x10000   // Buffer dimension in bytes                                                              
#define ROD_DMATIMEOUT            4000    // DMA timeout in ms                                                                     
#define DEF_VME_ADDR       "0x78000008"  // VME default address                                                                    
#define DEF_OPERATION               "r"  // Default operation              



bool blockTransferReadFromFilePure(int vmeBaseAddress, int numberOfWords, std::string filePath, bool isRead);


#endif 
