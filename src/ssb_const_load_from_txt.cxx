#include "ftkvme/VMEInterface.h" 
#include "ftkvme/VMEManager.h" 
#include "ssb/ssb_vme_addr.h"
#include "ssb/ssb.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>     // std::string, std::stoi
#include <chrono>


//////////////////
/*
 VME Error catching is done up a level, around the function call to ssb_const_load_from_txt
*/
//////////////////


namespace daq 
{
    namespace ftk 
    {
        bool ssb_const_load_from_txt(VMEInterface*& vme, const string& EXPCnstPath, const string& TKFCnstPath, const string& name){

            std::vector<u_int> data;
            std::vector<u_int> data_readback;
            u_int nWords=64;
            u_int sector_id=0x0; 

            ifstream input;
            string line;
            stringstream stream;
            u_int constant; 

            // EXP constant loading
            if( EXPCnstPath!="" ){
                ERS_LOG(" >>>>  loading constants to EXP rldram <<<< ");
                data.clear();
                input.open(EXPCnstPath.c_str()); 

                if (!input.is_open()){
                    std::stringstream message;
                    message << "Could not open "<<EXPCnstPath.c_str();
                    daq::ftk::IOError ioerror(ERS_HERE, name, message.str());
                    throw(ioerror);
                }

                getline(input, line);
                stream << std::hex << line; 
                stream >> sector_id; 
                stream.clear(); 
                stream.str("");
                std::chrono::duration<double> time_span;
 
                while( !input.eof() ){

                    data.push_back( sector_id );
       
                    // constants come in blocks of 64 words, one block per (sector, connection)
                    for(u_int i=1; i<nWords;i++){
                        getline(input, line);
                        stream << std::hex << line; 
                        stream >> constant; 
                        stream.clear(); 
                        stream.str("");
                        data.push_back( constant );
                    }

                    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
                    // load one block  
                    vme->write_block(SSB_ADDR_EXPCONST_BLOCK, data);
                    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
                    // check the one block just written  
                    //          vme->write_word(SSB_ADDR_EXPCONST_SECTORID, sector_id );  // sector ID to be block-read out
                    //	  data_readback=vme->read_block(SSB_ADDR_EXPCONST_BLOCK, nWords);
        
                    // for(int iword=0; iword<nWords-3; iword++){
                    //   if(data[iword]!=data_readback[iword]){
                    //     FTK_WARNING("ERROR in EXP constant loading: sector " << std::hex << "0x" << data[0] 
                    //                                            << " written=" << std::hex << "0x" << data[iword] 
                    //                                            << " readback=" << std::hex << "0x" << data_readback[iword] 
                    //                ); 
                    //   }
                    // }
        
                    data.clear();
                    data_readback.clear();

                    // next block sector id
                    getline(input, line); 
                    stream << std::hex << line; 
                    stream >> sector_id; 
                    stream.clear(); 
                    stream.str("");
                    time_span += std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
                }
                ERS_LOG( " >>>>  done loading constants to EXP rldram. It took me " << time_span.count() << " seconds. <<<< " );
                data.clear(); 
                input.close();
            } // exp cnst loading 

            // TKF constant loading
            if( TKFCnstPath!="" ){

                ERS_LOG( " >>>>  loading constants to TF rldram <<<< " );
                data.clear();

                input.open(TKFCnstPath.c_str()); 

                if (!input.is_open()){
                    std::stringstream message;
                    message << "Could not open "<<TKFCnstPath.c_str();
                    daq::ftk::IOError ioerror(ERS_HERE, name, message.str());
                    throw(ioerror);
                }

                getline(input, line);
                stream << std::hex << line; 
                stream >> sector_id; 
                stream.clear(); 
                stream.str("");
                std::chrono::duration<double> time_span;

                while( !input.eof() ){

                    data.push_back( sector_id );

                    // constants come in blocks of 64 words, 6 blocks per sector  
                    for(u_int i=1; i<nWords;i++){
                        getline(input, line);
                        stream << std::hex << line; 
                        stream >> constant; 
                        stream.clear(); 
                        stream.str("");
                        data.push_back( constant );
                    }
                    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
                    // load one block  
                    vme->write_block(SSB_ADDR_TKFCONST_BLOCK, data);
                    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
                    // check the one block just written 
                    // vme->write_word(SSB_ADDR_TKFCONST_SECTORID, sector_id );  // sector ID to be block-read out
                    // data_readback=vme->read_block(SSB_ADDR_TKFCONST_BLOCK, nWords);

          
                    // for(int iword=0; iword<nWords-3; iword++){
                    //   if(data[iword]!=data_readback[iword]){
                    //     FTK_WARNING("ERROR in TKF constant loading: sector " << std::hex << "0x" << data[0] 
                    //                                            << " written=" << std::hex << "0x" << data[iword] 
                    //                                            << " readback=" << std::hex << "0x" << data_readback[iword] 
                    //                ); 
                    //   }
                    // }

                    data.clear();
                    data_readback.clear();

                    // next block sector id
                    getline(input, line); 
                    stream << std::hex << line; 
                    stream >> sector_id; 
                    stream.clear(); 
                    stream.str(""); 
                    time_span += std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
                }

                data.clear(); 
                input.close();
                ERS_LOG( " >>>>  done loading constants to TF rldram It took me " << time_span.count() << " seconds. <<<< " );
            } // TF constant loading


            if(EXPCnstPath=="" && TKFCnstPath=="") return false; 
            return true; 

        }//ssb_const_load
    
    } //namespace daq
} //namespace ftk


