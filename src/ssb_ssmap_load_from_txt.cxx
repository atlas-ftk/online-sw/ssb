#include "ftkvme/VMEInterface.h" 
#include "ftkvme/VMEManager.h" 
#include "ssb/ssb_vme_addr.h"
#include "ssb/ssb.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>     // std::string, std::stoi
#include <chrono>

namespace daq 
{
  namespace ftk 
  {
      // VME Error catching is done up a level, around the function call to ssb_ssmap_load_from_txt
      bool ssb_ssmap_load_from_txt(VMEInterface*& vme, const string& AUXSSMapPix, const string& AUXSSMapSCT, const string& DFSSMapPix, const string& DFSSMapSCT, const string& name){
      ERS_LOG(" >>>>  loading ssmap  <<<< ");

      std::vector<u_int> data;
      std::vector<u_int> data_readback;
      // u_int nWords=64;
      // u_int sector_id=0x0; 

      ifstream input1;
      ifstream input2;
      ifstream input3;
      ifstream input4;
      string line;
      stringstream stream;
      u_int constant; 

      std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

      if( AUXSSMapPix!="" ){
        ERS_LOG( " >>>>  loading AUX pix ssmap  <<<< " << AUXSSMapPix.c_str() );
        data.clear();
        input1.open(AUXSSMapPix.c_str()); 

        if (input1.is_open()) 
        {
            while( !input1.eof() ){
	      getline(input1, line);
	      stream << std::hex << line;
	      stream >> constant;
	      //	  std::cout << "ssmap " << constant << std::endl; 
	      stream.clear(); 
	      stream.str("");
	      data.push_back( constant );
	      vme->write_word(0x010, data[0] );
	      //	  vme->write_word(0x014, data[0] );  
	      data_readback.push_back(vme->read_word(0x014));
	      u_int data_9bits = data[0] & 0x1ff;
            
	      if(data_9bits!=data_readback[0]){
                  std::stringstream message;
                  message << "ERROR in ssmap loading:"  << " written=" << std::hex << "0x" << data_9bits << " readback=" << std::hex << "0x" << data_readback[0];
                  daq::ftk::VmeError excp(ERS_HERE, name, message.str());
                  throw excp;
	      }
	      
              data.clear();
              data_readback.clear();
      	      
            }
      	    ERS_LOG(" >>>>  done loading ssmap <<<< " );
            data.clear(); 
	    data_readback.clear();
            input1.close();
        }
        else 
        {
            std::stringstream message;
            message << "ERROR in AUX pix ssmap:"  << AUXSSMapPix.c_str() << " didn't open!";
            daq::ftk::IOError excp(ERS_HERE, name, message.str());
            throw excp;
        }
      }

      int i =0;
      if( AUXSSMapSCT!="" ){
        ERS_LOG(" >>>>  loading AUX sct ssmap  <<<< " <<AUXSSMapSCT.c_str());
        data.clear();
        input2.open(AUXSSMapSCT.c_str()); 

        if (input2.is_open())
        {
            while( !input2.eof() ){
	      getline(input2, line);
	      stream << std::hex << setw(8) << line;
	      stream >> std:: hex >> constant;
	      //	  std::cout << "ssmap " << constant << std::endl; 
	      stream.clear(); 
	      stream.str("");
	      data.push_back( constant );
	      vme->write_word(0x010, constant );
	      //	  vme->write_word(0x014, data[0] );  
	      data_readback.push_back(vme->read_word(0x014));
	      
	      u_int data_9bits = data[0] & 0x1ff;
	       if(data_9bits!=data_readback[0]){
                   std::stringstream message;
                   message << "ERROR in ssmap loading: "  << i << " written=" << std::hex << "0x" << data_9bits << " readback=" << std::hex << "0x" << data_readback[0] ;
                   daq::ftk::VmeError excp(ERS_HERE, name, message.str());
                   throw excp;
            
	       }
            
            
              data.clear();
              data_readback.clear();
      	      i++;
            }
      	    ERS_LOG(" >>>>  done loading ssmap <<<< ");
            data.clear(); 
	    data_readback.clear();
            input2.close();
        }
        else
        {
            std::stringstream message;
            message << "ERROR in AUX sct ssmap:"  << AUXSSMapSCT.c_str() << " didn't open!";
            daq::ftk::IOError excp(ERS_HERE, name, message.str());
            throw excp;
        }
        
      }


      if( DFSSMapPix!="" ){
        ERS_LOG(" >>>>  loading DF pix ssmap  <<<< " << DFSSMapPix.c_str());
        data.clear();
        input3.open(DFSSMapPix.c_str()); 

        if (input3.is_open())
        {

            while( !input3.eof() ){
	      getline(input3, line);
	      stream << std::hex << setw(8) << line;
	      stream >> std:: hex >> constant;
	      //	  std::cout << "ssmap " << constant << std::endl; 
	      stream.clear(); 
	      stream.str("");
	      data.push_back( constant );
	      vme->write_word(0x010, data[0] );
	      //vme->write_word(0x014, data[0] );  
	      data_readback.push_back(vme->read_word(0x014));
	      u_int data_9bits = data[0] & 0x1ff;
	       if(data_9bits!=data_readback[0]){
                   std::stringstream message;
                   message << "ERROR in ssmap loading:"  << " written=" << std::hex << "0x" << data_9bits << " readback=" << std::hex << "0x" << data_readback[0] ;
                   daq::ftk::VmeError excp(ERS_HERE, name, message.str());
                   throw excp;
	       }
            
            
              data.clear();
              data_readback.clear();
      	      
            }
      	    ERS_LOG(" >>>>  done loading ssmap <<<< ");
            data.clear(); 
	    data_readback.clear();
            input3.close();
        }
        else
        {
            std::stringstream message;
            message << "ERROR in DF pix ssmap:"  << DFSSMapPix.c_str() << " didn't open!";
            daq::ftk::IOError excp(ERS_HERE, name, message.str());
            throw excp;
        }
      }



      if( DFSSMapSCT!="" ){
        ERS_LOG(" >>>>  loading DF sct ssmap  <<<< " << DFSSMapSCT.c_str());
        data.clear();
        input4.open(DFSSMapSCT.c_str()); 

        if (input4.is_open())
        {
        

            while( !input4.eof() ){
	      getline(input4, line);
	      stream << std::hex << setw(8) << line;
	      stream >> std:: hex >> constant;
	      //	  std::cout << "ssmap " << constant << std::endl; 
	      stream.clear(); 
	      stream.str("");
	      data.push_back( constant );
	      vme->write_word(0x010, data[0] );
	      //vme->write_word(0x014, data[0] );  
	      data_readback.push_back(vme->read_word(0x014));
	      u_int data_9bits = data[0] & 0x1ff;
            
	       if(data_9bits!=data_readback[0]){
                   std::stringstream message;
                   message << "ERROR in ssmap loading:"  << " written=" << std::hex << "0x" << data_9bits << " readback=" << std::hex << "0x" << data_readback[0] ;
                   daq::ftk::VmeError excp(ERS_HERE, name, message.str());
                   throw excp;
	       }
            
              data.clear();
              data_readback.clear();
      	      
            }
      	    ERS_LOG(" >>>>  done loading ssmap <<<< ");
            data.clear(); 
	    data_readback.clear();
            input4.close();
        }
        else
        {
            std::stringstream message;
            message << "ERROR in DF sct ssmap:"  << DFSSMapSCT.c_str() << " didn't open!";
            daq::ftk::IOError excp(ERS_HERE, name, message.str());
            throw excp;
        }
      }


      std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double> time_span = t2 - t1;
      ERS_LOG( " Scalability: >>>> Loaded SSMAPs in "<< time_span.count() << " seconds. <<<< " );

      if(AUXSSMapSCT=="" && AUXSSMapSCT=="" && DFSSMapSCT=="" && DFSSMapSCT=="") return false; 
      return true; 

    }//ssb_const_load
    
  } //namespcae daq
} //namespcae ftk


