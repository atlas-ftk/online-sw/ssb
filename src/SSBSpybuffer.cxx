// Functions used for spy buffer access using VME block transfer

#include "ssb/SSBSpybuffer.h"

#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>

namespace daq { 
  namespace ftk {

    ssb_spyBuffer::ssb_spyBuffer(unsigned int status_addr, 
				 unsigned int base_addr,
				 VMEInterface *vmei): vme_spyBuffer(status_addr, base_addr, vmei) {
        //readSpyStatusRegister(m_spyStatus); // this is not implemented yet

    }

    //---------------------------------------------------

    ssb_spyBuffer::~ssb_spyBuffer(){}

    //---------------------------------------------------

    int ssb_spyBuffer::readSpyStatusRegister( uint32_t& spyStatus ) {
        //spyStatus = m_vme_interface->read_word(m_spy_status_reg_VME_address);
        return 0;
    }

  } // namespace ftk
} // namespace daq
