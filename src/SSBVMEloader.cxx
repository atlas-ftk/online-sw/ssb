#include <ssb/SSBVMEloader.h>


SSBVMEloader::SSBVMEloader(uint32_t slot,uint32_t fpga) { 
  vme=VMEManager::global().ssb(slot,fpga);
}
 
void SSBVMEloader::loadEXP(const std::vector<uint32_t> &payload) {
  vme->write_word(SSB_EXP_MEM_INIT,1);
  usleep(200000);
    load(payload,SSB_ADDR_EXPCONST_BLOCK,64);
}
void SSBVMEloader::loadTKF(const std::vector<uint32_t> &payload) {
   vme->write_word(SSB_TF_MEM_INIT,1);
  usleep(200000);
    load(payload,SSB_ADDR_TKFCONST_BLOCK,64);
}
bool SSBVMEloader::verifyEXP(const std::vector<uint32_t> &payload) {
    return verify(payload,SSB_ADDR_EXPCONST_BLOCK,SSB_ADDR_EXPCONST_SECTORID,64);
}
bool  SSBVMEloader::verifyTKF(const std::vector<uint32_t> &payload) {
    return verify(payload,SSB_ADDR_TKFCONST_BLOCK,SSB_ADDR_TKFCONST_SECTORID,64);
}

bool SSBVMEloader::verify(const std::vector<uint32_t> &payload ,uint32_t addr,uint32_t waddr,uint32_t nWords) {
  int32_t cmem_size=(payload.size()*sizeof(uint32_t));
  
  unsigned long cmem_desc_uaddr;
  unsigned long cmem_desc_paddr;
  int desc=vme->allocateCmem(cmem_desc_uaddr,cmem_desc_paddr,cmem_size);
  
  uint32_t *data=(uint32_t*)cmem_desc_uaddr;
  
  for(unsigned int i=0;i<payload.size()/nWords;i++)  {
    u_long paddr=cmem_desc_paddr+i*nWords*sizeof(uint32_t);
    uint32_t id=payload[i*nWords];
    
    vme->write_word(waddr, id);
    vme->read_block_to_cmem(addr, paddr, nWords,1);
  }
    bool check=true;
    for(unsigned i=0;i<payload.size();i++) 
      check&=(data[i]==payload[i]);
    vme->deallocateCmem(desc);
    return check;
}

void SSBVMEloader::load(const std::vector<uint32_t> &payload,uint32_t addr,uint32_t nWords) {
  unsigned int sectors=payload.size()/nWords;
  int32_t cmem_size=(payload.size()*sizeof(uint32_t));
  unsigned long cmem_desc_uaddr;
  unsigned long cmem_desc_paddr;
  int desc=vme->allocateCmem(cmem_desc_uaddr,cmem_desc_paddr,cmem_size);
  uint32_t *data=(uint32_t*)cmem_desc_uaddr;
  
  for(unsigned i=0;i<payload.size();i++) data[i]=payload[i];
  
  u_int maxchainel=VME_MAXCHAIN;
    u_int nblocks=sectors/maxchainel;
    u_int r_nblocks=sectors%maxchainel;
    for(unsigned int i=0;i<nblocks;i++)  {
      u_long paddr=cmem_desc_paddr+i*nWords*maxchainel*sizeof(uint32_t);
      vme->write_block_from_cmem(addr, paddr ,nWords,maxchainel);
    }
    u_long paddr=cmem_desc_paddr+nblocks*nWords*maxchainel*sizeof(uint32_t);
    vme->write_block_from_cmem(addr, paddr ,nWords,r_nblocks);
    vme->deallocateCmem(desc);
}

