#include "ftkvme/VMEInterface.h" 
#include "ftkvme/VMEManager.h" 

#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>     // std::string, std::stoi

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

using namespace std;

/*
 SSB VME address, 32 bits: 
    0000 1XXX XXXX XXXX 0001 0000 0000 0000
    ------             / --- --------------
    slot#           r/w  FPGA#            \ offset address
 */

int main(int argc, char *argv[]){

  using namespace  boost::program_options ;

  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("9"), "slot number")
    ("fpga", value< std::string >()->default_value("1"), "fpga id")
    ("mode", value< std::string >()->default_value("0"), "test mode")
    ("exp", value< std::string >()->default_value(""), "constant file for exp")
    ("tkf", value< std::string >()->default_value(""), "constant file for tkf")
    ;

  positional_options_description p;

  variables_map vm;
  try
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }

  int test_mode = std::stoi( vm["mode"].as<std::string>() ); 
  string exp_constant_file = vm["exp"].as<std::string>() ;
  string tkf_constant_file = vm["tkf"].as<std::string>() ;
  u_int slot = std::stoi( vm["slot"].as<std::string>() );
  u_int fpga = std::stoi( vm["fpga"].as<std::string>() );

  // take the arguments 
  //string exp_constant_file="";
  //string tkf_constant_file=""; 


  if( test_mode==0 && exp_constant_file=="" && tkf_constant_file=="" ){
    ERS_LOG("Please supply with the constant file for exp and tkf");
    return 0; 
  }
  //else if( test_mode==0) {
  //  exp_constant_file = (string)argv[1]; 
  //  tkf_constant_file = (string)argv[2]; 
  //  ERS_LOG("Loading constants from "<<exp_constant_file<<" "<<tkf_constant_file );
  //}

  //std::cout<<"DEBUG: before writing"<<std::endl;



  //u_int fpga=0x1; 
  //u_int slot=9;

  //u_int vme_baseaddr = (slot<<27);

  VMEInterface *vme=VMEManager::global().aux(slot,0); // always start with the base address of the ssb slot
  vme->disableBlockTransfers(false); // use real block transfer

  u_int result=0; 
  std::vector<u_int> data;
  u_int nWords=64;
  u_int address=0xF000+0x000;
  u_int sector_id=0x0; 

  //std::cout<<"DEBUG: before writing"<<std::endl;

  if( test_mode==1 ){
    std::cout<< " >>>>  test ping0 address  <<<< " << std::endl;

    vme->write_word((fpga<<12)+0x000, 0xffff1234); 
    result=vme->read_word((fpga<<12)+0x000); 
    std::cout<< "---  read out: "<< std::setw(8) << std::setfill('0') << std::right << std::hex << result<<std::endl;
    return 0; 
  }

  // spybuffer address: 0x400
  //std::cout<< " >>>>  test block transfer to vmeif address F000 <<<< " << std::endl;

  //for(u_int i=0; i<nWords;i++){
  //  data.push_back( (i+1)+((i+1)<<24) );
  //}

  //address=0xF000+0x000;
  //vme->write_block(address, data);

  //data=vme->read_block(0xF000+0x000, nWords);

  //std::cout<<"nWords read out="<<std::dec<<data.size()<<std::endl;

  //for(u_int i=0; i<data.size(); i++){
  //  std::cout<< "word " << std::setw(2) << std::dec << i <<": 0x" << std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::endl;
  //}


  // test address space 
  if( test_mode==2 ){
    std::cout<< " >>>>  test block transfer to address 100 <<<< " << std::endl;

    data.clear(); 
    nWords=32;
    for(u_int i=0; i<nWords;i++){
      //data.push_back( (i+1)+((i+1)<<16) );
      data.push_back( (i+1)+((i+1)<<24) );
    }
 
    address=(fpga<<12)+0x100;
    vme->write_block(address, data);

    data=vme->read_block((fpga<<12)+0x100, nWords);

    std::cout<<"nWords read out="<<std::dec<<data.size()<<std::endl;

    for(u_int i=0; i<data.size(); i++){
      std::cout<< "word " << std::setw(2) << std::dec << i <<": 0x" << std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::endl;
    }
    return 0;
  }

  // EXP external memory address space 
  if( test_mode==3 ){
    std::cout<< " >>>>  test block transfer to EXP rldram <<<< " << std::endl;

    for( sector_id=0x01; sector_id<0x02; sector_id++){
      nWords=64;
      //u_int sector_id = 0x000000a1; 
      data.clear();
      data.push_back( sector_id ); // sector ID
      for(u_int i=1; i<nWords;i++){
        data.push_back( sector_id+((i+1)<<16) );
        //data.push_back( (i+1)+((i+1)<<24) );
      }
 
      //for(u_int i=0; i<data.size(); i++){
      //  //std::cout<< std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::dec << std::endl;
      //  std::cout<< "word " << std::setw(2) << std::dec << i <<": 0x" << std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::endl;
      //}

      address=(fpga<<12)+0x200;
      vme->write_block(address, data);

      vme->write_word((fpga<<12)+0x0FC, sector_id );  // sector ID to be block-read out
      data=vme->read_block((fpga<<12)+0x200, nWords);

      std::cout<<" nWords read out="<<std::dec<<data.size()<<std::endl;

      for(u_int i=0; i<data.size(); i++){
        //std::cout<< std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::dec << std::endl;
        std::cout<< "word " << std::setw(2) << std::dec << i <<": 0x" << std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::endl;
      }
    }
    return 0; 
  }

  ifstream input;
  string line;
  stringstream stream;
  u_int constant; 

  // EXP constant loading
  if(test_mode==0 && exp_constant_file!="" ){
    std::cout<< " >>>>  test constant loading to EXP rldram <<<< " << std::endl;
    nWords=64;
    data.clear();

    //input.open("/afs/cern.ch/work/l/lulu/public/FTK/software/ssb/vmestream_extrapolator_1B_sector0102.txt");
    input.open(exp_constant_file.c_str()); 

    getline(input, line);
    stream << std::hex << line; 
    stream >> sector_id; 
    stream.clear(); 
    stream.str("");

    while( !input.eof() ){

      data.push_back( sector_id );
    
      for(u_int i=1; i<nWords;i++){
        getline(input, line);
        stream << std::hex << line; 
        stream >> constant; 
        stream.clear(); 
        stream.str("");
        data.push_back( constant );
      }
    
      address=(fpga<<12)+0x200;
      vme->write_block(address, data);
    
      vme->write_word((fpga<<12)+0x0FC, sector_id );  // sector ID to be block-read out
      data=vme->read_block((fpga<<12)+0x200, nWords);
    
      std::cout<<" nWords read out="<<std::dec<<data.size()<<std::endl;
    
      for(u_int i=0; i<data.size(); i++){
        std::cout<< "word " << std::setw(2) << std::dec << i <<": 0x" << std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::endl;
      }
    
      data.clear();

      getline(input, line); 
      stream << std::hex << line; 
      stream >> sector_id; 
      stream.clear(); 
      stream.str(""); 

    }

    data.clear(); 
    input.close();
  }



  //for(u_int i=0; i<data.size(); i++){
  //  //std::cout<< std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::dec << std::endl;
  //  std::cout<< "word " << std::setw(2) << std::dec << i <<": 0x" << std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::endl;
  //}

  // TF external memory address space 
  if( test_mode==4){
    std::cout<< " >>>>  test block transfer to TF rldram <<<< " << std::endl;

    for( sector_id=0x200; sector_id<0x206; sector_id++){

      nWords=64;
      //sector_id = 0x00000001; 
      //sector_id = 0x00000ffd; 
      data.clear();
      data.push_back( sector_id ); // sector ID
      for(u_int i=1; i<nWords;i++){
        //data.push_back( (i+1)+((i+1)<<16) );
        data.push_back( sector_id+((i+1)<<16) );
        //data.push_back( (i+1)+((i+1)<<24) );
      }
 
      //for(u_int i=0; i<data.size(); i++){
      //  //std::cout<< std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::dec << std::endl;
      //  std::cout<< "word " << std::setw(2) << std::dec << i <<": 0x" << std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::endl;
      //}

      address=(fpga<<12)+0x300;
      vme->write_block(address, data);
    }

    for( sector_id=0x200; sector_id<0x206; sector_id++){
      nWords=64;
    
      vme->write_word((fpga<<12)+0x0F8, sector_id );  // sector ID to be block-read out
      data=vme->read_block((fpga<<12)+0x300, nWords);

      std::cout<<" nWords read out="<<std::dec<<data.size()<<std::endl;

      for(u_int i=0; i<data.size(); i++){
        //std::cout<< std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::dec << std::endl;
        std::cout<< "word " << std::setw(2) << std::dec << i <<": 0x" << std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::endl;
      }

    }

    return 0; 
  }

  // TKF constant loading
  if(test_mode==0 && tkf_constant_file!="" ){

    std::cout<< " >>>>  test constant loading to TF rldram <<<< " << std::endl;
    data.clear();

    nWords=64;
    //return 0;

    //input.open("/afs/cern.ch/work/l/lulu/public/FTK/software/ssb/vmestream_trackfitter_test.txt");
    input.open(tkf_constant_file.c_str()); 

    getline(input, line);
    stream << std::hex << line; 
    stream >> sector_id; 
    stream.clear(); 
    stream.str("");

    std::vector<u_int> sector_ids; 

    while( !input.eof() ){

      data.push_back( sector_id );
      sector_ids.push_back( sector_id );
    
      for(u_int i=1; i<nWords;i++){
        getline(input, line);
        stream << std::hex << line; 
        stream >> constant; 
        stream.clear(); 
        stream.str("");
        data.push_back( constant );
      }
    
      address=(fpga<<12)+0x300;
      vme->write_block(address, data);

      data.clear();

      getline(input, line); 
      stream << std::hex << line; 
      stream >> sector_id; 
      stream.clear(); 
      stream.str(""); 
    }
 
    for(u_int iid=0; iid<sector_ids.size(); iid++){ 
      sector_id = sector_ids[iid];
      vme->write_word((fpga<<12)+0x0F8, sector_id );  // sector ID to be block-read out
      data=vme->read_block((fpga<<12)+0x300, nWords);
    
      std::cout<<" nWords read out="<<std::dec<<data.size()<<std::endl;
    
      for(u_int i=0; i<data.size(); i++){
        std::cout<< "word " << std::setw(2) << std::dec << i <<": 0x" << std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i] << std::endl;
      }
    

    }
  }

  data.clear(); 
  input.close();

  return 0;
}
