#include "ftkvme/VMEInterface.h" 
#include "ftkvme/VMEManager.h" 

#include "ssb/ssb_vme_addr.h"
#include "ssb/ssb_defines.h"
#include "ssb/ssb_srsb.h"

#include "ftkcommon/exceptions.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>     // std::string, std::stoi
#include <iomanip>
#include <limits>
#include <exception>

/*

TODO clean up define / variable usage

 */


/*
 SSB VME address, 32 bits: 
    0000 1XXX XXXX XXXX 0001 0000 0000 0000
    ------             / --- --------------
    slot#           r/w  FPGA#            \ offset address
 */

int main(int argc, char *argv[]){

  using namespace boost::program_options ;
  using namespace std;
  using namespace daq::ftk;

  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("9"), "slot number")
    ("NEXTF", value< std::string >()->default_value("1"), "N processing EXTF")
    ("EXTF_mask", value<std::string >()->default_value("0x0"), "EXTF bit Mask in HEX")
    ("fast", "Skip reading SRSB")
    ("noDebug", "Skip extra debug info")
    ;

  positional_options_description p;

  variables_map vm;
  try
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }

  // Setup
  u_int slot = std::stoi( vm["slot"].as<std::string>() );
  u_int N_fpga = std::stoi( vm["NEXTF"].as<std::string>() );

  stringstream ss;
  ss << std::hex <<vm["EXTF_mask"].as<std::string>();
  u_int EXTF_mask;
  ss >> EXTF_mask;

  bool fast = (vm.count("fast") ? true : false);
  bool noDebug = (vm.count("noDebug") ? true : false);

  std::vector<u_int> fpga_number;
  if(N_fpga == 1) {
    fpga_number.push_back(0);
  }else if(N_fpga == 2) {
    fpga_number.push_back(0);
    fpga_number.push_back(1);
  }else if(N_fpga == 3) {
    fpga_number.push_back(0);
    fpga_number.push_back(1);
    fpga_number.push_back(2);
  }else if(N_fpga == 4) {
    fpga_number.push_back(0);
    fpga_number.push_back(1);
    fpga_number.push_back(2);
    fpga_number.push_back(3);
  }
  fpga_number.push_back(4);

 
  if (EXTF_mask > 0)
  {
      fpga_number.clear();
      
      if (EXTF_mask & (0x1 << 0)) fpga_number.push_back(0);
      if (EXTF_mask & (0x1 << 1)) fpga_number.push_back(1);
      if (EXTF_mask & (0x1 << 2)) fpga_number.push_back(2);
      if (EXTF_mask & (0x1 << 3)) fpga_number.push_back(3);
      fpga_number.push_back(4);
  }

  // ***********************
  // Header
  // ***********************
  std::cout<< "SSB Status Main, slot="<<std::setw(2)<<slot<<":" << std::endl;  
  // Loop over FPGAs
  for(uint fpgaid=0;fpgaid<fpga_number.size();fpgaid++){

    VMEInterface *vme=VMEManager::global().ssb(slot,fpga_number[fpgaid]); // always start with the base address of the ssb slot
    // vme->disableBlockTransfers(false); // use real block transfer

    try { // Giant try to catch VME errors
          // Start reading info from board via VME
          u_int temperature = 0;
          if(fpga_number[fpgaid] ==4) temperature = vme->read_word(SSB_HW_TEMPERATURE);
          else                        temperature = vme->read_word(SSB_EXTF_TEMPERATURE);
          float temp_c = (503.975 * temperature / 4096) - 273.15;
          
          //Firmware versions    
          u_int Firmware_version;
          if(fpga_number[fpgaid] ==4) Firmware_version = vme->read_word(SSB_HW_FIRMWARE_VERSION_ADDR);
          else  Firmware_version = vme->read_word(SSB_FIRMWARE_VERSION_ADDR_EXTF);
          
          u_int FW_commitHash = 0;
          u_int FW_commitDate = 0;    
          u_int N_EXTF_FREQ = 0;

          if (fpga_number[fpgaid] <4)
          {
              FW_commitHash = vme->read_word(SSB_EXTF_FWHASH);
              FW_commitDate = vme->read_word(SSB_EXTF_FWDATE);
              N_EXTF_FREQ = vme->read_word(SSB_EXTF_FREQ);
          }
          else if (fpga_number[fpgaid] ==4)
          {
              FW_commitHash = vme->read_word(SSB_HW_FWHASH);
              FW_commitDate = vme->read_word(SSB_HW_FWDATE);
          }
    
          std::cout<< "--------------------------------------------------------------------" << std::endl;
                       
          
          std::cout<< (fpga_number[fpgaid] < 4 ? "EXTF = " : "HW = " )<<fpga_number[fpgaid]<< ", Firmware version, hash-date= " << std::hex << Firmware_version << " " << setfill('0') << setw(8) << FW_commitHash << "-"<<setw(8) << FW_commitDate<< " @ "<<std::fixed<<std::setprecision(0)<<std::dec<<std::dec<<N_EXTF_FREQ/1000000<<" MHz, ";    
          std::cout << setfill(' ');
          std::cout<<"FPGA Temp = " << std::setprecision(1) <<std::fixed << temp_c <<"C"<<std::endl;
          
          // *********************************************************
          // EXTF
          // *********************************************************
          if(fpga_number[fpgaid] < 4 ){ // EXTF FPGA, not HW

              ssb_srsb srsb(vme); 
              if (!fast){ // Readout status register spybuffer
                  srsb.update();
              }
              // parse srsb
              float f_stream_fifo_datacount1 = srsb.getStatus<float>(ssb_srsb_contents_extf::stream_fifo_datacount1);
              float f_stream_fifo_datacount2 = srsb.getStatus<float>(ssb_srsb_contents_extf::stream_fifo_datacount2);
              float f_stream_fifo_datacount3 = srsb.getStatus<float>(ssb_srsb_contents_extf::stream_fifo_datacount3);
              float f_stream_fifo_datacount4 = srsb.getStatus<float>(ssb_srsb_contents_extf::stream_fifo_datacount4);
              float f_df_module_id_ibl__datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::df_module_id_ibl__datacount);
              float f_df_module_id_sct1__datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::df_module_id_sct1__datacount);
              float f_df_module_id_sct2__datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::df_module_id_sct2__datacount);
              float f_df_ibl_hit_col_coord__datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::df_ibl_hit_col_coord__datacount);
              float f_df_ibl_hit_row_coord__datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::df_ibl_hit_row_coord__datacount);
              float f_df_sct_hit1_coord__datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::df_sct_hit1_coord__datacount);
              float f_df_sct_hit2_coord__datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::df_sct_hit2_coord__datacount);
              float f_aux_module_id_ibl__datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::aux_module_id_ibl__datacount);
              float f_aux_module_id_sct__datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::aux_module_id_sct__datacount);
              float f_aux_ibl_hit_col_coord__datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::aux_ibl_hit_col_coord__datacount);
              float f_aux_ibl_hit_row_coord__datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::aux_ibl_hit_row_coord__datacount);
              float f_aux_sct_hit_coord__datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::aux_sct_hit_coord__datacount);

              float f_TF_outfifo_datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::TF_outfifo_datacount);
              float f_exmem_nom_sector_data_count = srsb.getStatus<float>(ssb_srsb_contents_extf::exmem_nom_sector_data_count);
              float f_exmem_pix_sector_data_count = srsb.getStatus<float>(ssb_srsb_contents_extf::exmem_pix_sector_data_count);
              float f_exmem_sct_sector_data_count = srsb.getStatus<float>(ssb_srsb_contents_extf::exmem_sct_sector_data_count);
              float f_exmem_trans_data_count = srsb.getStatus<float>(ssb_srsb_contents_extf::exmem_trans_data_count);
              float f_TF_header_fifo_datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::TF_header_fifo_datacount);
              float f_TF_trailer_fifo_datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::TF_trailer_fifo_datacount);
              float f_TF_nom_count_datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::TF_nom_count_datacount);
              float f_TF_pix_count_datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::TF_pix_count_datacount);
              float f_TF_sct_count_datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::TF_sct_count_datacount);
              float f_ssid_fifo_datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::ssid_fifo_datacount);
              float f_hcm_fifo_datacount = srsb.getStatus<float>(ssb_srsb_contents_extf::hcm_fifo_datacount);
              float f_hlm_fifo_datacount0 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount0);
              float f_hlm_fifo_datacount1 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount1);
              float f_hlm_fifo_datacount2 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount2);
              float f_hlm_fifo_datacount3 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount3);
              float f_hlm_fifo_datacount4 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount4);
              float f_hlm_fifo_datacount5 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount5);
              float f_hlm_fifo_datacount6 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount6);
              float f_hlm_fifo_datacount7 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount7);
              float f_hlm_fifo_datacount8 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount8);
              float f_hlm_fifo_datacount9 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount9);
              float f_hlm_fifo_datacount10 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount10);
              float f_hlm_fifo_datacount11 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount11);
              float f_hlm_fifo_datacount12 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount12);
              float f_hlm_fifo_datacount13 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount13);
              float f_hlm_fifo_datacount14 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount14);
              float f_hlm_fifo_datacount15 = srsb.getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount15);


              bool b_OVERFLOW_stream_fifo_datacount1           = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount1);
              bool b_OVERFLOW_stream_fifo_datacount2           = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount2);
              bool b_OVERFLOW_stream_fifo_datacount3           = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount3);
              bool b_OVERFLOW_stream_fifo_datacount4           = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount4);
              bool b_OVERFLOW_df_module_id_ibl__datacount      = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_module_id_ibl__datacount);
              bool b_OVERFLOW_df_module_id_sct1__datacount     = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_module_id_sct1__datacount);
              bool b_OVERFLOW_df_module_id_sct2__datacount     = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_module_id_sct2__datacount);
              bool b_OVERFLOW_df_ibl_hit_col_coord__datacount  = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_ibl_hit_col_coord__datacount);
              bool b_OVERFLOW_df_ibl_hit_row_coord__datacount  = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_ibl_hit_row_coord__datacount);
              bool b_OVERFLOW_df_sct_hit1_coord__datacount     = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_sct_hit1_coord__datacount);
              bool b_OVERFLOW_df_sct_hit2_coord__datacount     = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_sct_hit2_coord__datacount);
              bool b_OVERFLOW_aux_module_id_ibl__datacount     = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::aux_module_id_ibl__datacount);
              bool b_OVERFLOW_aux_module_id_sct__datacount     = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::aux_module_id_sct__datacount);
              bool b_OVERFLOW_aux_ibl_hit_col_coord__datacount = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::aux_ibl_hit_col_coord__datacount);
              bool b_OVERFLOW_aux_ibl_hit_row_coord__datacount = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::aux_ibl_hit_row_coord__datacount);
              bool b_OVERFLOW_aux_sct_hit_coord__datacount     = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::aux_sct_hit_coord__datacount);

              bool b_OVERFLOW_TF_outfifo_datacount        = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::TF_outfifo_datacount);
              bool b_OVERFLOW_exmem_nom_sector_data_count = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::exmem_nom_sector_data_count);
              bool b_OVERFLOW_exmem_pix_sector_data_count = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::exmem_pix_sector_data_count);
              bool b_OVERFLOW_exmem_sct_sector_data_count = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::exmem_sct_sector_data_count);
              bool b_OVERFLOW_exmem_trans_data_count      = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::exmem_trans_data_count);
              bool b_OVERFLOW_TF_header_fifo_datacount    = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::TF_header_fifo_datacount);
              bool b_OVERFLOW_TF_trailer_fifo_datacount   = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::TF_trailer_fifo_datacount);
              bool b_OVERFLOW_TF_nom_count_datacount      = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::TF_nom_count_datacount);
              bool b_OVERFLOW_TF_pix_count_datacount      = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::TF_pix_count_datacount);
              bool b_OVERFLOW_TF_sct_count_datacount      = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::TF_sct_count_datacount);
              bool b_OVERFLOW_ssid_fifo_datacount         = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::ssid_fifo_datacount);
              bool b_OVERFLOW_hcm_fifo_datacount          = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hcm_fifo_datacount);
              bool b_OVERFLOW_hlm_fifo_datacount0         = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount0);
              bool b_OVERFLOW_hlm_fifo_datacount1         = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount1);
              bool b_OVERFLOW_hlm_fifo_datacount2         = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount2);
              bool b_OVERFLOW_hlm_fifo_datacount3         = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount3);
              bool b_OVERFLOW_hlm_fifo_datacount4         = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount4);
              bool b_OVERFLOW_hlm_fifo_datacount5         = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount5);
              bool b_OVERFLOW_hlm_fifo_datacount6         = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount6);
              bool b_OVERFLOW_hlm_fifo_datacount7         = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount7);
              bool b_OVERFLOW_hlm_fifo_datacount8         = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount8);
              bool b_OVERFLOW_hlm_fifo_datacount9         = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount9);
              bool b_OVERFLOW_hlm_fifo_datacount10        = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount10);
              bool b_OVERFLOW_hlm_fifo_datacount11        = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount11);
              bool b_OVERFLOW_hlm_fifo_datacount12        = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount12);
              bool b_OVERFLOW_hlm_fifo_datacount13        = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount13);
              bool b_OVERFLOW_hlm_fifo_datacount14        = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount14);
              bool b_OVERFLOW_hlm_fifo_datacount15        = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount15);


              bool b_UNDERFLOW_stream_fifo_datacount1           = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount1);
              bool b_UNDERFLOW_stream_fifo_datacount2           = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount2);
              bool b_UNDERFLOW_stream_fifo_datacount3           = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount3);
              bool b_UNDERFLOW_stream_fifo_datacount4           = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount4);
              bool b_UNDERFLOW_df_module_id_ibl__datacount      = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_module_id_ibl__datacount);
              bool b_UNDERFLOW_df_module_id_sct1__datacount     = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_module_id_sct1__datacount);
              bool b_UNDERFLOW_df_module_id_sct2__datacount     = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_module_id_sct2__datacount);
              bool b_UNDERFLOW_df_ibl_hit_col_coord__datacount  = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_ibl_hit_col_coord__datacount);
              bool b_UNDERFLOW_df_ibl_hit_row_coord__datacount  = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_ibl_hit_row_coord__datacount);
              bool b_UNDERFLOW_df_sct_hit1_coord__datacount     = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_sct_hit1_coord__datacount);
              bool b_UNDERFLOW_df_sct_hit2_coord__datacount     = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::df_sct_hit2_coord__datacount);
              bool b_UNDERFLOW_aux_module_id_ibl__datacount     = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::aux_module_id_ibl__datacount);
              bool b_UNDERFLOW_aux_module_id_sct__datacount     = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::aux_module_id_sct__datacount);
              bool b_UNDERFLOW_aux_ibl_hit_col_coord__datacount = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::aux_ibl_hit_col_coord__datacount);
              bool b_UNDERFLOW_aux_ibl_hit_row_coord__datacount = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::aux_ibl_hit_row_coord__datacount);
              bool b_UNDERFLOW_aux_sct_hit_coord__datacount     = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::aux_sct_hit_coord__datacount);

              bool b_UNDERFLOW_TF_outfifo_datacount        = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::TF_outfifo_datacount);
              bool b_UNDERFLOW_exmem_nom_sector_data_count = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::exmem_nom_sector_data_count);
              bool b_UNDERFLOW_exmem_pix_sector_data_count = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::exmem_pix_sector_data_count);
              bool b_UNDERFLOW_exmem_sct_sector_data_count = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::exmem_sct_sector_data_count);
              bool b_UNDERFLOW_exmem_trans_data_count      = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::exmem_trans_data_count);
              bool b_UNDERFLOW_TF_header_fifo_datacount    = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::TF_header_fifo_datacount);
              bool b_UNDERFLOW_TF_trailer_fifo_datacount   = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::TF_trailer_fifo_datacount);
              bool b_UNDERFLOW_TF_nom_count_datacount      = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::TF_nom_count_datacount);
              bool b_UNDERFLOW_TF_pix_count_datacount      = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::TF_pix_count_datacount);
              bool b_UNDERFLOW_TF_sct_count_datacount      = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::TF_sct_count_datacount);
              bool b_UNDERFLOW_ssid_fifo_datacount         = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::ssid_fifo_datacount);
              bool b_UNDERFLOW_hcm_fifo_datacount          = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hcm_fifo_datacount);
              bool b_UNDERFLOW_hlm_fifo_datacount0         = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount0);
              bool b_UNDERFLOW_hlm_fifo_datacount1         = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount1);
              bool b_UNDERFLOW_hlm_fifo_datacount2         = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount2);
              bool b_UNDERFLOW_hlm_fifo_datacount3         = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount3);
              bool b_UNDERFLOW_hlm_fifo_datacount4         = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount4);
              bool b_UNDERFLOW_hlm_fifo_datacount5         = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount5);
              bool b_UNDERFLOW_hlm_fifo_datacount6         = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount6);
              bool b_UNDERFLOW_hlm_fifo_datacount7         = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount7);
              bool b_UNDERFLOW_hlm_fifo_datacount8         = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount8);
              bool b_UNDERFLOW_hlm_fifo_datacount9         = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount9);
              bool b_UNDERFLOW_hlm_fifo_datacount10        = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount10);
              bool b_UNDERFLOW_hlm_fifo_datacount11        = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount11);
              bool b_UNDERFLOW_hlm_fifo_datacount12        = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount12);
              bool b_UNDERFLOW_hlm_fifo_datacount13        = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount13);
              bool b_UNDERFLOW_hlm_fifo_datacount14        = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount14);
              bool b_UNDERFLOW_hlm_fifo_datacount15        = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount15);


              

              u_int f_processing_time_min = srsb.getStatusRaw(ssb_srsb_contents_extf::processing_time_min_r);
              u_int f_processing_time_max = srsb.getStatusRaw(ssb_srsb_contents_extf::processing_time_max_r);
              u_int n_dbg_hitfifo_error   = srsb.getStatusRaw(ssb_srsb_contents_extf::dbg_hitfifo_error);
              u_int n_dbg_eol_counter     = srsb.getStatusRaw(ssb_srsb_contents_extf::dbg_eol_counter); 
              float f_hxmpp_hlm_IBL_hitrate     = float(srsb.getStatusRaw(ssb_srsb_contents_extf::hxmpp_hlm_IBL_hitrate)) / SSB_EXTF_RATE_DENOMINATOR;
              float f_hxmpp_hlm_SCT1_hitrate    = float(srsb.getStatusRaw(ssb_srsb_contents_extf::hxmpp_hlm_SCT1_hitrate)) / SSB_EXTF_RATE_DENOMINATOR;
              float f_hxmpp_hlm_SCT2_hitrate    = float(srsb.getStatusRaw(ssb_srsb_contents_extf::hxmpp_hlm_SCT2_hitrate)) / SSB_EXTF_RATE_DENOMINATOR;
              float f_hxmpp_hlm_SCT3_hitrate    = float(srsb.getStatusRaw(ssb_srsb_contents_extf::hxmpp_hlm_SCT3_hitrate)) / SSB_EXTF_RATE_DENOMINATOR;


              u_int n_Count_SelRecIBL              = srsb.getStatusRaw(ssb_srsb_contents_extf::Count_SelRecIBL           );
              u_int n_Count_SelRecSCT1             = srsb.getStatusRaw(ssb_srsb_contents_extf::Count_SelRecSCT1          );
              u_int n_Count_SelRecSCT2             = srsb.getStatusRaw(ssb_srsb_contents_extf::Count_SelRecSCT2          );
              u_int n_Count_SelRecSCT3             = srsb.getStatusRaw(ssb_srsb_contents_extf::Count_SelRecSCT3          );
              u_int n_max_BP_dfa                   = srsb.getStatusRaw(ssb_srsb_contents_extf::max_BP_dfa                );
              u_int n_max_BP_dfb                   = srsb.getStatusRaw(ssb_srsb_contents_extf::max_BP_dfb                );
              u_int n_max_BP_auxa                  = srsb.getStatusRaw(ssb_srsb_contents_extf::max_BP_auxa               );
              u_int n_max_BP_auxb                  = srsb.getStatusRaw(ssb_srsb_contents_extf::max_BP_auxb               );

              u_int n_dfa_max_IBL_mod_per_event        = srsb.getStatusRaw(ssb_srsb_contents_extf::dfa_max_IBL_mod_per_event     );
              u_int n_dfa_max_SCT_mod_per_event        = srsb.getStatusRaw(ssb_srsb_contents_extf::dfa_max_SCT_mod_per_event     );
              float f_dfa_IBL_mod_rate                 = float(srsb.getStatusRaw(ssb_srsb_contents_extf::dfa_IBL_mod_rate  ) / SSB_EXTF_RATE_DENOMINATOR);
              float f_dfa_SCT_mod_rate                 = float(srsb.getStatusRaw(ssb_srsb_contents_extf::dfa_SCT_mod_rate  ) / SSB_EXTF_RATE_DENOMINATOR);
              u_int n_dfa_max_IBL_clusters_per_event   = srsb.getStatusRaw(ssb_srsb_contents_extf::dfa_max_IBL_clusters_per_event);
              u_int n_dfa_max_SCT_clusters_per_event   = srsb.getStatusRaw(ssb_srsb_contents_extf::dfa_max_SCT_clusters_per_event);
              float f_dfa_IBL_cluster_rate             = float(srsb.getStatusRaw(ssb_srsb_contents_extf::dfa_IBL_cluster_rate ) / SSB_EXTF_RATE_DENOMINATOR );
              float f_dfa_SCT_cluster_rate             = float(srsb.getStatusRaw(ssb_srsb_contents_extf::dfa_SCT_cluster_rate ) / SSB_EXTF_RATE_DENOMINATOR );

              u_int n_dfb_max_IBL_mod_per_event        = srsb.getStatusRaw(ssb_srsb_contents_extf::dfb_max_IBL_mod_per_event     );
              u_int n_dfb_max_SCT_mod_per_event        = srsb.getStatusRaw(ssb_srsb_contents_extf::dfb_max_SCT_mod_per_event     );
              float f_dfb_IBL_mod_rate                 = float(srsb.getStatusRaw(ssb_srsb_contents_extf::dfb_IBL_mod_rate  ) / SSB_EXTF_RATE_DENOMINATOR);
              float f_dfb_SCT_mod_rate                 = float(srsb.getStatusRaw(ssb_srsb_contents_extf::dfb_SCT_mod_rate  ) / SSB_EXTF_RATE_DENOMINATOR);
              u_int n_dfb_max_IBL_clusters_per_event   = srsb.getStatusRaw(ssb_srsb_contents_extf::dfb_max_IBL_clusters_per_event);
              u_int n_dfb_max_SCT_clusters_per_event   = srsb.getStatusRaw(ssb_srsb_contents_extf::dfb_max_SCT_clusters_per_event);
              float f_dfb_IBL_cluster_rate             = float(srsb.getStatusRaw(ssb_srsb_contents_extf::dfb_IBL_cluster_rate ) / SSB_EXTF_RATE_DENOMINATOR );
              float f_dfb_SCT_cluster_rate             = float(srsb.getStatusRaw(ssb_srsb_contents_extf::dfb_SCT_cluster_rate ) / SSB_EXTF_RATE_DENOMINATOR );


              uint n_aux_track_limit_hit_cnt  = srsb.getStatusRaw(ssb_srsb_contents_extf::aux_track_limit_hit_cnt );
              uint n_tf_track_limit_hit_cnt   = srsb.getStatusRaw(ssb_srsb_contents_extf::tf_track_limit_hit_cnt );
              uint n_tf_fit_limit_hit_nom_cnt = srsb.getStatusRaw(ssb_srsb_contents_extf::tf_fit_limit_hit_nom_cnt );
              uint n_tf_fit_limit_hit_pix_cnt = srsb.getStatusRaw(ssb_srsb_contents_extf::tf_fit_limit_hit_pix_cnt );
              uint n_tf_fit_limit_hit_sct_cnt = srsb.getStatusRaw(ssb_srsb_contents_extf::tf_fit_limit_hit_sct_cnt );
              uint n_track_count_max          = srsb.getStatusRaw(ssb_srsb_contents_extf::track_count_max);

              // Read information from fpga
              u_int N_SSB_EXTF_STATE = vme->read_word(SSB_EXTF_STATE);
              u_int N_EXTF_Links     = vme->read_word(SSB_EXTF_LINKS    );
              u_int N_EXTF_DF_LINK_DISPARITY_ERRORS   =   vme->read_word(SSB_EXTF_DF_LINK_DISPARITY_ERRORS  );
              u_int N_EXTF_DFA_LINK_DISPARITY_ERRORS = (N_EXTF_DF_LINK_DISPARITY_ERRORS & 0xFFFF0000) >> 16;
              u_int N_EXTF_DFB_LINK_DISPARITY_ERRORS = (N_EXTF_DF_LINK_DISPARITY_ERRORS & 0xFFFF);
              
              u_int N_EXTF_AUX_LINK_DISPARITY_ERRORS  =   vme->read_word( SSB_EXTF_AUX_LINK_DISPARITY_ERRORS  );
              u_int N_EXTF_AUXA_LINK_DISPARITY_ERRORS = (N_EXTF_AUX_LINK_DISPARITY_ERRORS & 0xFFFF0000) >> 16;
              u_int N_EXTF_AUXB_LINK_DISPARITY_ERRORS = (N_EXTF_AUX_LINK_DISPARITY_ERRORS & 0xFFFF);
              
              u_int N_EXTF_DF_LINK_NONINTABLE_ERRORS  =   vme->read_word( SSB_EXTF_DF_LINK_NONINTABLE_ERRORS );
              u_int N_EXTF_DFA_LINK_NONINTABLE_ERRORS = (N_EXTF_DF_LINK_NONINTABLE_ERRORS & 0xFFFF0000) >> 16;
              u_int N_EXTF_DFB_LINK_NONINTABLE_ERRORS = (N_EXTF_DF_LINK_NONINTABLE_ERRORS & 0xFFFF);
              
              u_int N_EXTF_AUX_LINK_NONINTABLE_ERRORS =   vme->read_word( SSB_EXTF_AUX_LINK_NONINTABLE_ERRORS );
              u_int N_EXTF_AUXA_LINK_NONINTABLE_ERRORS = (N_EXTF_AUX_LINK_NONINTABLE_ERRORS & 0xFFFF0000) >> 16;
              u_int N_EXTF_AUXB_LINK_NONINTABLE_ERRORS = (N_EXTF_AUX_LINK_NONINTABLE_ERRORS & 0xFFFF);
              
              
              u_int N_EXTF_EventsIn  = vme->read_word(SSB_EXTF_EVENTSIN );
              u_int N_EXTF_EventsOut = vme->read_word(SSB_EXTF_EVENTSOUT);
              u_int N_EXTF_TracksEXO = vme->read_word(SSB_EXTF_TRACKSEXO);
              u_int N_EXTF_TracksNom = vme->read_word(SSB_EXTF_TRACKSNOM);
              u_int N_EXTF_TracksPix = vme->read_word(SSB_EXTF_TRACKSPIX);
              u_int N_EXTF_TracksSct = vme->read_word(SSB_EXTF_TRACKSSCT);
              
              u_int N_EXTF_CountBpEXP  = vme->read_word(SSB_EXTF_COUNTBPEXP);
              u_int N_EXTF_CountBpDFA  = vme->read_word(SSB_EXTF_COUNTBPDFA);
              u_int N_EXTF_CountBpDFB  = vme->read_word(SSB_EXTF_COUNTBPDFB);
              u_int N_EXTF_CountBpAUXA = vme->read_word(SSB_EXTF_COUNTBPAUXA);
              u_int N_EXTF_CountBpAUXB = vme->read_word(SSB_EXTF_COUNTBPAUXB);
              u_int N_EXTF_CountBpHW   = vme->read_word(SSB_EXTF_COUNTBPHW );
              u_int N_EXTF_FullReg     = vme->read_word(SSB_EXTF_FULLREG   );
              
              u_int N_LVLID_DFA      = vme->read_word(SSB_LVLID_DFA     );        
              u_int N_LVLID_DFB      = vme->read_word(SSB_LVLID_DFB     );  
              u_int N_LVLID_AUXA     = vme->read_word(SSB_LVLID_AUXA     );  
              u_int N_LVLID_AUXB     = vme->read_word(SSB_LVLID_AUXB     );  
              u_int N_ERR_LVLID_DFA  = vme->read_word(SSB_ERR_LVLID_DFA );  
              u_int N_ERR_LVLID_DFB  = vme->read_word(SSB_ERR_LVLID_DFB );  
              u_int N_ERR_LVLID_AUXA = vme->read_word(SSB_ERR_LVLID_AUXA ); 
              u_int N_ERR_LVLID_AUXB = vme->read_word(SSB_ERR_LVLID_AUXB ); 
              
              float N_EXTF_RATEIN   =  float(vme->read_word(SSB_EXTF_RATEIN)) / SSB_EXTF_RATE_DENOMINATOR;
              float N_EXTF_RATEOUT  =  float(vme->read_word(SSB_EXTF_RATEOUT)) / SSB_EXTF_RATE_DENOMINATOR;
              float N_EXTF_RATENOM  =  float(vme->read_word(SSB_EXTF_RATENOM)) / SSB_EXTF_RATE_DENOMINATOR;
              float N_EXTF_RATEPIX  =  float(vme->read_word(SSB_EXTF_RATEPIX)) / SSB_EXTF_RATE_DENOMINATOR;
              float N_EXTF_RATESCT  =  float(vme->read_word(SSB_EXTF_RATESCT)) / SSB_EXTF_RATE_DENOMINATOR;
              
              float N_EXTF_DFABPFRAC  = float(vme->read_word(SSB_EXTF_DFABPFRAC))  / (N_EXTF_FREQ == 0xeffdeffd ? SSB_EXTF_BP_DIVISOR : N_EXTF_FREQ);
              float N_EXTF_DFBBPFRAC  = float(vme->read_word(SSB_EXTF_DFBBPFRAC))  / (N_EXTF_FREQ == 0xeffdeffd ? SSB_EXTF_BP_DIVISOR : N_EXTF_FREQ);
              float N_EXTF_AUXABPFRAC = float(vme->read_word(SSB_EXTF_AUXABPFRAC)) / (N_EXTF_FREQ == 0xeffdeffd ? SSB_EXTF_BP_DIVISOR : N_EXTF_FREQ);
              float N_EXTF_AUXBBPFRAC = float(vme->read_word(SSB_EXTF_AUXBBPFRAC)) / (N_EXTF_FREQ == 0xeffdeffd ? SSB_EXTF_BP_DIVISOR : N_EXTF_FREQ);
                
              u_int N_EXTF_FREEZE_REASON = vme->read_word(SSB_EXTF_FREEZE_MENU); 
              u_int N_EXTF_RESETS = vme->read_word(SSB_EXTF_RESET); 
              u_int N_EXTF_AUX_EXTRA_WORD = vme->read_word(SSB_EXTF_AUX_EXTRA_WORD);
              
              u_int N_SSB_EXTF_SYNC_PROC_EVENTS_COUNT_DFA  = vme->read_word(SSB_EXTF_SYNC_PROC_EVENTS_COUNT_DFA);
              u_int N_SSB_EXTF_SYNC_PROC_EVENTS_COUNT_DFB  = vme->read_word(SSB_EXTF_SYNC_PROC_EVENTS_COUNT_DFB);
              u_int N_SSB_EXTF_SYNC_PROC_EVENTS_COUNT_AUXA = vme->read_word(SSB_EXTF_SYNC_PROC_EVENTS_COUNT_AUXA);
              u_int N_SSB_EXTF_SYNC_PROC_EVENTS_COUNT_AUXB = vme->read_word(SSB_EXTF_SYNC_PROC_EVENTS_COUNT_AUXB);

              u_int N_SSB_EXTF_SYNC_DISC_EVENTS_COUNT_DFA  = vme->read_word(SSB_EXTF_SYNC_DISC_EVENTS_COUNT_DFA);
              u_int N_SSB_EXTF_SYNC_DISC_EVENTS_COUNT_DFB  = vme->read_word(SSB_EXTF_SYNC_DISC_EVENTS_COUNT_DFB);
              u_int N_SSB_EXTF_SYNC_DISC_EVENTS_COUNT_AUXA = vme->read_word(SSB_EXTF_SYNC_DISC_EVENTS_COUNT_AUXA);
              u_int N_SSB_EXTF_SYNC_DISC_EVENTS_COUNT_AUXB = vme->read_word(SSB_EXTF_SYNC_DISC_EVENTS_COUNT_AUXB);

              // State debug information
              u_int N_SSB_EXTF_EXP_STATE = vme->read_word(SSB_EXTF_EXP_STATE);
              u_int N_SSB_EXTF_READY = vme->read_word(SSB_EXTF_READY);
              u_int N_SSB_EXTF_MASTER_STATE = vme->read_word(SSB_EXTF_MASTER_STATE);
              u_int N_SSB_EXTF_SE_STATE   = vme->read_word(SSB_EXTF_SE_STATE);
              u_int N_SSB_EXTF_TFW_FULL = vme->read_word(SSB_EXTF_TFW_FULL);


              u_int N_SSB_EXTF_EOR_DF       = vme->read_word(SSB_EXTF_EOR_DF);
              u_int N_SSB_EXTF_EOR_AUX      = vme->read_word(SSB_EXTF_EOR_AUX);
              u_int N_SSB_EXTF_EOP_DF       = vme->read_word(SSB_EXTF_EOP_DF);
              u_int N_SSB_EXTF_EOP_AUX      = vme->read_word(SSB_EXTF_EOP_AUX);
              u_int N_SSB_EXTF_CNT_TRAILER  = vme->read_word(SSB_EXTF_CNT_TRAILER);
              u_int N_SSB_EXTF_EOL          = vme->read_word(SSB_EXTF_EOL);
              u_int N_SSB_EXTF_LAST_TTOTF   = vme->read_word(SSB_EXTF_LAST_TTOTF);
              u_int N_SSB_EXTF_LAST_TTOTF_E = vme->read_word(SSB_EXTF_LAST_TTOTF_E);
              u_int N_SSB_EXTF_CNT_L1ID_OUT = vme->read_word(SSB_EXTF_CNT_L1ID_OUT);
              u_int N_SSB_EXTF_CNT_L1ID_OUT_E = vme->read_word(SSB_EXTF_CNT_L1ID_OUT_E);


              u_int ERRORS = vme->read_word(SSB_ERROR_REGISTER); 
              
              
              
              bool dfa_link  = N_EXTF_Links & 0x1; 
              bool dfb_link  = N_EXTF_Links & 0x8;
              bool auxa_link = N_EXTF_Links & AUX_A_MASK;
              bool auxb_link = N_EXTF_Links & AUX_B_MASK;
              
              // Start printing info
              std::cout<< "Links ("<<std::hex << N_EXTF_Links<<std::dec<<") to AUX (A,B) = (" << auxa_link <<","<<auxb_link  << "), to DF (A,B) = (" << dfa_link <<","<<dfb_link<<")  State = " << N_SSB_EXTF_STATE << std::endl;  

              // Sync engine info
              std::cout << "Sync Engine:"
                        << "   "             << std::setw(12) << "DFA"                         << std::setw(12) << "DFB"                        
                                             << std::setw(12) << "AUXA"                        << std::setw(12) << "AUXB"  <<std::endl;

              std::cout << "  N Processed: " << std::hex << std::setw(12) << N_SSB_EXTF_SYNC_PROC_EVENTS_COUNT_DFA                        << std::setw(12) << N_SSB_EXTF_SYNC_PROC_EVENTS_COUNT_DFB                       
                                             << std::setw(12) << N_SSB_EXTF_SYNC_PROC_EVENTS_COUNT_AUXA                       << std::setw(12) << N_SSB_EXTF_SYNC_PROC_EVENTS_COUNT_AUXB << std::endl;              

              std::cout << "  N Discarded: " << std::hex << std::setw(12) << N_SSB_EXTF_SYNC_DISC_EVENTS_COUNT_DFA                        << std::setw(12) << N_SSB_EXTF_SYNC_DISC_EVENTS_COUNT_DFB                       
                                             << std::setw(12) << N_SSB_EXTF_SYNC_DISC_EVENTS_COUNT_AUXA                       << std::setw(12) << N_SSB_EXTF_SYNC_DISC_EVENTS_COUNT_AUXB << std::endl<< std::endl;              
              

              std::cout<< "# Events in the EXP (hit aux trk limit)" << std::hex << std::setw(10) <<N_EXTF_EventsIn  << " @ " << std::dec << std::fixed << std::setprecision(2) << N_EXTF_RATEIN  << " kHz ("<<std::hex<<n_aux_track_limit_hit_cnt<<")" << std::endl;
              std::cout<< "# Events out of the TF                 " << std::hex << std::setw(10) <<N_EXTF_EventsOut << " @ " << std::dec << std::fixed << std::setprecision(2) << N_EXTF_RATEOUT << " kHz" << std::endl;
              std::cout<< "# Total tracks out of the EXP (tf lmt) " << std::hex << std::setw(10) <<N_EXTF_TracksEXO << " ("<< n_tf_track_limit_hit_cnt <<")"<< std::endl;
              std::cout<< "# IBL/SCT1/2/3 hit rates               " << std::fixed << std::setprecision(2) << std::setw(10) << f_hxmpp_hlm_IBL_hitrate << " / " << std::setw(10) << f_hxmpp_hlm_SCT1_hitrate << " / " << std::setw(10) << f_hxmpp_hlm_SCT2_hitrate<< " / " << std::setw(10) << f_hxmpp_hlm_SCT3_hitrate<< " kHz"<<std::endl;
              std::cout<< "# Total tracks going in the Nom Fitter " << std::hex << std::setw(10) <<N_EXTF_TracksNom << " @ " << std::dec << std::fixed << std::setprecision(2) << N_EXTF_RATENOM << " kHz ("<<n_tf_fit_limit_hit_nom_cnt <<")" << std::endl;
              std::cout<< " -> Recovery Fits IBL/SCT5/7/11        " << std::hex << std::setw(10) << n_Count_SelRecIBL << " / " << std::setw(10) << n_Count_SelRecSCT1 << " / " <<  std::setw(10) << n_Count_SelRecSCT2 << " / " <<  std::setw(10) << n_Count_SelRecSCT3  << std::endl;        
              std::cout<< "# Total tracks going in the Pix Fitter " << std::hex << std::setw(10) <<N_EXTF_TracksPix << " @ " << std::dec << std::fixed << std::setprecision(2) << N_EXTF_RATEPIX << " kHz ("<<n_tf_fit_limit_hit_pix_cnt <<")" << std::endl;
              std::cout<< "# Total tracks going in the SCT Fitter " << std::hex << std::setw(10) <<N_EXTF_TracksSct << " @ " << std::dec << std::fixed << std::setprecision(2) << N_EXTF_RATESCT << " kHz ("<<  n_tf_fit_limit_hit_sct_cnt <<")" << std::endl;
              std::cout<< "Max 12L tracks / event                 " << std::hex << std::setw(10) << n_track_count_max<<std::endl;

              std::cout<< "Max module per event IBL / SCT (dfa)   " << std::hex << std::setw(10) <<n_dfa_max_IBL_mod_per_event << " @ " << std::dec << std::fixed << std::setprecision(2) << f_dfa_IBL_mod_rate << " kHz / "
                                                                    << std::hex << std::setw(10) <<n_dfa_max_SCT_mod_per_event << " @ " << std::dec << std::fixed << std::setprecision(2) << f_dfa_SCT_mod_rate << " kHz"
                                                                    << std::endl
                       << "                               (dfb)   " << std::hex << std::setw(10) <<n_dfb_max_IBL_mod_per_event << " @ " << std::dec << std::fixed << std::setprecision(2) << f_dfb_IBL_mod_rate << " kHz / "
                                                                    << std::hex << std::setw(10) <<n_dfb_max_SCT_mod_per_event << " @ " << std::dec << std::fixed << std::setprecision(2) << f_dfb_SCT_mod_rate  << " kHz" << std::endl;
              std::cout<< "Max cluster per event IBL / SCT (dfa)  " << std::hex << std::setw(10) <<n_dfa_max_IBL_clusters_per_event << " @ " << std::dec << std::fixed << std::setprecision(2) << f_dfa_IBL_cluster_rate << " kHz / "
                                                                    << std::hex << std::setw(10) <<n_dfa_max_SCT_clusters_per_event << " @ " << std::dec << std::fixed << std::setprecision(2) << f_dfa_SCT_cluster_rate << " kHz"
                                                                    << std::endl
                       << "                                (dfb)  " << std::hex << std::setw(10) <<n_dfb_max_IBL_clusters_per_event << " @ " << std::dec << std::fixed << std::setprecision(2) << f_dfb_IBL_cluster_rate << " kHz / "
                                                                    << std::hex << std::setw(10) <<n_dfb_max_SCT_clusters_per_event << " @ " << std::dec << std::fixed << std::setprecision(2) << f_dfb_SCT_cluster_rate << " kHz" << std::endl;
                                                                    
              std::cout<< "Min / Max Event Processing Time        " << std::setw(10) << std::hex<< f_processing_time_min << " / " << std::setw(10) << f_processing_time_max <<std::endl << std::endl;

              
              bool BP_DFA  = N_EXTF_FullReg & 0x1;
              bool BP_DFB  = N_EXTF_FullReg & 0x2;
              bool BP_AUXA = N_EXTF_FullReg & 0x4;
              bool BP_AUXB = N_EXTF_FullReg & 0x8;
              bool BP_EXP  = N_EXTF_FullReg & 0x100;
              bool BP_HW   = N_EXTF_FullReg & 0x200;
              
              
              std::cout << "Full Registers: " << std::setw(10) << std::hex << N_EXTF_FullReg    <<  "  (BP is FROM HW and EXP)" << std::endl;
              std::cout << "             " << std::setw(12) << "DFA"                         << std::setw(12) << "DFB"                        
                                           << std::setw(12) << "AUXA"                        << std::setw(12) << "AUXB"  
                                           << std::setw(12) << "EXP"           << std::setw(12) << "HW" <<std::endl;
              
              std::cout << "  BP to:     " << std::setw(12) << BP_DFA                        << std::setw(12) << BP_DFB                       
                                           << std::setw(12) << BP_AUXA                       << std::setw(12) << BP_AUXB 
                                           << std::setw(12) << BP_EXP                        << std::setw(12) << BP_HW << std::endl;
              
              std::cout << "  Sum BP to: " << std::setw(12) << std::hex<< N_EXTF_CountBpDFA  << std::setw(12) << std::hex<< N_EXTF_CountBpDFB 
                                           << std::setw(12) << std::hex<< N_EXTF_CountBpAUXA << std::setw(12) << std::hex<< N_EXTF_CountBpAUXB
                                           << std::setw(12) << std::hex<< N_EXTF_CountBpEXP  << std::setw(12) << std::hex<< N_EXTF_CountBpHW << std::endl;
              
              std::cout << "  Frc BP to: " << std::fixed << std::setprecision(2)
                                           << std::setw(12) << N_EXTF_DFABPFRAC  << std::setw(12) << N_EXTF_DFBBPFRAC
                                           << std::setw(12) << N_EXTF_AUXABPFRAC << std::setw(12) << N_EXTF_AUXBBPFRAC
                                           << std::endl;
              std::cout << "  Max BP to: " << std::hex << std::setw(12) << n_max_BP_dfa  << std::setw(12) << n_max_BP_dfb
                                                       << std::setw(12) << n_max_BP_auxa << std::setw(12) << n_max_BP_auxb
                                           << std::endl;
              std::cout << "  FIFO Full: " << std::fixed << std::setprecision(2) << std::dec
                                           << std::setw(6) << f_stream_fifo_datacount1 << " ("<<b_OVERFLOW_stream_fifo_datacount1<<"/"<<b_UNDERFLOW_stream_fifo_datacount1<<")"
                                           << std::setw(6) << f_stream_fifo_datacount2 << " ("<<b_OVERFLOW_stream_fifo_datacount2<<"/"<<b_UNDERFLOW_stream_fifo_datacount2<<")"
                                           << std::setw(6) << f_stream_fifo_datacount3 << " ("<<b_OVERFLOW_stream_fifo_datacount3<<"/"<<b_UNDERFLOW_stream_fifo_datacount3<<")"
                                           << std::setw(6) << f_stream_fifo_datacount4 << " ("<<b_OVERFLOW_stream_fifo_datacount4<<"/"<<b_UNDERFLOW_stream_fifo_datacount4<<")"
                                           << std::endl;


              
              std::cout << "  LVL1ID:    " << std::setw(12) << std::hex<< N_LVLID_DFA                   << std::setw(12) << N_LVLID_DFB
                                           << std::setw(12) << std::hex<< N_LVLID_AUXA                  << std::setw(12) << N_LVLID_AUXB << std::endl; 
              
              std::cout << "  LVL1ID Err:" << std::setw(12) << N_ERR_LVLID_DFA               << std::setw(12) << N_ERR_LVLID_DFB
                                           << std::setw(12) << N_ERR_LVLID_AUXA              << std::setw(12) << N_ERR_LVLID_AUXB << std::endl; 
              
              std::cout << "  Dispar Err:" << std::setw(12) << N_EXTF_DFA_LINK_DISPARITY_ERRORS   << std::setw(12) << N_EXTF_DFB_LINK_DISPARITY_ERRORS
                                           << std::setw(12) << N_EXTF_AUXA_LINK_DISPARITY_ERRORS      << std::setw(12) << N_EXTF_AUXB_LINK_DISPARITY_ERRORS  << std::endl;
              
              std::cout << "  Nonint Err:" << std::setw(12) << N_EXTF_DFA_LINK_NONINTABLE_ERRORS   << std::setw(12) << N_EXTF_DFB_LINK_NONINTABLE_ERRORS
                                           << std::setw(12) << N_EXTF_AUXA_LINK_NONINTABLE_ERRORS      << std::setw(12) << N_EXTF_AUXB_LINK_NONINTABLE_ERRORS  << std::endl << std::endl; 
              
              
              
              std::cout << "Freeze reason: " << std::hex << N_EXTF_FREEZE_REASON << std::endl;
              std::cout << "N FPGA Resets: " << std::hex << N_EXTF_RESETS << std::endl;
              if (!noDebug)
              {
                  std::cout << "____________DEBUGGING INFO____________ " <<std::endl;
                  std::cout << "Extra word from AUX: " << std::hex << N_EXTF_AUX_EXTRA_WORD << std::endl;
                  
                  std::cout << "Ready Status (TFW, AUX, DFP, EXP, TF): " 
                            << ((N_SSB_EXTF_READY >> 0) & 0x1) << ", " 
                            << ((N_SSB_EXTF_READY >> 1) & 0x1) << ", "
                            << ((N_SSB_EXTF_READY >> 2) & 0x1) << ", "
                            << ((N_SSB_EXTF_READY >> 3) & 0x1) << ", "
                            << ((N_SSB_EXTF_READY >> 4) & 0x1) << std::endl;
                  std::cout << "EXP state machines (AUX, DFP, DFW, TFO, Lookup, LMT): " << std::hex 
                            << ((N_SSB_EXTF_EXP_STATE >> 0) & 0xfff) << ", " 
                            << ((N_SSB_EXTF_EXP_STATE >> 12) & 0xf)  << ", " 
                            << ((N_SSB_EXTF_EXP_STATE >> 16) & 0xf)  << ", " 
                            << ((N_SSB_EXTF_EXP_STATE >> 20) & 0xf)  << ", " 
                            << ((N_SSB_EXTF_EXP_STATE >> 24) & 0x3f) << ", "
                            << ((N_SSB_EXTF_EXP_STATE >> 30) & 0x3) << std::endl;
                  std::cout << "SE state machines (framepack, sync, build, empties): " << std::hex
                            << ((N_SSB_EXTF_SE_STATE >> 0)  & 0xf) << ", "
                            << ((N_SSB_EXTF_SE_STATE >> 4)  & 0x7) << ", "
                            << ((N_SSB_EXTF_SE_STATE >> 16) & 0x7) << ", "
                            << ((N_SSB_EXTF_SE_STATE >> 20) & 0x7) << std::endl;
                  std::cout << "Master state (hxm, se, lookup, even, odd, trailer, eop DF, eop AUX): "  //(exec SSID hxm, exec read SE, end of lookup, reset done even, reset done odd, trailer out, end of packet DF, end of packet AUX) 
                            << ((N_SSB_EXTF_MASTER_STATE >> 0) & 0x1) << ", "
                            << ((N_SSB_EXTF_MASTER_STATE >> 1) & 0x1) << ", "
                            << ((N_SSB_EXTF_MASTER_STATE >> 2) & 0x1) << ", "
                            << ((N_SSB_EXTF_MASTER_STATE >> 3) & 0x1) << ", "
                            << ((N_SSB_EXTF_MASTER_STATE >> 4) & 0x1) << ", "
                            << ((N_SSB_EXTF_MASTER_STATE >> 5) & 0x1) << ", "
                            << ((N_SSB_EXTF_MASTER_STATE >> 6) & 0x1) << ", "
                            << ((N_SSB_EXTF_MASTER_STATE >> 7) & 0x1) << std::endl;
                  
                  std::cout << "Debug counters that should be equal: " << std::endl <<std::hex
                            <<  "EOR DF:     " << std::setw(9) << N_SSB_EXTF_EOR_DF   << " "
                            <<  "EOR AUX:    " << std::setw(9) << N_SSB_EXTF_EOR_AUX  << " "
                            <<  "EOP DF:     " << std::setw(9) << N_SSB_EXTF_EOP_DF   << " "
                            <<  "EOP AUX:    " << std::setw(9) << N_SSB_EXTF_EOP_AUX  << " "         
                            <<  "CNT Trl:    " << std::setw(9) << N_SSB_EXTF_CNT_TRAILER << " "   << std::endl
                            <<  "EOL:        " << std::setw(9) << N_SSB_EXTF_EOL         << " "  
                            <<  "TtoTF:      " << std::setw(9) << N_SSB_EXTF_LAST_TTOTF   << " "
                            <<  "ToTF_E:     " << std::setw(9) << N_SSB_EXTF_LAST_TTOTF_E << " "
                            <<  "CNT L1ID:   " << std::setw(9) << N_SSB_EXTF_CNT_L1ID_OUT << " "
                            <<  "CNT L1ID_E: " << std::setw(9) << N_SSB_EXTF_CNT_L1ID_OUT_E << std::endl;
                  
                  std::cout << "FIFO Fullness (Over/Underflow Flag): " 
                            << " -- HLM, HCM, SSID FIFOs full: " << std::hex
                            << ((N_SSB_EXTF_TFW_FULL >> 0)  & 0xffff) << ", "
                            << ((N_SSB_EXTF_TFW_FULL >> 16) & 0xf) << ", "
                            << ((N_SSB_EXTF_TFW_FULL >> 20) & 0x1) << std::endl
                            << std::fixed << std::setprecision(2) 
                            << "  DF MOD IBL:  " << std::setw(3) << f_df_module_id_ibl__datacount  << " ("<<b_OVERFLOW_df_module_id_ibl__datacount  <<"/"<<b_UNDERFLOW_df_module_id_ibl__datacount  <<")"
                            << ", DF MOD SCT1: " << std::setw(3) << f_df_module_id_sct1__datacount<< " ("<<b_OVERFLOW_df_module_id_sct1__datacount<<"/"<<b_UNDERFLOW_df_module_id_sct1__datacount<<")"
                            << ", DF MOD SCT2: " << std::setw(3) << f_df_module_id_sct2__datacount<< " ("<<b_OVERFLOW_df_module_id_sct2__datacount<<"/"<<b_UNDERFLOW_df_module_id_sct2__datacount<<")"
                            << ", DF IBL COL:  " << std::setw(3) << f_df_ibl_hit_col_coord__datacount << " ("<<b_OVERFLOW_df_ibl_hit_col_coord__datacount <<"/"<<b_UNDERFLOW_df_ibl_hit_col_coord__datacount <<")" <<std::endl
                            << "  DF IBL ROW:  " << std::setw(3) << f_df_ibl_hit_row_coord__datacount<< " ("<<b_OVERFLOW_df_ibl_hit_row_coord__datacount<<"/"<<b_UNDERFLOW_df_ibl_hit_row_coord__datacount<<")"
                            << ", DF SCT HIT1: " << std::setw(3) << f_df_sct_hit1_coord__datacount<< " ("<<b_OVERFLOW_df_sct_hit1_coord__datacount<<"/"<<b_UNDERFLOW_df_sct_hit1_coord__datacount<<")"
                            << ", DF SCT HIT2: " << std::setw(3) << f_df_sct_hit2_coord__datacount<< " ("<<b_OVERFLOW_df_sct_hit2_coord__datacount<<"/"<<b_UNDERFLOW_df_sct_hit2_coord__datacount<<")"
                            << ", AUX MOD IBL: " << std::setw(3) << f_aux_module_id_ibl__datacount << " ("<<b_OVERFLOW_aux_module_id_ibl__datacount <<"/"<<b_UNDERFLOW_aux_module_id_ibl__datacount <<")" << std::endl
                            << "  AUX MOD SCT: " << std::setw(3) << f_aux_module_id_sct__datacount<< " ("<<b_OVERFLOW_aux_module_id_sct__datacount<<"/"<<b_UNDERFLOW_aux_module_id_sct__datacount<<")"
                            << ", AUX IBL COL: " << std::setw(3) << f_aux_ibl_hit_col_coord__datacount<< " ("<<b_OVERFLOW_aux_ibl_hit_col_coord__datacount<<"/"<<b_UNDERFLOW_aux_ibl_hit_col_coord__datacount<<")"
                            << ", AUX IBL ROW: " << std::setw(3) << f_aux_ibl_hit_row_coord__datacount<< " ("<<b_OVERFLOW_aux_ibl_hit_row_coord__datacount<<"/"<<b_UNDERFLOW_aux_ibl_hit_row_coord__datacount<<")"
                            << ", AUX SCT CRD: " << std::setw(3) << f_aux_sct_hit_coord__datacount << " ("<<b_OVERFLOW_aux_sct_hit_coord__datacount <<"/"<<b_UNDERFLOW_aux_sct_hit_coord__datacount <<")" << std::endl
                            << "  TF OUTFIFIO: " << std::setw(3) << f_TF_outfifo_datacount             << " ("<< b_OVERFLOW_TF_outfifo_datacount             <<"/"<< b_UNDERFLOW_TF_outfifo_datacount             <<")"
                            << ", Exmem Nom:   " << std::setw(3) << f_exmem_nom_sector_data_count      << " ("<< b_OVERFLOW_exmem_nom_sector_data_count      <<"/"<< b_UNDERFLOW_exmem_nom_sector_data_count      <<")"
                            << ", Exmem Pix:   " << std::setw(3) << f_exmem_pix_sector_data_count      << " ("<< b_OVERFLOW_exmem_pix_sector_data_count      <<"/"<< b_UNDERFLOW_exmem_pix_sector_data_count      <<")"
                            << ", Exmem SCT:   " << std::setw(3) << f_exmem_sct_sector_data_count     << " ("<< b_OVERFLOW_exmem_sct_sector_data_count       <<"/"<< b_UNDERFLOW_exmem_sct_sector_data_count      <<")"<< std::endl
                            << "  Exmem Trans: " << std::setw(3) << f_exmem_trans_data_count           << " ("<< b_OVERFLOW_exmem_trans_data_count           <<"/"<< b_UNDERFLOW_exmem_trans_data_count           <<")"
                            << ", TF header:   " << std::setw(3) << f_TF_header_fifo_datacount         << " ("<< b_OVERFLOW_TF_header_fifo_datacount         <<"/"<< b_UNDERFLOW_TF_header_fifo_datacount         <<")"
                            << ", TF trailer:  " << std::setw(3) << f_TF_trailer_fifo_datacount        << " ("<< b_OVERFLOW_TF_trailer_fifo_datacount        <<"/"<< b_UNDERFLOW_TF_trailer_fifo_datacount        <<")"
                            << ", TF Nom:      " << std::setw(3) << f_TF_nom_count_datacount           << " ("<< b_OVERFLOW_TF_nom_count_datacount           <<"/"<< b_UNDERFLOW_TF_nom_count_datacount           <<")"<< std::endl
                            << "  TF Pix:      " << std::setw(3) << f_TF_pix_count_datacount           << " ("<< b_OVERFLOW_TF_pix_count_datacount           <<"/"<< b_UNDERFLOW_TF_pix_count_datacount           <<")"
                            << ", TF SCT:      " << std::setw(3) << f_TF_sct_count_datacount           << " ("<< b_OVERFLOW_TF_sct_count_datacount           <<"/"<< b_UNDERFLOW_TF_sct_count_datacount           <<")"
                            << ", SSID:        " << std::setw(3) << f_ssid_fifo_datacount              << " ("<< b_OVERFLOW_ssid_fifo_datacount              <<"/"<< b_UNDERFLOW_ssid_fifo_datacount              <<")"
                            << ", HCM:         " << std::setw(3) << f_hcm_fifo_datacount               << " ("<< b_OVERFLOW_hcm_fifo_datacount               <<"/"<< b_UNDERFLOW_hcm_fifo_datacount               <<")"<< std::endl
                            << "  HLM0:        " << std::setw(3) << f_hlm_fifo_datacount0              << " ("<< b_OVERFLOW_hlm_fifo_datacount0              <<"/"<< b_UNDERFLOW_hlm_fifo_datacount0              <<")"
                            << ", HLM1:        " << std::setw(3) << f_hlm_fifo_datacount1              << " ("<< b_OVERFLOW_hlm_fifo_datacount1              <<"/"<< b_UNDERFLOW_hlm_fifo_datacount1              <<")"
                            << ", HLM2:        " << std::setw(3) << f_hlm_fifo_datacount2              << " ("<< b_OVERFLOW_hlm_fifo_datacount2              <<"/"<< b_UNDERFLOW_hlm_fifo_datacount2              <<")"
                            << ", HLM3:        " << std::setw(3) << f_hlm_fifo_datacount3              << " ("<< b_OVERFLOW_hlm_fifo_datacount3              <<"/"<< b_UNDERFLOW_hlm_fifo_datacount3              <<")"<< std::endl
                            << "  HLM4:        " << std::setw(3) << f_hlm_fifo_datacount4              << " ("<< b_OVERFLOW_hlm_fifo_datacount4              <<"/"<< b_UNDERFLOW_hlm_fifo_datacount4              <<")"
                            << ", HLM5:        " << std::setw(3) << f_hlm_fifo_datacount5              << " ("<< b_OVERFLOW_hlm_fifo_datacount5              <<"/"<< b_UNDERFLOW_hlm_fifo_datacount5              <<")"
                            << ", HLM6:        " << std::setw(3) << f_hlm_fifo_datacount6              << " ("<< b_OVERFLOW_hlm_fifo_datacount6              <<"/"<< b_UNDERFLOW_hlm_fifo_datacount6              <<")"
                            << ", HLM7:        " << std::setw(3) << f_hlm_fifo_datacount7              << " ("<< b_OVERFLOW_hlm_fifo_datacount7              <<"/"<< b_UNDERFLOW_hlm_fifo_datacount7              <<")"<< std::endl
                            << "  HLM8:        " << std::setw(3) << f_hlm_fifo_datacount8              << " ("<< b_OVERFLOW_hlm_fifo_datacount8              <<"/"<< b_UNDERFLOW_hlm_fifo_datacount8              <<")"
                            << ", HLM9:        " << std::setw(3) << f_hlm_fifo_datacount9              << " ("<< b_OVERFLOW_hlm_fifo_datacount9              <<"/"<< b_UNDERFLOW_hlm_fifo_datacount9              <<")"
                            << ", HLM10:       " << std::setw(3) << f_hlm_fifo_datacount10             << " ("<< b_OVERFLOW_hlm_fifo_datacount10             <<"/"<< b_UNDERFLOW_hlm_fifo_datacount10             <<")"
                            << ", HLM11:       " << std::setw(3) << f_hlm_fifo_datacount11             << " ("<< b_OVERFLOW_hlm_fifo_datacount11             <<"/"<< b_UNDERFLOW_hlm_fifo_datacount11             <<")"<< std::endl
                            << "  HLM12:       " << std::setw(3) << f_hlm_fifo_datacount12             << " ("<< b_OVERFLOW_hlm_fifo_datacount12             <<"/"<< b_UNDERFLOW_hlm_fifo_datacount12             <<")"
                            << ", HLM13:       " << std::setw(3) << f_hlm_fifo_datacount13             << " ("<< b_OVERFLOW_hlm_fifo_datacount13             <<"/"<< b_UNDERFLOW_hlm_fifo_datacount13             <<")"
                            << ", HLM14:       " << std::setw(3) << f_hlm_fifo_datacount14             << " ("<< b_OVERFLOW_hlm_fifo_datacount14             <<"/"<< b_UNDERFLOW_hlm_fifo_datacount14             <<")"
                            << ", HLM15:       " << std::setw(3) << f_hlm_fifo_datacount15             << " ("<< b_OVERFLOW_hlm_fifo_datacount15             <<"/"<< b_UNDERFLOW_hlm_fifo_datacount15             <<")" << std::endl;
                  
                  
                  std::cout << "Packet Errors Flag (to be developed) " << std::hex << ERRORS <<std::endl;
                  std::cout << "dbg_hitfifo / dbg_eol " << std::hex << n_dbg_hitfifo_error << " / "<< n_dbg_eol_counter <<std::endl;
              }
        }
          
        // *********************************************************
        // HitWarrior
        // ********************************************************

        if (fpga_number[fpgaid] ==4){ // This is hitwarrior info
        
            ssb_srsb srsb(vme, false); // tell the srsb this is a hitwarrior
            if (!fast){ // Readout status register spybuffer                                                                              
                srsb.update();
            }
            // parse srsb                                                                                                                 
            float f_track_rate = float(srsb.getStatusRaw(ssb_srsb_contents_hw::track_rate)) / SSB_HW_RATE_DENOMINATOR;
            float f_event_rate = float(srsb.getStatusRaw(ssb_srsb_contents_hw::event_rate)) / SSB_HW_RATE_DENOMINATOR;

            uint n_eventsout = srsb.getStatusRaw(ssb_srsb_contents_hw::eventsout);
            uint n_tracksout = srsb.getStatusRaw(ssb_srsb_contents_hw::tracksout);

            float f_extf0_fifo_full = srsb.getStatus<float>(ssb_srsb_contents_hw::extf0_fifo); //extf0_fifo_datacount_s;
            float f_extf1_fifo_full = srsb.getStatus<float>(ssb_srsb_contents_hw::extf1_fifo); //extf1_fifo_datacount_s;
            float f_extf2_fifo_full = srsb.getStatus<float>(ssb_srsb_contents_hw::extf2_fifo); //extf2_fifo_datacount_s;
            float f_extf3_fifo_full = srsb.getStatus<float>(ssb_srsb_contents_hw::extf3_fifo); //extf3_fifo_datacount_s;
            float f_flic_fifo_full  = srsb.getStatus<float>(ssb_srsb_contents_hw::flic_fifo ); //flic_fifo_datacount_s;
            uint n_processing_time_min_s = srsb.getStatusRaw(ssb_srsb_contents_hw::processing_time_min);
            uint n_processing_time_max_s = srsb.getStatusRaw(ssb_srsb_contents_hw::processing_time_max);

            bool b_OVERFLOW_extf0_fifo_full = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_hw::extf0_fifo); //extf0_fifo_datacount_s;
            bool b_OVERFLOW_extf1_fifo_full = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_hw::extf1_fifo); //extf1_fifo_datacount_s;
            bool b_OVERFLOW_extf2_fifo_full = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_hw::extf2_fifo); //extf2_fifo_datacount_s;
            bool b_OVERFLOW_extf3_fifo_full = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_hw::extf3_fifo); //extf3_fifo_datacount_s;
            bool b_OVERFLOW_flic_fifo_full  = SSB_SRSB_FIFO_OVERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_hw::flic_fifo ); //flic_fifo_datacount_s;

            bool b_UNDERFLOW_extf0_fifo_full = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_hw::extf0_fifo); //extf0_fifo_datacount_s;
            bool b_UNDERFLOW_extf1_fifo_full = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_hw::extf1_fifo); //extf1_fifo_datacount_s;
            bool b_UNDERFLOW_extf2_fifo_full = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_hw::extf2_fifo); //extf2_fifo_datacount_s;
            bool b_UNDERFLOW_extf3_fifo_full = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_hw::extf3_fifo); //extf3_fifo_datacount_s;
            bool b_UNDERFLOW_flic_fifo_full  = SSB_SRSB_FIFO_UNDERFLOW_MASK & srsb.getStatusRaw(ssb_srsb_contents_hw::flic_fifo ); //flic_fifo_datacount_s;




            float N_HW_EVENT_RATE   = float(vme->read_word(SSB_HW_EVENT_RATE)) / SSB_HW_RATE_DENOMINATOR;
            float N_HW_TRACK_RATE   = float(vme->read_word(SSB_HW_TRACK_RATE) ) / SSB_HW_RATE_DENOMINATOR;
            float N_HW_FLIC_BPFRAC  = float(vme->read_word(SSB_HW_FLIC_BPFRAC) ) / SSB_HW_BP_DIVISOR;
            
            u_int N_HW_EventsIn  = vme->read_word(SSB_HW_EVENTSIN );
            u_int N_HW_EventsOut = vme->read_word(SSB_HW_EVENTSOUT); 
            u_int N_HW_TracksOut = vme->read_word(SSB_HW_TRACKSOUT);
            u_int N_HW_FullReg   = vme->read_word(SSB_HW_FULLREG);
            u_int N_HW_CountBP   = vme->read_word(SSB_HW_COUNTBP);
            u_int N_HW_packet_count_SE              = vme->read_word(SSB_HW_PACKET_COUNT_SE           );
            u_int N_HW_RAW_Err_counter_lvl1id       = vme->read_word(SSB_HW_RAW_ERR_COUNTER_LVL1ID    );
            u_int N_HW_SE_Prev_lvl1id               = vme->read_word(SSB_HW_SE_PREV_LVL1ID                  );
            u_int N_HW_SE_Prev_lvl1id_sync          = vme->read_word(SSB_HW_SE_PREV_LVL1ID_SYNC     );
            u_int N_HW_SE_Err_counter_lvl1id_0      = vme->read_word(SSB_HW_SE_ERR_COUNTER_LVL1ID_0   );
            u_int N_HW_SE_Err_counter_lvl1id_1      = vme->read_word(SSB_HW_SE_ERR_COUNTER_LVL1ID_1   );
            u_int N_HW_SE_Err_counter_lvl1id_2      = vme->read_word(SSB_HW_SE_ERR_COUNTER_LVL1ID_2   );
            u_int N_HW_SE_Err_counter_lvl1id_3      = vme->read_word(SSB_HW_SE_ERR_COUNTER_LVL1ID_3   );
            u_int N_HW_FquIn_Prev_lvl1_id           = vme->read_word(SSB_HW_FQUIN_PREV_LVL1_ID      );
            u_int N_HW_FquIn_Err_counter_lvl1_id    = vme->read_word(SSB_HW_FQUIN_ERR_COUNTER_LVL1_ID );
            u_int N_HW_FquOut_Prev_lvl1_id          = vme->read_word(SSB_HW_FQUOUT_PREV_LVL1_ID     );
            u_int N_HW_FquOut_Err_counter_lvl1_id   = vme->read_word(SSB_HW_FQUOUT_ERR_COUNTER_LVL1_ID);
            
            u_int HW_FLIC_FC = vme->read_word(SSB_HW_FLIC_FC);      
            
            u_int HW_IGNORE_FLIC_BP = vme->read_word(SSB_ADDR_IGNOREBP);      

            u_int HW_FREEZE_REASON = vme->read_word(SSB_HW_FREEZE_MENU); 

            u_int HW_VCCINT = vme->read_word( SSB_HW_VCCINT ); 
            u_int HW_VCCAUX = vme->read_word( SSB_HW_VCCAUX ); 
            u_int HW_VPVN = vme->read_word( SSB_HW_VPVN ); 
            u_int HW_VREFP = vme->read_word( SSB_HW_VREFP ); 
            u_int HW_VREFN = vme->read_word( SSB_HW_VREFN ); 
            u_int HW_VCCBRAM = vme->read_word( SSB_HW_VCCBRAM ); 
            
            //u_int HW_pSSB_LINK     = 0;

            /*std::cout<< "VCCINT=" << std::dec << std::setprecision(2) << 3.*HW_VCCINT/4096  
                     << ", VCCAUX=" << std::dec << std::setprecision(2) << 3.*HW_VCCAUX/4096
                     << ", VPVN=" << std::dec << std::setprecision(2) << 3.*HW_VPVN/4096
                     << ", VREFP=" << std::dec << std::setprecision(2) << 3.*HW_VREFP/4096
                     << ", VREFN=" << std::dec << std::setprecision(2) << 3.*HW_VREFN/4096
                     << ", VCCBRAM=" << std::dec << std::setprecision(2) << 3.*HW_VCCBRAM/4096
                     << std::endl;
            */
            //std::cout<<"pSSB-fSSB Link=                "        << HW_pSSB_LINK << std::endl;
            std::cout<< "# Events in the HW=           "        << std::hex << N_HW_EventsIn  << std::endl;
            std::cout<< "# Events out of the HW=       "        << std::hex << N_HW_EventsOut << " @ " << std::dec << std::fixed << std::setprecision(2) << N_HW_EVENT_RATE << " kHz" << std::endl;
            std::cout<<"Min / Max Proc Time            "        << std::hex << std::setw(8) << n_processing_time_min_s << " / " << std::setw(8) << n_processing_time_max_s << std::endl;
            std::cout<< "# Total tracks out of the HW= "        << std::hex << N_HW_TracksOut << " @ " << std::dec << std::fixed << std::setprecision(2) << N_HW_TRACK_RATE << " kHz" << std::endl;

            std::cout<< "Flic BP (ignored="<<bool(HW_IGNORE_FLIC_BP) <<") " << std::hex << N_HW_FullReg - 0xfb000000 << ", cumulative = "<< std::hex << std::setw(8) <<  N_HW_CountBP - 0xfbc00000 << " w/Frac = " << std::dec << std::setprecision(2) << N_HW_FLIC_BPFRAC << " out_fifo_full = "<<f_flic_fifo_full << " (" << b_OVERFLOW_flic_fifo_full << "/" << b_UNDERFLOW_flic_fifo_full   << ")"<< std::endl; // remove label fb from register

            std::cout << "Last FLIC FC Word               " << std::hex <<     HW_FLIC_FC                        <<std::endl; 
            std::cout << "N_HW_packet_count_SE            " << std::hex <<     N_HW_packet_count_SE              <<std::endl;
            std::cout << "N_HW_SE_Prev_lvl1id             " << std::hex <<     N_HW_SE_Prev_lvl1id               <<std::endl; 
            std::cout << "N_HW_SE_Prev_lvl1id_sync        " << std::hex <<     N_HW_SE_Prev_lvl1id_sync          <<std::endl;
            std::cout << "N_HW_RAW_Err_counter_lvl1id     " << std::hex <<     N_HW_RAW_Err_counter_lvl1id       <<std::endl;
            std::cout << "FIFO fullnesss (over/under)     " << std::dec << std::setprecision(2) << std::setw(3) << f_extf0_fifo_full << " (" << b_OVERFLOW_extf0_fifo_full << "/" << b_UNDERFLOW_extf0_fifo_full <<")"
                                                            << std::dec << std::setprecision(2) << std::setw(3) << f_extf1_fifo_full << " (" << b_OVERFLOW_extf1_fifo_full << "/" << b_UNDERFLOW_extf1_fifo_full <<")"
                                                            << std::dec << std::setprecision(2) << std::setw(3) << f_extf2_fifo_full << " (" << b_OVERFLOW_extf2_fifo_full << "/" << b_UNDERFLOW_extf2_fifo_full <<")"
                                                            << std::dec << std::setprecision(2) << std::setw(3) << f_extf3_fifo_full << " (" << b_OVERFLOW_extf3_fifo_full << "/" << b_UNDERFLOW_extf3_fifo_full <<")" << std::endl;

            std::cout << "HW SE Error l1id per ch         " << std::hex << std::setw(10) <<  N_HW_SE_Err_counter_lvl1id_0 << std::setw(10) <<  N_HW_SE_Err_counter_lvl1id_1 << std::setw(10) <<  N_HW_SE_Err_counter_lvl1id_2 << std::setw(10) <<  N_HW_SE_Err_counter_lvl1id_3 << std::endl;



            std::cout << "N_HW_FquIn_Prev_lvl1_id         " << std::hex <<     N_HW_FquIn_Prev_lvl1_id           <<std::endl; 
            std::cout << "N_HW_FquIn_Err_counter_lvl1_id  " << std::hex <<     N_HW_FquIn_Err_counter_lvl1_id    <<std::endl;
            std::cout << "N_HW_FquOut_Prev_lvl1_id        " << std::hex <<     N_HW_FquOut_Prev_lvl1_id          <<std::endl; 
            std::cout << "N_HW_FquOut_Err_counter_lvl1_id " << std::hex <<     N_HW_FquOut_Err_counter_lvl1_id   <<std::endl;
            std::cout << "Freeze reason: " << std::hex << HW_FREEZE_REASON << std::endl;        
        }

    } catch ( daq::ftk::VmeError& e ) {
        cout <<"VME_Error: Failed to contact FPGA: "<<fpga_number[fpgaid] << " Maybe it isn't programmed." << std::endl;    
    } catch ( daq::ftk::CMEMError& e ) {
        cout <<"CMEM_Error: Failed to contact FPGA: "<<fpga_number[fpgaid] << std::endl;
    }

  }
 

  return 0;
}
