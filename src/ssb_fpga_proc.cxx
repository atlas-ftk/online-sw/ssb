#include "ftkvme/VMEInterface.h"
//#include "ftkvme/VMEManager.h"

#include <boost/crc.hpp>

#include "ssb/ssb_fpga_proc.h"
#include "ssb/ssb_vme_addr.h"
#include "ssb/ssb.h"
#include <ssb/SsbFitConstants.hh>
#include <ssb/SSBVMEloader.h>

#include <ftkcommon/FtkCool.hh>

#include <fstream>
#include <iostream>
#include <sstream>

#include <chrono>
#include <thread>
using Clock=std::chrono::high_resolution_clock;

namespace daq
{
  namespace ftk
  {

    ssb_fpga_proc::ssb_fpga_proc(uint slot, uint fpga) :
      ssb_fpga(slot, fpga)
    {
      ERS_LOG("Constructing ssb_fpga_proc with slot " << slot << " and fpga " << fpga );
    }

    ssb_fpga_proc::~ssb_fpga_proc()
    {
      ERS_LOG("Destructor of ssb_fpga_proc called" );
    }


      void ssb_fpga_proc::configure(unsigned int runNumber, std::map<uint32_t, std::unique_ptr<SsbFitConstants> >& db_constants, bool useCool)
    {    

        // Change the FPGA FSM to configured
        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                m_vme->write_word( SSB_ADDR_CONFIG, SSB_COMMAND); // This line triggers internal state transitions in the firmware. The FIFOs, state machines, GTX links are reset upon receiving this vme command. 
                std::this_thread::sleep_for(std::chrono::milliseconds(100)); // make sure internal fpga configure finishes, this is a guess of the time based on the mem init working
                break;
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to send configure command to FPGA.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to send configure command to FPGA.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            break;
        }


        // Load constants if needed
	if(useCool && daq::ftk::isLoadMode()) 
        { // load via database
	   SSBVMEloader loader(m_vme);
           
           std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	   if(m_LoadEXPConstants) {
	     loader.loadTKF(db_constants[getTower()]->getTFconstants());
	   }
           std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
           std::chrono::duration<double> time_span = t2 - t1;
           ERS_LOG( " Scalability: >>>> Loaded EXP constants from COOL in "<< time_span.count() << " seconds. <<<< " );

           t1 = std::chrono::high_resolution_clock::now();
	   if(m_LoadTFConstants)  {  
	     loader.loadEXP(db_constants[getTower()]->getEXPconstants());
	   }
           t2 = std::chrono::high_resolution_clock::now();
           time_span = t2 - t1;
           ERS_LOG( " Scalability: >>>> Loaded TF constants from COOL in "<< time_span.count() << " seconds. <<<< " );


	} else if (daq::ftk::isLoadMode())  { // load via text file
            if (m_LoadTFConstants && m_LoadEXPConstants)
            {
                // VME error catching is in these functions
                initializeMemory(); // this does memory initialization in parallel
                loadTFConstants();
                loadEXPConstants();

            } else if (m_LoadTFConstants)
	    {
                // VME error catching is in these functions
                initializeTFMemory();
                loadTFConstants();              
	    } else if (m_LoadEXPConstants)
	    {
                // VME error catching is in these functions
                initializeEXPMemory();
                loadEXPConstants();              
	    }
	}

        // Load SSMAPs, always for now since it's fast and there are no checksums
        if (daq::ftk::isLoadMode())
        {
            // VME error catching is in this function
            loadSSMaps();   // TODO: there is no checksum for this, need to think about what to do here.
        }    

        // Configure FPGA settings
        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                // Set registers on the FPGA as needed
	        if(m_conf->get_IgnoreBP()){
                    m_vme->write_word(SSB_ADDR_EXT_IGNOREBPHW, 0x00000001);
                }
                if(m_conf->get_EmulatorMode()) {
                    m_vme->write_word(SSB_ADDR_SLINKMODE, 0x00000001);
                }else  m_vme->write_word(SSB_ADDR_SLINKMODE, 0x00000000);

                // Send configuration to FPGA via VME      
                m_vme->write_word(SSB_EXTF_AUXDUP_REMOVAL, m_conf->get_AUXDupRemoval());  //Aux duplicate track removal
        
                m_vme->write_word(SSB_EXTF_FREEZE_MENU, m_conf->get_FreezeMask());    // freeze mask

                uint32_t auxdelay = (m_conf->get_AUXBDelay() << 16) || m_conf->get_AUXADelay();
                m_vme->write_word(SSB_EXTF_AUX_DELAY, auxdelay); // aux delay in spybuffer freeze

                uint32_t dftfdelay = (m_conf->get_TFDelay() << 16 ) || m_conf->get_DFBDelay();
                m_vme->write_word(SSB_EXTF_TFDFB_DELAY, dftfdelay); // tf delay in spybuffer freeze

                m_vme->write_word(SSB_EXTF_FREEZE_DFPACKETLENGTH, m_conf->get_DFPacketFreezeLength());
                m_vme->write_word(SSB_EXTF_FREEZE_AUXTRACKCOUNT, m_conf->get_AUXPacketFreezeLength());
                m_vme->write_word(SSB_EXTF_FREEZE_TFTRACKCOUNT, m_conf->get_TFPacketFreezeLength());

                m_vme->write_word(SSB_EXTF_RESET, m_conf->get_FPGAReset()); // fpga reset setting

                m_vme->write_word(SSB_EXTF_LINKID, m_LinkID); // fpga linkID

                m_vme->write_word(SSB_EXTF_TIMEOUT_THRESH, m_conf->get_TimeoutThreshold()); // sync engine timeout threshold
      
                m_vme->write_word(SSB_EXTF_IGNOREDFB, m_conf->get_IgnoreDFB());
                m_vme->write_word(SSB_EXTF_IGNOREAUXB, m_conf->get_IgnoreAUXB());
      
                m_vme->write_word(SSB_EXTF_AUXEXP_TRACKLIMITS, ((m_conf->get_AUXTrackLimit() << 16) & 0x03ff0000) | (m_conf->get_EXPTrackLimit() & 0x000003ff)  ); // aux and exp track limits

                // SSMAP configuration
                m_vme->write_word(SSB_EXTF_DIVISOR_ETA_IBL , m_conf->get_Divisor_Eta_IBL());
                m_vme->write_word(SSB_EXTF_DIVISOR_PHI_IBL , m_conf->get_Divisor_Phi_IBL());
                m_vme->write_word(SSB_EXTF_DIVISOR_PHI_SCT , m_conf->get_Divisor_Phi_SCT());
                m_vme->write_word(SSB_EXTF_NSSID_PHI_IBL , m_conf->get_NSSID_Phi_IBL());
                m_vme->write_word(SSB_EXTF_NSSID_SCT , m_conf->get_NSSID_SCT());

                // Chi2 cut
                m_vme->write_word(SSB_EXTF_CHI2_CUT , m_conf->get_Chi2_Cut());
                m_vme->write_word(SSB_EXTF_REC_CHI2_CUT , m_conf->get_Chi2_Cut_Recovery());
                break;
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to set all configuration registers.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    if (daq::ftk::isFatalMode()) throw excp;
                    else ers::error(excp);
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to set all configuration registers.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    if (daq::ftk::isFatalMode()) throw excp;
                    else ers::error(excp);
                }
                continue;
            }
            break;
        } // end vme access try-catch loop

    }

    void ssb_fpga_proc::connect()
    {      
        ERS_LOG("ssb_fpga_proc::connect()" );

        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                m_vme->write_word( SSB_ADDR_CONNECT, SSB_COMMAND);               
                std::this_thread::sleep_for(std::chrono::milliseconds(100)); // make sure internal fpga configure finishes, this is a guess of the time based on the mem init working
                break;
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to send connect command to FPGA.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to send connect command to FPGA.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            break;
        }
    }


    void ssb_fpga_proc::prepareForRun()
    {
        ERS_LOG("ssb_fpga_proc::prepareForRun()" );

        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                m_vme->write_word( SSB_ADDR_START, SSB_COMMAND); 
                std::this_thread::sleep_for(std::chrono::milliseconds(100)); // make sure internal fpga configure finishes, this is a guess of the time based on the mem init working
                break;
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to send prepareForRun command to FPGA.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to send prepareForRun command to FPGA.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            break;
        }
    }


    void ssb_fpga_proc::stop()
    {
        ERS_LOG("ssb_fpga_proc::stop()" );

        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                m_vme->write_word( SSB_ADDR_STOP, SSB_COMMAND); 
                std::this_thread::sleep_for(std::chrono::milliseconds(100)); // make sure internal fpga configure finishes, this is a guess of the time based on the mem init working
                break;
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to send stop commadn to FPGA.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to send stop commadn to FPGA.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            break;
        }
    }

    void ssb_fpga_proc::disconnect()
    {
        ERS_LOG("ssb_fpga_proc::disconnect()" );

        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                m_vme->write_word( SSB_ADDR_DISCONNECT, SSB_COMMAND); 
                std::this_thread::sleep_for(std::chrono::milliseconds(100)); // make sure internal fpga configure finishes, this is a guess of the time based on the mem init working
                break;
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to send disconnect command to FPGA.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to send disconnect command to FPGA.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            break;
        }
    }

    void ssb_fpga_proc::unconfigure()
    {        
        ERS_LOG("ssb_fpga_proc::unconfigure()" );

        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                m_vme->write_word( SSB_ADDR_UNCONFIG, SSB_COMMAND); 
                std::this_thread::sleep_for(std::chrono::milliseconds(100)); // make sure internal fpga configure finishes, this is a guess of the time based on the mem init working
                break;
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to send unconfigure command to FPGA.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to send unconfigure command to FPGA.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            break;
        }
    }

    /*
    bool ssb_fpga_proc::check_fw_version(uint32_t version_number)
    {
      ERS_LOG("ssb_fpga_proc::check_fw_version(" << version_number << ")" );
      return version_number;
    }

    void ssb_fpga_proc:: set_upstream_flow(bool enable)
    {
      ERS_LOG("ssb_fpga_proc::set_upstream_flow(" << enable << ")" );
    }

    void ssb_fpga_proc:: set_downstream_flow(bool enable)
    {
      ERS_LOG("ssb_fpga_proc::set_downstream_flow(" << enable << ")" );
    }

    void ssb_fpga_proc::setup_high_speed()
    {
      ERS_LOG("ssb_fpga_proc::setup_high_speed()" );
    }

    void ssb_fpga_proc::rx_link_idle()
    {
      ERS_LOG("ssb_fpga_proc::rx_link_idle()" );
      }*/

    void ssb_fpga_proc::config_ram_lut()
    {
      ERS_LOG("ssb_fpga_proc::config_ram_lut()" );
    }
      

    void ssb_fpga_proc::reset()
    {
      ERS_LOG("ssb_fpga_proc::reset()" );
    }

    bool ssb_fpga_proc::getLinkStatus(u_int &linkStatus)
    {
        u_int status = 0x0;
        
        bool retVal = false;
        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                status = m_vme->read_word(SSB_EXTF_LINKS); 
                break;
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to get Link status.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    if (daq::ftk::isFatalMode()) throw excp;
                    else ers::error(excp);
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to get Link status.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    if (daq::ftk::isFatalMode()) throw excp;
                    else ers::error(excp);
                }
                continue;
            }
            retVal = true;
            break;
        }

        linkStatus = status;
        return retVal;
    }

    bool ssb_fpga_proc::initializeMemory()
    {
        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                m_vme->write_word( SSB_EXP_MEM_INIT, 0x1);
                m_vme->write_word( SSB_TF_MEM_INIT, 0x1);

                // need to see a 0 to know it's done
                int max_tries = 100;
                int tries = 0;
                uint32_t readWord = 1;
                while (readWord !=0 && tries < max_tries)
                {
                    std::this_thread::sleep_for(std::chrono::milliseconds(50)); // exp timing only, taken from below
                    readWord  = m_vme->read_word(SSB_EXP_MEM_INIT);       
                    readWord |= m_vme->read_word(SSB_TF_MEM_INIT);       
                    tries++;
                }

                if (readWord == 0) return true;
                else 
                {
                    std::stringstream message;
                    message << "SSB memory initialization timed out! Checksum may be bad.";
                    daq::ftk::FTKOperationTimedOut excp(ERS_HERE, name_ftk(), message.str());
                    ers::error(excp);

                    return false;
                }
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to initialize memory.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp; // throw already here.  We know the checksum won't come out right if the memory wasn't initialized, even if the constants are loaded correctly
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to initialize memory.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp; // throw already here.  We know the checksum won't come out right if the memory wasn't initialized, even if the constants are loaded correctly
                }
                continue;
            }
            break; // shouldn't actually ever get to this line as it will either return something or throw an exception above
        }

        return false; // nor this line, but it removes a compile-time warning
    }


    bool ssb_fpga_proc::initializeTFMemory()
    {
        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                m_vme->write_word( SSB_TF_MEM_INIT, 0x1);

                // need to see a 0 to know it's done
                int max_tries = 100;
                int tries = 0;
                uint32_t readWord = 1;
                while (readWord !=0 && tries < max_tries)
                {
                    std::this_thread::sleep_for(std::chrono::milliseconds(50)); // exp timing only, taken from below
                    readWord = m_vme->read_word(SSB_TF_MEM_INIT);       
                    tries++;
                }

                if (readWord == 0) return true;
                else 
                {
                    std::stringstream message;
                    message << "SSB TF memory initialization timed out! Checksum may be bad.";
                    daq::ftk::FTKOperationTimedOut excp(ERS_HERE, name_ftk(), message.str());
                    ers::error(excp);

                    return false;
                }
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB TF failed to initialize memory.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp; // throw already here.  We know the checksum won't come out right if the memory wasn't initialized, even if the constants are loaded correctly
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB TF failed to initialize memory.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp; // throw already here.  We know the checksum won't come out right if the memory wasn't initialized, even if the constants are loaded correctly
                }
                continue;
            }
            break; // shouldn't actually ever get to this line as it will either return something or throw an exception above
        }

        return false; // nor this line, but it removes a compile-time warning
    }

    bool ssb_fpga_proc::initializeEXPMemory()
    {
        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                m_vme->write_word( SSB_EXP_MEM_INIT, 0x1);

                // need to see a 0 to know it's done
                int max_tries = 100;
                int tries = 0;
                uint32_t readWord = 1;
                while (readWord !=0 && tries < max_tries)
                {
                    std::this_thread::sleep_for(std::chrono::milliseconds(50)); // exp timing only, taken from below
                    readWord = m_vme->read_word(SSB_EXP_MEM_INIT);       
                    tries++;
                }

                if (readWord == 0) return true;
                else 
                {
                    std::stringstream message;
                    message << "SSB EXP memory initialization timed out! Checksum may be bad.";
                    daq::ftk::FTKOperationTimedOut excp(ERS_HERE, name_ftk(), message.str());
                    ers::error(excp);

                    return false;
                }
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB EXP failed to initialize memory.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp; // throw already here.  We know the checksum won't come out right if the memory wasn't initialized, even if the constants are loaded correctly
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB EXP failed to initialize memory.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp; // throw already here.  We know the checksum won't come out right if the memory wasn't initialized, even if the constants are loaded correctly
                }
                continue;
            }
            break; // shouldn't actually ever get to this line as it will either return something or throw an exception above
        }

        return false; // nor this line, but it removes a compile-time warning
    }


    bool ssb_fpga_proc::loadEXPConstants()
    {
        bool result = false;

        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try 
	      {
		  result = daq::ftk::ssb_const_load_from_txt( m_vme, m_conf->get_EXPPath(), "", m_name );
	      }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES) 
                {
                    std::stringstream message;
                    message << "SSB failed to load EXP constants.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES) 
                {
                    std::stringstream message;
                    message << "SSB failed to load EXP constants.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            break;
        }     

        return result;
    }

    bool ssb_fpga_proc::loadTFConstants()
    {
        bool result = false;

        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try 
	      {
		  result = daq::ftk::ssb_const_load_from_txt( m_vme, "", m_conf->get_TFPath(), m_name );
	      }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES) 
                {
                    std::stringstream message;
                    message << "SSB failed to load TF constants.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES) 
                {
                    std::stringstream message;
                    message << "SSB failed to load TF constants.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            break;
        }     

        return result;
    }





    bool ssb_fpga_proc::loadSSMaps()
    {
        bool result = false;
        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try 
            {
	      result = daq::ftk::ssb_ssmap_load_from_txt( m_vme, m_conf->get_SSMapAUXPix(), m_conf->get_SSMapAUXSCT(), m_conf->get_SSMapDFPix(), m_conf->get_SSMapDFSCT(), m_name); 
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES) 
                {
                    std::stringstream message;
                    message << "SSB failed to load SSMaps.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES) 
                {
                    std::stringstream message;
                    message << "SSB failed to load SSMaps.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    throw excp;
                }
                continue;
            }
            break;
        }     
        return result;
    }

    void ssb_fpga_proc::getHWChecksums(uint32_t &EXP_Checksum, uint32_t &TF_Checksum)
    {
        // Retrive checksum from hardware
        EXP_Checksum = 0xFFFFFFFF;
        TF_Checksum = 0xFFFFFFFF;


        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try 
            {
                //auto t1 = Clock::now();

                // Initiate checksum calculation
                m_vme->write_word( SSB_DO_EXP_CHECKSUM, 0x1);
                m_vme->write_word( SSB_DO_TF_CHECKSUM, 0x1); 
                std::this_thread::sleep_for(std::chrono::milliseconds(100)); // exp timing only



                // Read value
                EXP_Checksum = m_vme->read_word( SSB_EXP_CHECKSUM);
                TF_Checksum = m_vme->read_word( SSB_TF_CHECKSUM); 

                // Checksum is all Fs until ready
                int max_tries = 100;
                int tries = 0;
                while ( (EXP_Checksum == 0xFFFFFFFF || TF_Checksum==0xFFFFFFFF) and tries < max_tries){
                    //while ( (EXP_Checksum == 0xFFFFFFFF) && tries < max_tries){
                    EXP_Checksum = m_vme->read_word( SSB_EXP_CHECKSUM);
                    TF_Checksum = m_vme->read_word( SSB_TF_CHECKSUM); 
                    //std::cout<<"tries:"<<std::dec<<tries<<std::endl;
                    //std::cout<<"exp checksum"<<std::hex <<EXP_Checksum<<std::endl;
                    tries++;
                    // todo, maybe need a sleep?
                    std::this_thread::sleep_for(std::chrono::milliseconds(1));
                    //std::cout<<"duration:"<<std::dec<<std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - t1).count() <<std::endl;
                }
                if (tries == max_tries) {
                    std::stringstream message;
                    message << "SSB timed out on checksum.";
                    daq::ftk::FTKOperationTimedOut excp(ERS_HERE, name_ftk(), message.str());
                    ers::error(excp);
                }

            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES) 
                {
                    std::stringstream message;
                    message << "SSB failed to get checksums.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES) 
                {
                    std::stringstream message;
                    message << "SSB failed to get checksums.";
                    daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            break;
        }     
    }
    
     
    void ssb_fpga_proc::getSWChecksums(uint32_t &EXPChecksum, uint32_t &TFChecksum, bool useCool)
    {
        if (useCool)
        {
            

        } else {
            TFChecksum  = m_conf->get_TFChecksum();
            EXPChecksum = m_conf->get_EXPChecksum();
        } 
    }


    uint ssb_fpga_proc::getRCState()
    {
        uint result = -1;

        // VME Access try-catch loop
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try 
            {
                result = m_vme->read_word(SSB_EXTF_STATE);
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES) 
                {
                    std::stringstream message;
                    message << "SSB failed to get RC state.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES) 
                {
                    std::stringstream message;
                    message << "SSB failed to get RC state.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            break;
        }     


        return result;
    }


  }
}
