#include <chrono>

#include <Eigen/Dense>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cassert>
#include <cstdio>
#include <cmath>
#include <bitset>
#include <map>
#include <vector>
#include <algorithm>
#include <sstream>
#include <utility>
#include "ssb/SsbFitConstants.hh"


using namespace std;

uint32_t SsbFitConstants::checkSumTF() {
  uint32_t temp;
  uint32_t last_sector=0;
  int32_t delta;
  uint32_t crc=0xffffffff;
  uint32_t zero=0;
  const std::vector<uint32_t> &vals=TFKconstants;
  unsigned len=vals.size()/64/6;
  if(len>4095) len=4096;
  std::vector<uint32_t> o;
  for(unsigned i=0;i<len;i++) {
    for(unsigned j=0;j<2;j++) { 
      for(unsigned step=0;step<6;step++) {
        unsigned n=60;
        if(step==5) n=20;
        for(unsigned k=0;k<n;k++) crc=doCRC(vals[i*6*64 + step*64 +k+1 ],crc);
      }
      for(unsigned k=0;k<160;k++) crc=doCRC(0,crc);
    }
  }
  return crc;
}


uint32_t SsbFitConstants::checkSumEXP() { 
  uint32_t temp;
  uint32_t last_sector=0;
  int32_t delta;
  uint32_t crc=0xffffffff;

  const std::vector<uint32_t> &vals=EXPconstants;
  uint32_t nsectors=vals.size()/64;
  for(uint32_t i=0;i<nsectors;i++) {
    uint32_t sector=vals[i*64];
    delta=sector-last_sector-1;
    for(int32_t j=0;j<delta*60;j++) crc=doCRC(0,crc);
    for(uint32_t j=0;j<60;j++) {
      crc=doCRC(vals.data()[i*64+1+j],crc);
    }
    last_sector=sector;
  }

  for(uint32_t i=0;i<(65535-last_sector)*60;i++) crc=doCRC(0,crc);
  return crc;
}

void SsbFitConstants::calculate() {  
  std::vector<std::pair <int, int> > vecOfMapSecID;
  std::vector<std::pair <int, int> > vecOfMapNconn;
  std::vector<std::vector<int>> moduleIDvec;
  unsigned Max_1stStage_sectors = 16383;
  for(unsigned int isec=0;isec<m_sectorMap.getNSectors();isec++){
    int ssector=m_sectorMap.getNSimilarSectors(isec);
    for(unsigned int Nconn=0; Nconn< ssector; Nconn++){
      if(Nconn >3 ) break;
      int TwelveLsecid = m_sectorMap.getSimilarSecID(isec,Nconn); 

      std::pair<int,int> Map_secid_Nconn(TwelveLsecid,isec);
      //     Map_secid_Nconn[TwelveLsecid] = isec;
      std::pair<int,int> Map_temp(TwelveLsecid,(int)m_sectorMap.getNSimilarSectors(isec));
      //     Map_temp[TwelveLsecid] = (int)sector->getNSimilarSectors(isec) ;
      vecOfMapNconn.push_back(Map_temp);
      vecOfMapSecID.push_back(Map_secid_Nconn);
      std::vector<int> module_temp;
      module_temp.clear();
      for (int ip=0; ip!=(completeNPlanes-nPlanes); ++ip) {
        const std::vector<unsigned> &similar=m_sectorMap.getSimilarStereoIDs(isec,Nconn);
	//	if(ip>=similar.size()) {std::cout << "ip="<<ip << std::endl;
	//	 std::cout << "sz="<<similar.size() << std::endl;}
     
        module_temp.push_back(similar[ip]);
      }
      moduleIDvec.push_back(module_temp);
    }
  }

  for(unsigned int isec=0;isec<getNSectors();isec++){
    calcTFConstant(isec,TFKconstants);
  }
  int previous =-1;
  
  unsigned int counter=0;
  for(unsigned i=0;i<vecOfMapSecID.size()-1;i++) {
    std::pair<int,int> nConnMap=vecOfMapNconn[i];
    std::pair<int,int> secMap=vecOfMapSecID[i];
    unsigned int isec=secMap.first;
    unsigned int nconn=secMap.second;
    unsigned int count_Nconn=nConnMap.second;

    if(count_Nconn>3) count_Nconn=4;
    if(previous!=nconn) counter=0;
    unsigned int Nconn=i;
    
    //            std::cout <<"isec= "<<isec << ", nconn= " << nconn << ", count_Nconn= " << count_Nconn << " it.second =" << nconn << " counter="<< counter <<std::endl;
    if(nconn<Max_1stStage_sectors)          
      calcExtrapolationConstant(isec,moduleIDvec[Nconn],nconn,counter,count_Nconn, EXPconstants);
    previous=nconn;
    counter++;
  }
}


unsigned int SsbFitConstants::floatToReg27(float f) {

  int f_f = (*(int*)&f);
  int f_sign = (f_f >> 31) & 0x1;
  int f_exp = (f_f >> 23) & 0xFF;
  int f_frac = f_f & 0x007FFFFF;
  int r_sign;
  int r_exp;
  int r_frac;
  r_sign = f_sign;
  if((f_exp == 0x00) || (f_exp == 0xFF)) {
    // 0x00 -> 0 or subnormal
    // 0xFF -> infinity or NaN
    r_exp = 0;
    r_frac = 0;
  } else {
    r_exp = (f_exp) & 0xFF;
    r_frac = ((f_frac >> 5)) & 0x0003FFFF;
  }
  return (r_sign << 26) | (r_exp << 18) | r_frac;
}

Eigen::MatrixXd SsbFitConstants::get_A_matrix(
					      unsigned secid,
					      std::vector<int> miss_idx
					      )
{
  Eigen::Map<const Eigen::Matrix<double,11,16>,Eigen::Unaligned,Eigen::Stride<1,16>> kernel(getKernel(secid).data());  
  // instantitae A_matrix
  Eigen::MatrixXd A_matrix(11, miss_idx.size()); // 11 will never change for FTK system (this is 16 coordinates minus 5 helix parameters)
  if(secid==0) {
    //    std::cout << kernel <<std::endl;

    }
  // Defining A column from the "m_kernel"
  for (unsigned int icol = 0; icol < miss_idx.size(); ++icol)
    {
      for (unsigned int irow = 0; irow < 11; ++irow)
	{
	  // obtain the A vector in ideal coordinates
	  double A_irow_icol = kernel(irow,miss_idx[icol]);

	  // scale them to hw_scale
	  A_irow_icol /= hw_scale.at(miss_idx[icol]);

	  // set the A matrix
	  A_matrix(irow, icol) = A_irow_icol;
	}
    }
 
  return A_matrix;
}
//======================
// B column vectors = B matrix
//======================
Eigen::MatrixXd SsbFitConstants::get_B_matrix(
					     unsigned secid,
				             std::vector<int> miss_idx
					      )
{
  Eigen::Map<const Eigen::Matrix<double,11,16>,Eigen::Unaligned,Eigen::Stride<1,16>> kernel(getKernel(secid).data());

  // instantiate B_matrix, very similar to A_matrix case
  Eigen::MatrixXd B_matrix(11, real_idx.size()); // 11 will never change for FTK system (this is 16 coordinates minus 5 helix parameters)

  // set the values
  for (unsigned int icol = 0; icol < real_idx.size(); ++icol)
    {
      for (unsigned int irow = 0; irow < 11; ++irow)
	{
	  double B_irow_icol = kernel(irow,real_idx[icol]);

	  B_irow_icol /= hw_scale.at(real_idx[icol]);

	  B_matrix(irow, icol) = B_irow_icol;
	}
    }

  return B_matrix;
}
//======================
// C matrix
//======================
Eigen::MatrixXd SsbFitConstants::get_C_matrix(
					      Eigen::MatrixXd A_matrix
					      )
{
  // the number of column vectors will be the row and col dim of C_matrix
  int col_dim_A = A_matrix.cols();

  // create C_matrix
  Eigen::MatrixXd C_matrix(col_dim_A, col_dim_A);

  // set the values;
  for (int i = 0; i < col_dim_A; ++i)
    {
      for (int j = 0; j < col_dim_A; ++j)
	{
	  C_matrix(i, j) = A_matrix.col(i).transpose() * A_matrix.col(j);
	}
    }

  return C_matrix;
}




//======================
// h vector
//======================
// The h vectors
Eigen::VectorXd SsbFitConstants::get_h_vector(
					      unsigned secid					
					      )
{
  Eigen::Map<const Eigen::VectorXd> kaverage(getKaverage(secid).data(),11);
  // h_vector has dimension of number of real hits
  Eigen::VectorXd h_vector(real_idx.size());

  // set the values
  for (unsigned int i = 0; i < real_idx.size(); ++i)
    {
      h_vector(i) = kaverage(i);
    }

  return h_vector;
}


//======================
// J vector
//======================
Eigen::VectorXd SsbFitConstants::get_J_vector(
					      Eigen::MatrixXd A_matrix,
					      Eigen::VectorXd h_vector
					      )
{
  // J_vector has the dimension of missed coordinates
  // (which is same as columns in A_matrix)
  Eigen::VectorXd J_vector(A_matrix.cols());

  // set the values
  for (unsigned int i = 0; i < A_matrix.cols(); ++i)
    {
      J_vector(i) = A_matrix.col(i).transpose() * h_vector;
    }

  return J_vector;
}




//======================
// D matrix
//======================
Eigen::MatrixXd SsbFitConstants::get_D_matrix(
					      Eigen::MatrixXd A_matrix,
					      Eigen::MatrixXd B_matrix
					      )
{
  // instantitaion
  Eigen::MatrixXd D_matrix(A_matrix.cols(), B_matrix.cols());

  // set the value
  for (unsigned int i = 0; i < A_matrix.cols(); ++i)
    {
      for (unsigned int j = 0; j < B_matrix.cols(); ++j)
	{
	  D_matrix(i, j) = A_matrix.col(i).transpose() * B_matrix.col(j);
	}
    }

  return D_matrix;
}


//======================
// F vector
//======================
Eigen::VectorXd SsbFitConstants::get_F_vector(
					      Eigen::MatrixXd C_matrix,
					      Eigen::VectorXd J_vector
					      )
{
  // instantiation
  Eigen::VectorXd F_vector(C_matrix.cols());

  F_vector = C_matrix.inverse() * J_vector * -1;

  return F_vector;
}
//==============================================
// E_matrix
//==============================================
Eigen::MatrixXd SsbFitConstants::get_E_matrix(
    Eigen::MatrixXd C_matrix,
    Eigen::MatrixXd D_matrix
)
{
  // instantitaion
  Eigen::MatrixXd E_matrix(C_matrix.cols(), D_matrix.cols());

  // calculate
  E_matrix = C_matrix.inverse() * D_matrix * -1.;

  return E_matrix;
}


void SsbFitConstants::calcExtrapolationConstant(int secid, vector<int> moduleid, int eightl_secid, int nconn, int sizenconn , std::vector<uint32_t> &out){

  int nmissing = 0;
                       
  std::vector<int> miss_idx;
  miss_idx.push_back(0);
  miss_idx.push_back(1);
  miss_idx.push_back(9);
  miss_idx.push_back(11);
  miss_idx.push_back(15);



  Eigen::MatrixXd A = get_A_matrix(secid,miss_idx);
  //  std::cout << A << std::endl;
  Eigen::MatrixXd B = get_B_matrix(secid,miss_idx);
  Eigen::MatrixXd C = get_C_matrix(A);
  Eigen::MatrixXd D = get_D_matrix(A,B);
  Eigen::VectorXd h = get_h_vector(secid);
  Eigen::VectorXd J = get_J_vector(A,h);
  Eigen::MatrixXd E = get_E_matrix(C,D);
  Eigen::VectorXd F = get_F_vector(C,J);



  vector<double> Constants;

  Constants.push_back(F[0]);
  Constants.push_back(E(0,0));
  Constants.push_back(E(0,1));
  Constants.push_back(E(0,2));
  Constants.push_back(E(0,3));
  Constants.push_back(E(0,4));
  Constants.push_back(E(0,5));
  Constants.push_back(E(0,6));
  Constants.push_back(E(0,7));
  Constants.push_back(E(0,8));
  Constants.push_back(E(0,9));
  Constants.push_back(E(0,10));
  
  Constants.push_back(F[1]);
  Constants.push_back(E(1,0));
  Constants.push_back(E(1,1));
  Constants.push_back(E(1,2));
  Constants.push_back(E(1,3));
  Constants.push_back(E(1,4));
  Constants.push_back(E(1,5));
  Constants.push_back(E(1,6));
  Constants.push_back(E(1,7));
  Constants.push_back(E(1,8));
  Constants.push_back(E(1,9));
  Constants.push_back(E(1,10));
  
  Constants.push_back(F[2]);
  Constants.push_back(E(2,0));
  Constants.push_back(E(2,1));
  Constants.push_back(E(2,2));
  Constants.push_back(E(2,3));
  Constants.push_back(E(2,4));
  Constants.push_back(E(2,5));
  Constants.push_back(E(2,6));
  Constants.push_back(E(2,7));
  Constants.push_back(E(2,8));
  Constants.push_back(E(2,9));
  Constants.push_back(E(2,10));
  

    Constants.push_back(F[3]);
    Constants.push_back(E(3,0));
    Constants.push_back(E(3,1));
    Constants.push_back(E(3,2));
    Constants.push_back(E(3,3));
    Constants.push_back(E(3,4));
    Constants.push_back(E(3,5));
    Constants.push_back(E(3,6));
    Constants.push_back(E(3,7));
    Constants.push_back(E(3,8));
    Constants.push_back(E(3,9));
    Constants.push_back(E(3,10));


    Constants.push_back(F[4]);
    Constants.push_back(E(4,0));
    Constants.push_back(E(4,1));
    Constants.push_back(E(4,2));
    Constants.push_back(E(4,3));
    Constants.push_back(E(4,4));
    Constants.push_back(E(4,5));
    Constants.push_back(E(4,6));
    Constants.push_back(E(4,7));
    Constants.push_back(E(4,8));
    Constants.push_back(E(4,9));
    Constants.push_back(E(4,10));


    int NconnID = nconn ;
    unsigned int word = (eightl_secid << 2) | NconnID ;
   

    out.push_back(word);

    for(int y=0;y<=8;y++){
      int x =y+y;
      unsigned mask =  (((1 << 2) - 1) << (x));
      word = ((moduleid[0] & mask) << (28-x)) | floatToReg27(Constants[y]);
       out.push_back(word);
    }
    word = floatToReg27(Constants[9]);
    out.push_back(word);
    for(int y=10;y<=18;y++){
      int x =(y+y-20);
      unsigned mask =  (((1 << 2) - 1) << x)  ;
      word = ((moduleid[1] & mask) << (28-x)) | floatToReg27(Constants[y]);
       out.push_back(word);
    }
    word = floatToReg27(Constants[19]);
    out.push_back(word);
    for(int y=20;y<=28;y++){
      int x =(y+y-40);
      unsigned mask =  (((1 << 2) - 1) << x);
      word = ((moduleid[2] & mask) << (28-x)) | floatToReg27(Constants[y]);
       out.push_back(word);
    }
    word = floatToReg27(Constants[29]);
    out.push_back(word);
    for(int y=30;y<=38;y++){
      int x =(y+y-60);
      unsigned mask =  (((1 << 2) - 1) << x);
      word = ((moduleid[3] & mask) << (28-x)) | floatToReg27(Constants[y]);
      out.push_back(word);
    }
    word = floatToReg27(Constants[39]);
    out.push_back(word);
    for(int y=40;y<=48;y++){
      int x =(y+y-80);
      unsigned mask =  (((1 << 2) - 1) << x) ;
      word = ((secid  & mask) << (28-x)) | floatToReg27(Constants[y]);
      out.push_back(word);
    }
    word = floatToReg27(Constants[49]);
     out.push_back(word);
    unsigned mask =  ((1 << 2) - 1) << 0;
    word = ((sizenconn & mask)<< (28)) | floatToReg27(Constants[50]);
    out.push_back(word);
    mask =  ((1 << 2) - 1) << 2;
    word = ((sizenconn & mask) << 26)  | floatToReg27(Constants[51]);
    out.push_back(word);
    word = floatToReg27(Constants[52]);
    out.push_back(word);
    word = floatToReg27(Constants[53]);
    out.push_back(word);
    word = floatToReg27(Constants[54]);
    out.push_back(word);
    word = floatToReg27(Constants[55]);
   out.push_back(word);
    word = floatToReg27(Constants[56]);
   out.push_back(word);
    word = floatToReg27(Constants[57]);
   out.push_back(word);
    word = floatToReg27(Constants[58]);
   out.push_back(word);
    word = floatToReg27(Constants[59]);
   out.push_back(word);
   out.push_back(0);
   out.push_back(0);
   out.push_back(0);

    return;

}
void SsbFitConstants::writeConstants( const std::vector<uint32_t> &out,std::ofstream& myfile)
{
 myfile << internal // fill between the prefix and the number
	<< setfill('0'); // fill with 0
 for(auto o:out) {
   myfile << hex<< setw(8) << o << std::endl; 
 }
}

void SsbFitConstants::calcTFConstant(int secid, std::vector<uint32_t> &out){
  std::vector<int> real_idx;

  real_idx.push_back(2);
  real_idx.push_back(3);
  real_idx.push_back(4);
  real_idx.push_back(5);
  real_idx.push_back(6);
  real_idx.push_back(7);
  real_idx.push_back(8);
  real_idx.push_back(10);
  real_idx.push_back(12);
  real_idx.push_back(13);
  real_idx.push_back(14);

  vector<Eigen::MatrixXd> Cinverse;


  for(int pp=0; pp<=11;pp++){
    std::vector<int> miss_idx1;                       
    miss_idx1.clear();
   if(pp ==0){
      miss_idx1.push_back(0);
      miss_idx1.push_back(1);
    }
    else if (pp ==1){
      miss_idx1.push_back(2);
      miss_idx1.push_back(3);
    }
    else if (pp ==2){
      miss_idx1.push_back(4);
      miss_idx1.push_back(5);
    }
    else if (pp ==3){
      miss_idx1.push_back(6);
      miss_idx1.push_back(7);
    }else{
      miss_idx1.push_back(pp+4);
    }


   Eigen::MatrixXd A = get_A_matrix(secid,miss_idx1);//11 (column)x16 (row) 
   Eigen::MatrixXd C = get_C_matrix(A);
   Eigen::MatrixXd Cinverse_temp(C.rows(),C.cols());
   Cinverse_temp = C.inverse();
   Cinverse.push_back(Cinverse_temp);
  }
 
  std::vector<int> miss_idx;
  miss_idx.push_back(0);
  miss_idx.push_back(1);
  miss_idx.push_back(2);
  miss_idx.push_back(3);
  miss_idx.push_back(4);
  miss_idx.push_back(5);
  miss_idx.push_back(6);
  miss_idx.push_back(7);
  miss_idx.push_back(8);
  miss_idx.push_back(9);
  miss_idx.push_back(10);
  miss_idx.push_back(11);
  miss_idx.push_back(12);
  miss_idx.push_back(13);
  miss_idx.push_back(14);
  miss_idx.push_back(15);
  

  int start = m_ncoords-1; 
  int finish =0;
  int number_block_transfer =0;
  unsigned int parola = (secid<<4) | number_block_transfer ; 
  const std::vector<double> &kaverage=getKaverage(secid);
  
  Eigen::Map<const Eigen::Matrix<double,11,16>,Eigen::Unaligned,Eigen::Stride<1,16>> kernel(getKernel(secid).data()); 

  out.push_back(parola);
  out.push_back(0); 
  out.push_back(floatToReg27(Cinverse[0](1,0))); //missing pix0 constants
  out.push_back(floatToReg27(Cinverse[0](0,0))); //missing pix0 constants
  out.push_back(floatToReg27(kaverage[0])); 
  for (int ik2=start;ik2>=finish;--ik2) { // loop kaverages
    out.push_back(floatToReg27(kernel(0,ik2)/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[0](1,1))); //missing pix0 constants
  out.push_back(floatToReg27(Cinverse[0](0,1))); //missing pix0 constants
  out.push_back(floatToReg27(kaverage[1])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(kernel(1,ik2)/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[1](1,0))); //missing pix1 constants
  out.push_back(floatToReg27(Cinverse[1](0,0))); //missing pix1 constants
  out.push_back(floatToReg27(kaverage[2])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(kernel(2,ik2)/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(0);
  out.push_back(0);
  number_block_transfer =1;
  parola = (secid<<4) | number_block_transfer ; 
  out.push_back(parola);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[1](1,1))); //missing pix1 constants
  out.push_back(floatToReg27(Cinverse[1](0,1))); //missing pix1 constants
  out.push_back(floatToReg27(kaverage[3])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(kernel(3,ik2)/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[2](1,0))); //missing pix2 constants
  out.push_back(floatToReg27(Cinverse[2](0,0))); //missing pix2 constants
  out.push_back(floatToReg27(kaverage[4])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(kernel(4,ik2)/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[2](1,1))); //missing pix2 constants
  out.push_back(floatToReg27(Cinverse[2](0,1))); //missing pix2 consytants
  out.push_back(floatToReg27(kaverage[5])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(kernel(5,ik2)/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(0);
  out.push_back(0);
  number_block_transfer =2;
  parola = (secid<<4) | number_block_transfer ; 
  out.push_back(parola);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[3](1,0))); //missing pix3 constants
  out.push_back(floatToReg27(Cinverse[3](0,0))); //missing pix3 constants
  out.push_back(floatToReg27(kaverage[6])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(kernel(6,ik2)/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[3](1,1))); //missing pix3 constants
  out.push_back(floatToReg27(Cinverse[3](0,1))); //missing pix3 constants
  out.push_back(floatToReg27(kaverage[7])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(kernel(7,ik2)/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[4](0,0))); //missing sct4 constants
  out.push_back(floatToReg27(kaverage[8])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(kernel(8,ik2)/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(0);
  out.push_back(0);
  number_block_transfer =3;
  parola = (secid<<4) | number_block_transfer ; 
  out.push_back(parola);
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[5](0,0))); //missing sct5 constants
  out.push_back(floatToReg27(kaverage[9])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(kernel(9,ik2)/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[6](0,0))); //missing sct6 constants
  out.push_back(floatToReg27(kaverage[10])); 
  for (int ik2=start;ik2>=finish;ik2--) { // loop kaverages
    out.push_back(floatToReg27(kernel(10,ik2)/hw_scale.at(ik2)));    
  }
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[7](0,0))); //missing sct7 constants
  out.push_back(floatToReg27(getFitConst(secid,"Cd"))); //d0
  const std::vector<double> &Vd=getFitPar(secid,"Vd");
  for (int ik2=start;ik2>=finish;ik2--) { 
    double scaled = (Vd[ik2] / hw_scale.at(ik2));
    out.push_back(floatToReg27(scaled));
  } 
  out.push_back(0);
  out.push_back(0);
  out.push_back(0);
  number_block_transfer =4;
  parola = (secid<<4) | number_block_transfer ; 
  out.push_back(parola);
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[8](0,0))); //missing sct8 constants
  out.push_back(floatToReg27(getFitConst(secid,"Cz0"))); //z0
  const std::vector<double> &Vz0=getFitPar(secid,"Vz0");
  for (int ik2=start;ik2>=finish;ik2--) { 
    double scaled = (Vz0[ik2] / hw_scale.at(ik2));
    out.push_back(floatToReg27(scaled));
  } 
  out.push_back(0);
  out.push_back(0);
  
  out.push_back(floatToReg27(Cinverse[9](0,0))); //missing sct9 constants 
  out.push_back(floatToReg27(getFitConst(secid,"Co"))); //coth
  const std::vector<double> &Vo=getFitPar(secid,"Vo");
  for (int ik2=start;ik2>=finish;ik2--) { 
    double scaled = (Vo[ik2] / hw_scale.at(ik2));
    out.push_back(floatToReg27(scaled));
  } 
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[10](0,0))); //missing sct10 constants 
  out.push_back(floatToReg27(getFitConst(secid,"Cf"))); //phi0
  const std::vector<double> &Vf=getFitPar(secid,"Vf");
  for (int ik2=start;ik2>=finish;ik2--) { 
    double scaled = (Vf[ik2] / hw_scale.at(ik2));
    out.push_back(floatToReg27(scaled));
  } 
  out.push_back(0);
  out.push_back(0);
  out.push_back(0);
  number_block_transfer =5;
  parola = (secid<<4) | number_block_transfer ; 
  out.push_back(parola);
  out.push_back(0);
  out.push_back(0);
  out.push_back(floatToReg27(Cinverse[11](0,0))); //missing sct11 constants
  out.push_back(floatToReg27(getFitConst(secid,"Cc"))); //curvature
  const std::vector<double> &Vc=getFitPar(secid,"Vc");
  for (int ik2=start;ik2>=finish;ik2--) { 
    double scaled = (Vc[ik2] / hw_scale.at(ik2));
    out.push_back(floatToReg27(scaled));
  } 
  for (int i=0; i<43;i++){
    out.push_back(0);
  }


  return;
}

