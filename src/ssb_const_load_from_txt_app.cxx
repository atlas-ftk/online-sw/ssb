#include "ftkvme/VMEInterface.h" 
#include "ftkvme/VMEManager.h" 
#include "ssb/ssb.h" 


#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>     // std::string, std::stoi
#include <chrono>

/*
 SSB VME address, 32 bits: 
    0000 1XXX XXXX XXXX 0001 0000 0000 0000
    ------             / --- --------------
    slot#           r/w  FPGA#            \ offset address
 */

int main(int argc, char *argv[]){

  using namespace boost::program_options ;
  using namespace std;
  using namespace std::chrono;

  high_resolution_clock::time_point t1 = high_resolution_clock::now();
 
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("9"), "slot number")
    ("fpga", value< std::string >()->default_value("0"), "fpga id")
    ("exp", value< std::string >()->default_value(""), "constant file for exp")
    ("tkf", value< std::string >()->default_value(""), "constant file for tkf")
    ;

  positional_options_description p;

  variables_map vm;
  try
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }

  string exp_constant_file = vm["exp"].as<std::string>() ;
  string tkf_constant_file = vm["tkf"].as<std::string>() ;
  u_int slot = std::stoi( vm["slot"].as<std::string>() );
  u_int fpga = std::stoi( vm["fpga"].as<std::string>() );

  if( exp_constant_file=="" && tkf_constant_file=="" ){
    std::cout << "Please supply with the constant file for exp and tkf" << std::endl;
    return 0; 
  }

  VMEInterface *vme=VMEManager::global().ssb(slot,fpga); // always start with the base address of the ssb slot
  vme->disableBlockTransfers(false); // use real block transfer
  std::cout << "Working on slot= " << slot << " fpga= " << fpga << std::endl;
  string name = "";
  bool result = daq::ftk::ssb_const_load_from_txt( vme, exp_constant_file, tkf_constant_file, name); 

  high_resolution_clock::time_point t2 = high_resolution_clock::now();
  duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

  std::cout << "It took me " << time_span.count() << " seconds to load the file";
  std::cout << std::endl;

  return (int)result;
}
