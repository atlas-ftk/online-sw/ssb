#include "ssb/ssb_fpga.h"

#include <thread>

namespace daq
{
  namespace ftk
  {

    ssb_fpga::ssb_fpga(uint slot, uint fpgaId) :
      m_slot(slot),
      m_fpgaId(fpgaId)
    {
      std::cout<< "Getting VME interface for slot " << slot << " fpga Id " << fpgaId << std::endl; 
      m_vme=VMEManager::global().ssb(slot,fpgaId); 
      m_vme->disableBlockTransfers(false); // use real block transfer

      m_FWDate = getFWDate(); // Used to tell software what version of FW is on the chip
      m_FREQ   = getFWFreq();
    }

    ssb_fpga::~ssb_fpga()
    {
      std::cout << "destructor of fpga base class called" << std::endl;
    }

    uint32_t ssb_fpga::getFWHash()
    {
        u_int FWHash;
        if ( isHitWarrior() ) FWHash = m_vme->read_word( SSB_HW_FWHASH);
        else                  FWHash = m_vme->read_word( SSB_EXTF_FWHASH);
        return FWHash;
    }  

    uint32_t ssb_fpga::getFWDate()
    {
        u_int FWDate;
        if ( isHitWarrior() ) FWDate = m_vme->read_word( SSB_HW_FWDATE);
        else                  FWDate = m_vme->read_word( SSB_EXTF_FWDATE);
        return FWDate;
    }  

    uint32_t ssb_fpga::getFWFreq()
    {
        u_int FWFreq;
        if ( isHitWarrior() ) FWFreq = m_vme->read_word( SSB_HW_FREQ );
        else                  FWFreq = m_vme->read_word( SSB_EXTF_FREQ );
        return FWFreq;
    }  


    void ssb_fpga::freezeSpybuffer()
    {
        // VME Access try-catch loop 
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                m_vme->write_word(SSB_VME_ADDR , 0x1);       
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to freeze spybuffers.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to freeze spybuffers.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }

            break;
        }        
    }

    void ssb_fpga::unfreezeSpybuffer(bool force)
    {

        // VME Access try-catch loop 
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                bool hasFreeze = false;
                
                if ( isHitWarrior() )
                {
                    hasFreeze = m_vme->read_word( SSB_HW_FREEZE_MENU ) ; // menu contains freeze reason
                    if (0xeffdeffd & m_vme->read_word( SSB_HW_FREEZE_MENU ) == 0xeffdeffd ) hasFreeze = false; //TODO this is to catch the returned value of HW firmware before the freezemenu was implemented 
                    
                    if (force) m_vme->write_word( SSB_HW_SPYBUFFER_UNFREEZE, 0x1); // force unfreeze
                    else if (!hasFreeze) m_vme->write_word( SSB_HW_SPYBUFFER_UNFREEZE, 0x1); // only unfreeze if there is no freeze
                }
                else
                {
                    hasFreeze = m_vme->read_word( SSB_EXTF_FREEZE_MENU ) ; // menu contains freeze reason
             
                    if (force)  m_vme->write_word( SSB_EXTF_SPYBUFFER_UNFREEZE, 0x1); // force unfreeze
                    else if (!hasFreeze) m_vme->write_word( SSB_EXTF_SPYBUFFER_UNFREEZE, 0x1); // only unfreeze if there is no freeze
                }
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to unfreeze spybuffers.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to unfreeze spybuffers.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            break;
        }        
    }

    void ssb_fpga::freezeSRSB()
    {

        // VME Access try-catch loop 
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                if ( isHitWarrior() ) m_vme->write_word(SSB_HW_SRSB_FREEZE , 0x1);        
                else                  m_vme->write_word(SSB_SRSB_FREEZE , 0x1);
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to freeze srsb.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to freeze srsb.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            break;
        }
    }

    void ssb_fpga::unfreezeSRSB(bool force)
    {

        // VME Access try-catch loop 
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                if ( isHitWarrior() ) m_vme->write_word( SSB_HW_SRSB_UNFREEZE, 0x1);
                else                  m_vme->write_word( SSB_SRSB_UNFREEZE, 0x1);
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to unfreeze srsb.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to unfreeze srsb.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            break;
        }        

    }
    



    std::shared_ptr< ssb_spyBuffer > ssb_fpga::getSpybuffer()
    {           
        std::shared_ptr<ssb_spyBuffer> spybuffer_reader(new ssb_spyBuffer(0x0, SSB_VME_ADDR, m_vme) );        
        spybuffer_reader->setSpyDimension(m_conf->get_NColSpy() * SSB_SPYBUFFER_BLT * SSB_SPYBUFFER_NWORDS);

        // VME Access try-catch loop 
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                spybuffer_reader->read_spy_buffer();                 
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to retrieve spybuffers.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to retrieve spybuffers.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            break;
        }        
        
        return spybuffer_reader;
    }


    std::shared_ptr< ssb_spyBuffer > ssb_fpga::getSpybufferSSB()
    {
        std::shared_ptr<ssb_spyBuffer> spybuffer_reader(new ssb_spyBuffer(0x0, SSB_VME_ADDR, m_vme) ); // will function as a shell to hold the buffer

        std::vector<uint32_t> data; 
        std::vector<uint32_t> data_temp; 

        // VME Access try-catch loop 
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                data.clear();

                for(uint32_t iblt=0; iblt<SSB_SPYBUFFER_BLT*m_conf->get_NColSpy(); iblt++)
                {
                    data_temp.clear();
                    data_temp=m_vme->read_block(SSB_VME_ADDR, SSB_SPYBUFFER_NWORDS);
                    data.insert( data.end(), data_temp.begin(), data_temp.end() );
                }
                
                spybuffer_reader->setBuffer(data);

            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to retrieve spybuffers.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to retrieve spybuffers.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            break;
        }        


        return spybuffer_reader;
    }


    std::shared_ptr< ssb_spyBuffer > ssb_fpga::getSpybuffer_histo(uint32_t addr, u_int nWords, u_int nBLT)
    {
        std::shared_ptr<ssb_spyBuffer> spybuffer_reader(new ssb_spyBuffer(0x0, addr, m_vme) ); // will function as a shell to hold the buffer

        std::vector<uint32_t> data; 
        std::vector<uint32_t> data_temp; 

        int nspy_histo = 1;

        // VME Access try-catch loop 
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                for(uint32_t iblt=0; iblt<nBLT*nspy_histo; iblt++)
                {
                    data_temp.clear();
                    data_temp=m_vme->read_block(addr, nWords);
                    data.insert( data.end(), data_temp.begin(), data_temp.end() );
                }                 
            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to retrieve histograms.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to retrieve histograms.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            break;
        }        


        spybuffer_reader->setBuffer(data);

        return spybuffer_reader;
    }

    std::vector<uint32_t> ssb_fpga::getSRSB(uint32_t addr, u_int nWords, u_int nBLT)
    {    
        std::vector<uint32_t> data; 
        std::vector<uint32_t> data_temp; 

        int nspy_srsb = 1;
        freezeSRSB();  // Freeze SRSB to capture data
        std::this_thread::sleep_for(std::chrono::milliseconds(100)); // give time to FW to capture data

        // VME Access try-catch loop 
        for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
        {
            try
            {
                data.clear();
                for(uint32_t iblt=0; iblt<nBLT*nspy_srsb; iblt++)
                {
                    data_temp.clear();
                    data_temp=m_vme->read_block(addr, nWords);
                    data.insert( data.end(), data_temp.begin(), data_temp.end() );
                }

            }
            catch (daq::ftk::VmeError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to retrieve SRSB.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            catch (daq::ftk::CMEMError& e)
            {
                if (i == SSB_MAX_VME_RETRIES)
                {
                    std::stringstream message;
                    message << "SSB failed to retrieve SRSB.";
                    daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
                    ers::error(excp);
                }
                continue;
            }
            break;
        }        

        unfreezeSRSB();  // unfreeze SRSB

        return data;
    }


    std::shared_ptr< ssb_spyBuffer > ssb_fpga::getSRSB_emon(uint32_t addr, u_int nWords, u_int nBLT)
    {
        std::vector<uint32_t> srsb = getSRSB(addr, nWords, nBLT);
        std::shared_ptr<ssb_spyBuffer> spybuffer_reader(new ssb_spyBuffer(0x0, addr, m_vme) ); // will function as a shell to hold the buffer
        spybuffer_reader->setBuffer(srsb);

        return spybuffer_reader;        
    }   

  }
}
