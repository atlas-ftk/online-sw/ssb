#include <ssb/SsbFitConstants.hh>
#include <boost/program_options.hpp>
#include <variant.hpp>
#include <chrono>

int main(int argc,char *argv[])
{

  boost::program_options::variables_map vm;
  std::string constantDir;
  std::string outputDir;
  unsigned int region;

  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "")
      ("constantDir,c", boost::program_options::value<std::string>(&constantDir)->default_value("."), "constant dir")
      ("outputDir,o",  boost::program_options::value<std::string>(&outputDir)->default_value("."), "output dir")
      ("region,r", boost::program_options::value<unsigned int>(&region)->default_value(0), "region");
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

    if (vm.count("help")) {
      std::cout << desc << '\n';
      exit(0);
    }
  } 
  
  catch (const  boost::program_options::error &ex) {
    std::cerr << ex.what() << '\n';
  }

  storage::variant v1;
  storage::variant v2;
  SsbFitConstants ssb(v1,v2);

  std::chrono::steady_clock::time_point read_start = std::chrono::steady_clock::now();
  //  ssb.readFromFile(use_msgpack);
  std::chrono::steady_clock::time_point read_end = std::chrono::steady_clock::now();

  std::cout << "-> Parsed configuration files " 
	    << std::chrono::duration_cast<std::chrono::milliseconds>(read_end-read_start).count() << " ms " << std::endl;
  std::chrono::steady_clock::time_point calc_start = std::chrono::steady_clock::now();
  ssb.calculate();
  std::chrono::steady_clock::time_point calc_end = std::chrono::steady_clock::now();

  std::cout << "-> Calculated EXP/TFK constants " 
	    << std::chrono::duration_cast<std::chrono::milliseconds>(calc_end-calc_start).count() << " ms " << std::endl;

  std::chrono::steady_clock::time_point write_start = std::chrono::steady_clock::now();
  //  ssb.write2Txt(outputDir);
  std::chrono::steady_clock::time_point write_end = std::chrono::steady_clock::now();

  std::cout << "-> Wrote EXP/TFK constanfs " 
	    << std::chrono::duration_cast<std::chrono::milliseconds>(write_end-write_start).count() << " ms " << std::endl;


  return 0;
}
