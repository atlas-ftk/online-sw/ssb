#include <ftkcommon/svfplayer.h>
#include "ssb/ssb.h"
#include "ssb/ssb_fpga.h"
#include "ssb/ssb_vme_regs.h"



/** Implementation of the SVFIntepreter designed to meet
 * the needs for JTAG comunnication for the SSB
 * in the AMB, with single VME access per transition.
 */
class SVFInterpreterSSB : public SVFInterpreter {
private:
	unsigned int _slot;
	unsigned int _fpga;
	VMEInterface *_vme;

	void send_command(const JTAGCmdBuffer &);

public:
  SVFInterpreterSSB(unsigned int slot, unsigned int fpga);

	virtual ~SVFInterpreterSSB();
};

SVFInterpreterSSB::SVFInterpreterSSB(unsigned int slot,unsigned int fpga) : _slot(slot), _fpga(fpga) {
	_hasCable = true;
	_vme = VMEManager::global().ssb(slot,fpga);
	cout << "Begin of programming sequence for SSBs" << endl;
	//	_vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 1);
	_vme->write_word(ENABLE_TCK, 0x1);
}

SVFInterpreterSSB::~SVFInterpreterSSB() {
	cout << "Switching off programming for SSB" << endl;
	//_vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 0);
	_vme->write_word(ENABLE_TCK, 0x0);
}

/** The send command method is designed to meet the design of
 * the JTAG cotnrol through VME for the SSB boards.
 */
void SVFInterpreterSSB::send_command(const JTAGCmdBuffer &cmd) {
	_totnbits += cmd.getNBits();

	bool bigCommand(cmd.getNBits()>1000000);

	std::vector<uint8_t>tdores(cmd.getBufferSize());
	for (size_t pos=0; pos!=cmd.getBufferSize(); ++pos) tdores[pos] = 0;

	size_t istep = (cmd.getNBits()+9)/10;
	unsigned int curstep(0);
	if (bigCommand) cout << "Large command: " << 0 << flush;
	for (size_t ibit=0; ibit!=cmd.getNBits(); ++ibit) {
		unsigned int step = ibit/istep;
		if (step>curstep && bigCommand) {
			cout << step << flush;
			curstep = step;
		}

		size_t bufpos = cmd.getBufferSize()-1-ibit/8; // the first position to be sent is at the end
		size_t bitpos = ibit&7; // within the word the bits have a normal order
		unsigned int tms = (cmd.getTMS(bufpos)>>bitpos)&1;
		unsigned int tdi = (cmd.getTDI(bufpos)>>bitpos)&1;
		unsigned int tdo = 0; // if not used start from 0

		if (cmd.hasTDO()) tdo = _vme->read_word(SSB_VME_TDO)&1;
		_vme->write_word(SSB_VME_TDITMS, tdi + (tms<<1) );

		if (_verbose>1) cout << tms << " " << tdi << " " << tdo << " (" << ibit << "," << bitpos << ")" << endl;
		tdores[bufpos] |= (tdo<<bitpos);
	}

	if (bigCommand)  cout << " Done" << endl;

	if (cmd.hasTDO()) checkTDOs(cmd, tdores.data());

}


/** Implementation of the SVFIntepreter designed to meet
 * the needs for JTAG comunnication for the FPGA BUSCA
 * in the SSB, with VME block transfer .
 */
class SVFInterpreterSSBBlockTransfer : public SVFInterpreter {
private:
	unsigned int _slot;
	unsigned int _fpga;
	VMEInterface *_vme;

	size_t _nbtwrites;

	void send_command(const JTAGCmdBuffer &);

public:
  SVFInterpreterSSBBlockTransfer(unsigned int slot,unsigned int fpga);

	virtual ~SVFInterpreterSSBBlockTransfer();

	virtual void printSummary();
};

SVFInterpreterSSBBlockTransfer::SVFInterpreterSSBBlockTransfer(unsigned int slot,unsigned int fpga) : _slot(slot),_fpga(fpga),
		_nbtwrites(0) {
	_hasCable = true;
	_vme = VMEManager::global().ssb(slot,fpga);
	_vme->disableBlockTransfers(false); // override the disable
	cout << "Begin of programming sequence for SSBs using the block transfer" << endl;
	//_vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 1);
	_vme->write_word(ENABLE_TCK, 0x1);
}

SVFInterpreterSSBBlockTransfer::~SVFInterpreterSSBBlockTransfer() {
	cout << "Switching off programming for SSBs" << endl;
	//_vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 0);
	_vme->write_word(ENABLE_TCK, 0x0);
}

/** The send command method is designed to meet the design of
 * the JTAG cotnrol through VME for the SSB boards.
 */
void SVFInterpreterSSBBlockTransfer::send_command(const JTAGCmdBuffer &cmd) {
	_totnbits += cmd.getNBits();

	// the boolean is true if for the JTAG set the block transfer can be used
	const bool canUseBT(!cmd.hasTDO());
	if (_verbose>0) cout << "Block transfer = " << canUseBT << endl;

	bool bigCommand(cmd.getNBits()>1000000);

	std::vector<uint8_t>tdores;
	if (cmd.hasTDO()) {
	  tdores=std::vector<uint8_t>(cmd.getBufferSize());
		for (size_t pos=0; pos!=cmd.getBufferSize(); ++pos) tdores[pos] = 0;
	}

	size_t istep = (cmd.getNBits()+9)/10;
	unsigned int curstep(0);
	if (bigCommand) cout << "Large command: " << 0 << flush;

	if (canUseBT) {
		_nbtwrites += cmd.getNBits();

		// it can use the BT, collect block of TDI-TMS  data and send as vector to the VME
		const unsigned int block_size(0x1000); // max number of bits that can be sent at once
		const unsigned int nblocks((cmd.getNBits()+block_size-1)/block_size); // number of transfers

		if (_verbose>0) cout << "N blocks = " << nblocks << endl;

		vector<unsigned int> tditms_data;
		vector<unsigned int> tdo_data;
		for (unsigned int iblock=0; iblock!=nblocks; ++iblock) { // loop over the blocks
			// reset the intermediate vectors
			tditms_data.clear();
			tdo_data.clear();

			const unsigned int firstBit(iblock*block_size); // offset of the block, as number of bit
			const unsigned int lastBit(firstBit+block_size); // offset of the block, as number of bit

			unsigned int step = firstBit/istep;
			if (step>curstep && bigCommand) {
				cout << step << flush;
				curstep = step;
			}

			for (size_t ibit=firstBit; ibit!=cmd.getNBits() && ibit!=lastBit; ++ibit) { // loop over the bits
				size_t bufpos = cmd.getBufferSize()-1-ibit/8; // the first position to be sent is at the end
				size_t bitpos = ibit&7; // within the word the bits have a normal order

				unsigned int tms = (cmd.getTMS(bufpos)>>bitpos)&1;
				unsigned int tdi = (cmd.getTDI(bufpos)>>bitpos)&1;
				//unsigned int tdo = 0; // if not used start from 0


				// populate the intermediate vector
				tditms_data.push_back(tdi | (tms<<1));
			} // end loop over the bits

			if (!tditms_data.empty()) _vme->write_block(SSB_MEM_TDITMS_BT,tditms_data);
			//TODO TDO is not implemented
		} // end loop over the blocks
	}
	else {
		// cannot use BT, perform a simpe bit-by-bit transfer, can also read TDO
		for (size_t ibit=0; ibit!=cmd.getNBits(); ++ibit) {
				unsigned int step = ibit/istep;
				if (step>curstep && bigCommand) {
					cout << step << flush;
					curstep = step;
				}

				size_t bufpos = cmd.getBufferSize()-1-ibit/8; // the first position to be sent is at the end
				size_t bitpos = ibit&7; // within the word the bits have a normal order
				unsigned int tms = (cmd.getTMS(bufpos)>>bitpos)&1;
				unsigned int tdi = (cmd.getTDI(bufpos)>>bitpos)&1;
				unsigned int tdo = 0; // if not used start from 0

				if (cmd.hasTDO()) tdo = _vme->read_word(SSB_MEM_TDO_BT)&1;
				_vme->write_word(SSB_MEM_TDITMS_BT, tdi | (tms<<1) );

				if (_verbose>1) cout << tms << " " << tdi << " " << tdo << " (" << ibit << "," << bitpos << ")" << endl;
				tdores[bufpos] |= (tdo<<bitpos);
			}
	}

	if (bigCommand)  cout << " Done" << endl;

  if (cmd.hasTDO()) {
    checkTDOs(cmd, tdores.data());
  }
}

void SVFInterpreterSSBBlockTransfer::printSummary() {
	SVFInterpreter::printSummary();
	cout << "Total number of bits tranfer using BT: " << _nbtwrites << endl;
}

int main(int argc, char **argv)
{
  using namespace  boost::program_options ;

  int verbose(0);

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value<unsigned int>()->default_value(15), "Use SSB in slot")
    ("fpga", value<unsigned int>()->default_value(0), "Use FPGA ")
    ("svffile,S", value<string>(), "SVF file with commands")
    ("verbose,v", value<int>(&verbose)->implicit_value(1), "Changes the verbosity level during the parsing")
    ("nocomment,N", "Suppress the print of the comments found in the SVF file")
    ("bt,B", "Uses block transfer more")
    ("test", "When used the VME interface is not opened, allowed tests of the parser")
    ;

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help
    {
      FTK_VME_ERROR( "-ssb_svfplayer : Failure in command_line_parser" );
      return 1;
    }

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      ERS_LOG( desc );
      return 0;
    }

  int slot = vm["slot"].as<unsigned int>();
  int fpga = vm["fpga"].as<unsigned int>();
  string svffile_path = vm["svffile"].as<string>();
  
  bool printComment = vm.count("nocomment")==0;
  bool blockTransfer = vm.count("bt")>0;

  struct timeval startTime;
  gettimeofday(&startTime, 0x0);

  int res = 0;

  // implement an interpreter compatible with the options
  std::unique_ptr<SVFInterpreter> svfplayer;
  if (!vm.count("test")) {
    if (not blockTransfer) svfplayer=std::make_unique<SVFInterpreterSSB>(slot,fpga);
    else if (blockTransfer) svfplayer=std::make_unique<SVFInterpreterSSBBlockTransfer>(slot,fpga);
  }
  else {
    svfplayer=std::make_unique<SVFInterpreter>();
  }
  // setup verbosity message levels
  svfplayer->setVerbose(verbose);
  svfplayer->setPrintComment(printComment);

  // open the file and parse it
  svfplayer->openFile(svffile_path.c_str());
  svfplayer->parse();

  if (!svfplayer->getNTDOErrors()) cout << "ALL GOOD" << endl;
  else cout << "Possible issues" << endl;


  struct timeval endTime;
  gettimeofday(&endTime, 0x0);

  cout << endl << "Elapsed time: " << endTime.tv_sec-startTime.tv_sec << " seconds" << endl;

  return res;

}
