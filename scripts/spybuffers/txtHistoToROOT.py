#!/usr/bin/env python2 

"""
Read in firmware histogram dumps from the SSB and save them as a histogram in a ROOT file.

example:
./txtHistoToROOT.py -i /afs/cern.ch/work/l/longjon/public/FTK/solo_run/2018-06-26/00h04m01s/SSB/crate-2/slot-9
"""

# Test dir /afs/cern.ch/work/l/longjon/public/FTK/solo_run/2018-06-26/00h04m01s/SSB/crate-2/slot-9

import argparse,os
from array import array
from ROOT import TH1I,TFile,TCanvas,gROOT

gROOT.SetBatch(True)

scaling = dict()
scaling["chi2"] = 1023./65500
scaling["d0"]   = 1023./64
scaling["z0"]   = 1023./640
scaling["cotth"]= 1023./13.1
scaling["phi0"] = 1023./6.28
scaling["curv"] = 1023./1.3

shifting = dict()
shifting["d0"] = 32
shifting["z0"] = 320
shifting["cotth"] = 6.55
shifting["curv"] = 0.65
    

def convToROOT(f, args):

    txtFile = args.input + "/" + f
    fHisto = open(txtFile)

    # Grab just the histo name
    fNameOnly = txtFile.split("/")[-1]
    firstSpot = fNameOnly.find("main_") + 5
    secondSpot = fNameOnly.find("_histo")
    histoName = fNameOnly[firstSpot:secondSpot]

    bins = list()
    binNumberPrev = -1
    for line in fHisto:
        if "word" in line:
            binNumber = int(line.split()[1].rstrip(":"))
            binContent = int(line.split()[2], 16)
            if args.fillMissingBins:
                while binNumber-1 != binNumberPrev:
                    binNumberPrev+=1
                    bins.append( (binNumberPrev, 0 ) )

            binNumberPrev = binNumber
                    
            bins.append( (binNumber, binContent ) )

    if len(bins)==0:
        print "Warning, didn't find bins for " + txtFile
        outHisto = TH1I(histoName, histoName, 1, 0, 1)
    else:
        # make the histo with nBins = len(bins) from the first bin number [0][0] to last [-1][0]
        outHisto = TH1I(histoName, histoName, len(bins), bins[0][0], bins[-1][0])

    # Fill the histo
    for i in bins:
        outHisto.SetBinContent(i[0], i[1])

    return outHisto


# Grab the scaling and shifting size for the histo and rebuild the bins
def rescaleHisto(hist):
    nbin = hist.GetNbinsX() + 1 # extra bin to shift 0th bin from underflow to a normal bin
    binWidth = hist.GetBinWidth(0)

    histoName = hist.GetName()
    scale = 1
    shift = 0

    bins = list()

    for key in shifting:
        if key in histoName.lower():
            shift = shifting[key]

    for key in scaling:
        if key in histoName.lower():
            scale = scaling[key]

    for i in range(0, nbin+1):
        bins.append(1.*i/scale - shift)
        ##bins.append((i-shift)*1./scale)

    newHist = TH1I(hist.GetName()+"_rescaled", hist.GetTitle(), nbin, array('d',bins) )

    for i in range(0,nbin+1):
        newHist.SetBinContent(i+1, hist.GetBinContent(i)) #should be lining up previous 'underflow' bin into the first bin

    return newHist
        
        
    

def main(args):

    # Open ROOT file
    print "Will write a new file to: " + args.output
    fOut = TFile(args.output, "recreate")


    # get list of histograms
    fileList = os.listdir(args.input)
    
    if args.pdf:
        canvas = TCanvas("canvas",'',800,600)

    for f in fileList:
        if "histo" in f and "root" not in f:
            print f

            hist = convToROOT(f, args)

            if not args.raw:
                hist = rescaleHisto(hist)

            hist.SetDirectory(fOut)
            hist.Write()

            if args.pdf:                
                if 'l1idskip' in str(hist.GetName()).lower() or 'chi2' in str(hist.GetName()).lower() or 'ntrack' in str(hist.GetName()).lower() or 'curv' in str(hist.GetName()).lower():
                    canvas.SetLogy(True)
                else:
                    canvas.SetLogy(False)
                canvas.SetLogy(True)



                hist.SetMarkerColor(2)
                hist.SetLineColor(2)
                hist.SetLineWidth(2)
                hist.SetMarkerSize(.2)
                hist.Draw()
                canvas.SaveAs(hist.GetName()+".pdf")


    fOut.Close()



    print "I'm finished!"




if __name__== "__main__":

    parser = argparse.ArgumentParser(description="Turn fw histos into root files.")

    parser.add_argument('-i', '--input', type=str, required=True, help="Input spybuffer directory")
    parser.add_argument('-o', '--output', type=str, default="histograms.root", help="Output filename")
    parser.add_argument('-r', '--raw', action='store_true', help="Raw histograms, don't shift and scale")
    parser.add_argument('-p', '--pdf', action='store_true', help="Print pdfs")
    parser.add_argument('-f', '--fillMissingBins', action='store_true', help="Look for missing bins and fill them with 0s")

    args = parser.parse_args()

    main(args)
