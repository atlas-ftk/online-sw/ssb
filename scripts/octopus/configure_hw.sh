#!/bin/bash

source slots.sh

## Configure HitWarrior to take N inputs (0x3 for two primaries)
vme_poke 0x${SSB_CODE}00461c 0x1

vme_poke 0x${SSB_CODE}004618 0x1 ## ignore BP from 'flic'
