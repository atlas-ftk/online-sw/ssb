#! /bin/bash

source slots.sh

[ ! -d dumps ] && mkdir dumps

DUMPNAME=

if [ ! -z $1 ]
then
    if [ "$1" == "now" ]
    then
	DUMPNAME=_$(date +%m-%d-%H%M)
    else
	DUMPNAME=_$1
    fi
fi

if [ ! -z $2 ]
then
    POSTFIX=-$2
fi

echo "taking EXTF and HW dumps in files dump/dump${DUMPNAME}_{EXTF,HW}${POSTFIX}.txt"

echo "Reason for the freeze is "$(vme_peek ${SSB_CODE}000734)
ssb_spybuffer_dump_main --slot $SSB_SLOT --fpga 4 --nSPY 4  > dumps/dump${DUMPNAME}_HW${POSTFIX}.txt
ssb_spybuffer_dump_main --slot $SSB_SLOT --fpga 0 --nSPY 10 > dumps/dump${DUMPNAME}_EXTF${POSTFIX}.txt

echo "taking histogram dumps in files dump/dump${DUMPNAME}_hist_{tracks,AUX,DF}${POSTFIX}.txt"

ssb_spybuffer_dump_main --slot $SSB_SLOT --fpga 0 --hmode --addr 0x800 > dumps/dump${DUMPNAME}_hist_tracks${POSTFIX}.txt
ssb_spybuffer_dump_main --slot $SSB_SLOT --fpga 0 --hmode --addr 0x900 > dumps/dump${DUMPNAME}_hist_AUX${POSTFIX}.txt
ssb_spybuffer_dump_main --slot $SSB_SLOT --fpga 0 --hmode --addr 0xa00 > dumps/dump${DUMPNAME}_hist_DF${POSTFIX}.txt
