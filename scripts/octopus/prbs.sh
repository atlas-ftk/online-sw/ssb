#! /bin/bash

source slots.sh

vme_poke 0x${SSB_CODE}004800 0
vme_poke 0x${SSB_CODE}0007f0 0
sleep 0.5
echo "starting it..."
vme_poke 0x${SSB_CODE}004800 3
sleep 0.5
vme_poke 0x${SSB_CODE}0007f0 1
sleep 0.5

watch -n 1 vme_peek 0x${SSB_CODE}004800

echo "stopping it..."
vme_poke 0x${SSB_CODE}004800 0
vme_poke 0x${SSB_CODE}0007f0 0
