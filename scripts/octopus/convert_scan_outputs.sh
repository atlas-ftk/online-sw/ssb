#!/bin/bash

cat "$1" | \
    awk \
        -v pstart=b0f00000 \
        -v pend=e0f00000 \
        -v dend=e0da0000 \
        -v idle=c9c9c9bc1 \
        -v pstartend=dead1d1e0 \
        -v outfile="$2" \
'
{
    sub(/0x/, "", $1)
    if ($1 == pstart) {
        nfile += 1
        outfile_mod = outfile
        sub(/([^\.]+)/, "&_" nfile, outfile_mod)
        print "Writing event to file " outfile_mod "..."
        startline = NR
    }

    if ($1 == dend)
       l1line[outfile_mod] = NR + 3 - startline

    data[outfile_mod] = data[outfile_mod] ($1 "0\n")
}
END {
    for (a in data) {
        if (a != "") {
            printf "%08x0\n", l1line[a] >a
            print pstartend >a
            printf data[a] >a
            print pstartend >a
            for (i=0; i<4090-l1line[a]; i+=1)
                print idle >a
        }
    }
}
'
