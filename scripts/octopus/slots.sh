OCTOPUS_SLOT=6
SSB_SLOT=12
#SSB_SLOT=9

function get_slot_code()
{
    local hex_code
    case $1 in 
        0 ) hex_code=00;;    1 ) hex_code=08;;
        2 ) hex_code=10;;    3 ) hex_code=18;;
        4 ) hex_code=20;;    5 ) hex_code=28;;
        6 ) hex_code=30;;    7 ) hex_code=38;;
        8 ) hex_code=40;;    9 ) hex_code=48;;
        10 ) hex_code=50;;   11 ) hex_code=58;;
        12 ) hex_code=60;;   13 ) hex_code=68;;
        14 ) hex_code=70;;   15 ) hex_code=78;;
        16 ) hex_code=80;;   17 ) hex_code=88;;
        18 ) hex_code=90;;   19 ) hex_code=98;;
        20 ) hex_code=a0;;   21 ) hex_code=a8;;
        22 ) hex_code=b0;;   23 ) hex_code=b8;;
        24 ) hex_code=c0;;   25 ) hex_code=c8;;
        26 ) hex_code=d0;;   27 ) hex_code=d8;;
        28 ) hex_code=e0;;   29 ) hex_code=e8;;
        30 ) hex_code=f0;;   31 ) hex_code=f8;;
    esac
    echo $hex_code
}

OCTOPUS_CODE=$(get_slot_code $OCTOPUS_SLOT)
SSB_CODE=$(get_slot_code $SSB_SLOT)
