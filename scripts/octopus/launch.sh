#! /bin/bash

source slots.sh

./configure.sh $1 && ./connect.sh && ./run.sh && ssb_status_main --slot $SSB_SLOT && ./monitor_running.sh && ssb_status_main --slot $SSB_SLOT
