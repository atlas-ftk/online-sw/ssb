#!/bin/bash

#raw_12LiblHW.moduleidmap
moduleMap=raw_12Libl3D.moduleidmap

for i in `seq 0 63`;
do
    ./create_ssmap_coe.py $moduleMap $i 0 1 > ssmapSctAux_tower$i.txt
    ./create_ssmap_coe.py $moduleMap $i 1 1 > ssmapPixAux_tower$i.txt
    ./create_ssmap_coe.py $moduleMap $i 1 0 >  ssmapPixDF_tower$i.txt
    ./create_ssmap_coe.py $moduleMap $i 0 0 >  ssmapSctDF_tower$i.txt

done  

