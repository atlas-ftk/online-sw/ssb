#!/usr/bin/env python


import os, sys,argparse
#Setup script path

pwd = os.path.dirname(os.path.realpath(__file__)) 
script_folder=os.path.dirname( os.path.abspath( pwd + '/../../python/crcmod-1.7/python2/crcmod/crcmod.py') )

 
if script_folder not in sys.path:
    sys.path.insert( 0, script_folder)
 

import crcmod

parser = argparse.ArgumentParser(description="EXP constant checksum")
parser.add_argument('-i', '--input', type = str, default='', help='EXP constants file')
args = parser.parse_args()



def int_to_invbytestr(val):
    temparray = [(val & (0xff << pos*8)) >> pos*8 for pos in range(4)]
    res = ''
    for pos in temparray:
        res += chr(int('{0:08b}'.format(pos)[::-1], 2))
    return res

crc_fun = crcmod.mkCrcFun(0x104c11db7, 0xffffffff, False)

f = open(args.input, "r")

set_sector = True               # First line of each 64-line bunch is the sectorID, then 60 constant
                                # words, then 3 zeroes that are excluded from checksum computation
sector_num = 0                  # Store the sectorID
word_i     = 0                  # Word index
crc = 0xffffffff                # Initial CRC value

for a in f.readlines():
    a = a.rstrip(' \r\n')       # Strip newlines and such
    if (set_sector == True):    # Get a sector
        if (int(a, 16) > 65535): # Not if it's >0xffff, we've set that as the max address on the FW
            sector_num = int(a, 16)
            break

        if (int(a, 16) - sector_num > 1): # If there is a gap between this sector and the previous
                                          # one, compute checksums for the blocks of
                                          # zero-initialized memory
            for i in range(int(a, 16) - sector_num - 1):
                for j in range(60):
                    crc = crc_fun(int_to_invbytestr(0), crc)
        sector_num = int(a, 16) # Grab sectorID
        word_i     = 0          # Init word index
        set_sector = False
        continue
    if (set_sector == False):
        if (word_i < 60):       # Compute crc for first 60 words, last 3 words are fillers
            crc = crc_fun(int_to_invbytestr(int(a, 16)), crc)
        if (word_i == 62):      # Next line belongs to the next block
            word_i     = 0
            set_sector = True
        else:
            word_i += 1

if (65535 - sector_num > 0):    # Compute checksums for the last blocks of zero-initialized memory
    for i in range(65535 - sector_num):
        for j in range(60):
            crc = crc_fun(int_to_invbytestr(0), crc)

print hex(crc).rstrip('L')
