#!/usr/bin/env python                                                                                   
import os,sys

pwd = os.path.dirname(os.path.realpath(__file__))
script_folder=os.path.dirname( os.path.abspath( pwd + '/../../python/crcmod-1.7/python2/crcmod/crcmod.py') )

if script_folder not in sys.path:
    sys.path.insert( 0, script_folder)


import argparse
import re
import crcmod
import inspect
import sys

parser = argparse.ArgumentParser(description="TF constants")
parser.add_argument('-i', '--InputConstFile', type = str, default='', help='TF constants file')
args = parser.parse_args()

# CRC funtion
def int_to_invbytestr(val):
	temparray = [(val & (0xff << pos*8)) >> pos*8 for pos in range(4)]
	res = ''
	for pos in temparray:
		res += chr(int('{0:08b}'.format(pos)[::-1], 2))
	return res

crc_fun = crcmod.mkCrcFun(0x104c11db7, 0xffffffff, False)
crc = 0xffffffff

filein = open(args.InputConstFile,"r")

line_num = -1
line_ID  = True

line_array     = list()
line_ID_array  = list()
line_num_array = list()

# Data are organised in block of 64 words of 32 bit: the first word is the sector ID
# This is probably a not elegant way of diving the lines depending if they are Sector ID (True) or other data (False)
# The last digit of the Sector ID is called sub-sector
# In this for look the line content is stored in a list
# True is found by number of line exact divition
for line in filein.readlines():
	line = line.rstrip(' \r\n')       # Strip newlines and such
	line_array.append(line)
	line_num += 1
	line_num_array.append(line_num)

	if (line_num % 64 == 0):
		line_ID = True
		#print line[7]
	else:
		line_ID = False

	line_ID_array.append(line_ID)



# Starting the real business
# - up to sub-sector 4 (XXXXXXX0 ... XXXXXXX4), the constants are 60 line after the Sector ID line
# - for sub-sector 5 (XXXXXXX5), the constants are 20 line after the Sector ID

# Sector ID state is 0
# sub-sector 0 -> 4 state is 1
# sub-sector 5 state is 2
# Due to firmware behaviour 
#    - zeros (zero_word) are added: state 3
#    - sector data has to be repeated (comprised the zero): state 4

word_i = 0
word_j = 0
word_z = 0
file_len = len(line_array)
zero_word = "00000000"
block_state = 0

crc_list0 = list()
crc_list1 = list()
crc_happy = list()

for i in range(file_len):
	
	if ((line_ID_array[i] == True) and (int(((line_array[i])[7]), 16) != 5) and (int(((line_array[i])[7]), 16) <6) and block_state == 0):
        # this is also due to firmware behaviour, we read up to a certain address
		if ((int(line_array[i], 16)) > 65535):
			break
		word_i = 0
		word_j = 0
		block_state = 1

	if (line_ID_array[i] == False and block_state == 1):
		word_i += 1
		if (word_i < 61):
			crc_list0.append(line_array[i])
		else:
			word_i == 0
			word_j == 0
			block_state = 0

	if ((line_ID_array[i] == True) and (int(((line_array[i])[7]), 16) == 5) and block_state == 0):
		if ((int(line_array[i], 16)) > 65535):
			#print line_num_array[i]
			break
		word_i = 0
		word_j = 0
		block_state = 2

	if (line_ID_array[i] == False and block_state == 2):
		word_j += 1
		if (word_j < 21):
			crc_list0.append(line_array[i])
		else:
			word_i == 0
			word_j == 0
			block_state = 3
	
	if block_state == 3:
		for k in range(160):
			crc_list0.append(zero_word)
		block_state = 4
		word_z = 0

	if block_state == 4:
		crc_list1 = crc_list0 + crc_list0
		crc_happy += crc_list1
		block_state = 0
		del crc_list0[:]
		del crc_list1[:]

tot_len = len(crc_happy)

for x in range(tot_len):
#	print crc_happy[x]
	crc = crc_fun(int_to_invbytestr(int(crc_happy[x], 16)), crc)

print hex(crc).rstrip('L')

